/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.core.authentication.manager;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.manager.IAccountManager;

/**
 * Class AccountManagerTest.
 *
 * @author Christian Cougourdan
 */
public class AccountManagerTest {

    /** La constante UID_USER. */
    private static final String UID_USER = "2016-12-USR-AAA-42";

    /** account manager. */
    private final IAccountManager<LocalUserBean> accountManager = new AccountManager();

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        AccountContext.currentUser(null);
    }

    @After
    public void tearDown() {
        AccountContext.currentUser(null);
    }

    /**
     * Test user bind.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUserBind() throws Exception {

        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("John");
        accountUserBean.setLastName("DOE");
        accountUserBean.setEmail("john.doe@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33612345678");
        accountUserBean.setAnonymous(false);

        final LocalUserBean actual = this.accountManager.getUserBean(UID_USER, accountUserBean);

        assertThat(actual,
                allOf( //
                        hasProperty("id", equalTo(UID_USER)), //
                        hasProperty("language", equalTo(accountUserBean.getLanguage())), //
                        hasProperty("anonymous", equalTo(accountUserBean.isAnonymous())) //
                ) //
        );
    }

    /**
     * Test user bind empty info.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUserBindEmptyInfo() throws Exception {
        final AccountUserBean accountUserBean = new AccountUserBean();

        final LocalUserBean actual = this.accountManager.getUserBean(UID_USER, accountUserBean);

        assertThat(actual,
                allOf( //
                        hasProperty("id", equalTo(UID_USER)), //
                        hasProperty("language", nullValue()), //
                        hasProperty("anonymous", equalTo(false)) //
                ) //
        );
    }

    /**
     * Test user bind no info.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUserBindNoInfo() throws Exception {
        final LocalUserBean actual = this.accountManager.getUserBean(UID_USER, null);

        assertThat(actual,
                allOf( //
                        hasProperty("id", equalTo(UID_USER)), //
                        hasProperty("country", nullValue()), //
                        hasProperty("language", nullValue()), //
                        hasProperty("anonymous", equalTo(Boolean.valueOf("false"))) //
                ) //
        );
    }

    /**
     * Build map info user.
     *
     * @return map
     */
    private Map<String, String> buildMapInfoUser() {
        final Map<String, String> mapInfoUser = new HashMap<>();
        mapInfoUser.put("origin", "GE");
        mapInfoUser.put("civility", "Monsieur");
        mapInfoUser.put("name", "DOE");
        mapInfoUser.put("forName", "John");
        mapInfoUser.put("mail", "john.doe@yopmail.com");
        mapInfoUser.put("country", "France");
        mapInfoUser.put("language", "fr");
        mapInfoUser.put("phone", "+33612345678");
        mapInfoUser.put("anonymous", "false");
        return mapInfoUser;
    }

    /**
     * Test persist.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersist() throws Exception {
        final AccountUserBean accountUserBean = new AccountUserBean();

        final LocalUserBean userBean = this.accountManager.getUserBean(UID_USER, accountUserBean);

        assertNull(AccountContext.currentUser());

        this.accountManager.persistUserContext(userBean);

        final LocalUserBean actual = AccountContext.currentUser();

        assertThat(actual,
                allOf( //
                        hasProperty("id", equalTo(UID_USER)), //
                        hasProperty("language", equalTo(accountUserBean.getLanguage())), //
                        hasProperty("anonymous", equalTo(accountUserBean.isAnonymous())) //
                ) //
        );
    }

    /**
     * Test persist super bean.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistSuperBean() throws Exception {
        final LocalUserBean userBean = new LocalUserBean(UID_USER);

        assertNull(AccountContext.currentUser());

        this.accountManager.persistUserContext(userBean);

        final LocalUserBean actual = AccountContext.currentUser();

        assertThat(actual,
                allOf( //
                        hasProperty("id", equalTo(UID_USER)), //
                        hasProperty("country", nullValue()), //
                        hasProperty("language", nullValue()), //
                        hasProperty("anonymous", equalTo(Boolean.valueOf("false"))) //
                ) //
        );
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemove() throws Exception {

        final AccountUserBean accountUserBean = new AccountUserBean();
        final LocalUserBean userBean = this.accountManager.getUserBean(UID_USER, accountUserBean);

        this.accountManager.persistUserContext(userBean);

        assertNotNull(AccountContext.currentUser());

        this.accountManager.cleanUserContext();

        assertNull(AccountContext.currentUser());
    }

}
