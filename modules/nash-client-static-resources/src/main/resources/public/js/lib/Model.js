/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/ValueReference', 'lib/ValidationError', 'lib/formContentReader', 'lib/i18n', 'lib/triggers', '_fm_adapters', '_fm_adapters_options' ], function($, ValueReference, ValidationError, formContentReader, i18n, triggers, adapters, adaptersOptions) {

    function retrieveAdapterOptions(block) {
        var rootId = $('form[name="record"] input[name="group.id"]').val();
        var fieldId = block.attr('data-field') || '';
        var normalizeId = rootId + '.' + fieldId.replace(/\[[^\]]+\]/g, '[]');

        return adaptersOptions[normalizeId]  || {};
    }


    /**
     * Persist values of an HTML block to model.
     * 
     * @param block
     * @returns persisted value
     */
    function persistModelValue(block) {
        var name = block.attr('data-field');
        var type = block.data('type');
        var adapter = adapters[type.toLowerCase()];
        var adapterOptions = retrieveAdapterOptions(block);

        var valueReference = ValueReference.create(name);

        block.children('.type-errors-list').remove();

        try {
            var newValue = retrievePageValue(block);
            valueReference.set(newValue);
        } catch (err) {
            if (err instanceof ValidationError) {
                console.log(err.fieldName + ' : ' + err.message);
                block.append($('<ul class="type-errors-list">').append($("<li>").text(err.message)));
            } else {
                console.warn(err.message)
            }
        }

        return valueReference.get();
    }


    function retrieveModelValue(name) {
        return ValueReference.create(name).get();
    }


    function retrievePageValue(block) {
        var name = block.attr('data-field');
        var type = block.data('type');
        type = type != null ? type.toLowerCase() : null;
        var adapter = adapters[type];
        var adapterOptions = retrieveAdapterOptions(block);

        if (adapter && adapter.validate) {
            var formContentData = formContentReader.read('[data-field="' + name + '"]').withPath(name);
            var validation = adapter.validate(formContentData, adapterOptions);
            if (validation.validated === true) {
                if (typeof validation.value === 'object') {
                    console.debug('Input "' + name + '" value has changed to :', validation.value);
                } else {
                    console.debug('Input "' + name + '" value has changed to "' + validation.value + '"');
                }
                return validation.value;
            } else {
                var displayedMessage = i18n(validation.message || 'This value is not valid.');
                throw new ValidationError(name, displayedMessage);
            }
        } else if (console) {
            throw new Error('no value validator found for type "' + type + '" !')
        }
    }


    function initialize(target) {
        var elm = null;
        if (target) {
            if (typeof target == 'string') {
                elm = $('[data-field^="' + target + '"][data-type]');
            } else {
                elm = target.fragment();
            }
        } else {
            elm = $('[data-field][data-type]');
        }

        var isReadOnly = $('form[name="record"]').data("readonly") || false;

        elm.off('fm.update').on('fm.update', function() {
            var block = $(this);
            var type = block.data('type');
            type = type != null ? type.toLowerCase() : null;
            var name = block.data('field'), adapter = adapters[type];
            var adapterOptions = retrieveAdapterOptions(block);

            if(!block.hasClass('hide')) {
                var hasHiddenParent = block.parents("div[data-group].hide").length > 0;

                if(!hasHiddenParent){
                    if (adapter) {
                        if (adapter.initialize) {
                            adapterOptions['readOnly'] = isReadOnly;
                            adapter.initialize(block, adapterOptions);
                        }
                    } else if (console) {
                        console.warn('no value adapter defined for type "' + (type || '') + '" !');
                    }
                }
            }
                
        }).trigger('fm.update');
    }


    function initializeGroup(target) {
        var group;
        if (typeof target == 'string') {
            group = $('[data-group="' + target + '"]');
        } else {
            group = target.fragment();
        }

        if (!group.hasClass('hide')) {
            var children = group.find('> .panel-body-container > .panel-body').find('> [data-field], > [data-group], > .repeatable > [data-field], > .repeatable > [data-group]');
            children.each(function() {
                var $elm = $(this);
                if (!$elm.hasClass('hide')) {
                    if ($elm.attr('data-group')) {
                        initializeGroup($elm.attr('data-group'));
                    } else if ($elm.attr('data-field')) {
                        initialize($elm.attr('data-field'));
                    }
                }
            });
        }
    }

    function displayAction(target, predicate) {
        if (triggers.action.execute(target, predicate)) {
            target.fragment().removeClass('hide');

            // If the element is a data we initialize it
            if (target.type() == 'field') {
                initialize(target);
            }

            // If the element is a GROUP we initialize all his data without the class Hide
            if (target.type() == 'group') {
                initializeGroup(target);
            }

            return true;
        } else {
            target.fragment().addClass('hide');
            return false;
        }
    }

    triggers.action.register('field:display', displayAction);
    triggers.action.register('group:display', displayAction);

    return {
        model: {
            set: persistModelValue,
            get: retrieveModelValue
        },
        page: {
            initialize: initialize,
            get: retrievePageValue
        }
    };

});
