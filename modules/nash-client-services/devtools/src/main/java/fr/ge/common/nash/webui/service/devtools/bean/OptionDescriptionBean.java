/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.bean;

/**
 * An option description.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class OptionDescriptionBean {

    /** The name. */
    private final String name;

    /** The HTML name. */
    private final String htmlName;

    /** The description. */
    private final String description;

    /** Is the field mandatory ?. */
    private final boolean mandatory;

    /** The option type. */
    private final String type;

    /** The value. */
    private final String value;

    /**
     * Constructor.
     *
     * @param name
     *            the name
     * @param htmlName
     *            the HTML name
     * @param description
     *            the description
     * @param mandatory
     *            is the field mandatory ?
     * @param type
     *            the optionType
     * @param value
     *            the value
     */
    public OptionDescriptionBean(final String name, final String htmlName, final String description, final boolean mandatory, final String type, final String value) {
        this.name = name;
        this.htmlName = null == htmlName ? name : htmlName;
        this.description = description;
        this.mandatory = mandatory;
        this.type = type;
        this.value = value;
    }

    /**
     * Getter on attribute {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter on attribute {@link #htmlName}.
     *
     * @return String htmlName
     */
    public String getHtmlName() {
        return this.htmlName;
    }

    /**
     * Getter on attribute {@link #description}.
     *
     * @return String description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Getter on attribute {@link #mandatory}.
     *
     * @return boolean mandatory
     */
    public boolean isMandatory() {
        return this.mandatory;
    }

    /**
     * Getter on attribute {@link #type}.
     *
     * @return String optionType
     */
    public String getType() {
        return this.type;
    }

    /**
     * Getter on attribute {@link #value}.
     *
     * @return String value
     */
    public String getValue() {
        return this.value;
    }

}
