/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, 
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */ 
'use strict';  

var path = require('path');
var extend = require('util')._extend;

var project = {
        build: {
            directory: 'target',
            outputDirectory: 'target/classes/public',
            sourceDirectory: 'src/main/resources/public'
        }
};
  
/**
 * Build CSS file item.
 * 
 * @param basePath source base path
 * @param name resource name
 * @returns file item
 */
function buildStyle(basePath, name) {
    return {
        src: [ basePath + '/' + name ],
        dest: '<%= project.build.outputDirectory %>/css/libs/' + path.basename(basePath) + '/' + path.basename(name).replace(/\.[^.]+$/, '.css')
    }
}

var overrides = {};

module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');

    var scripts = grunt.file.expandMapping([ '**/*.js' ], '<%= project.build.outputDirectory %>/js', {
        cwd: project.build.sourceDirectory + '/js'
    });

    var styles = Object.keys(pkg.dependencies || {}).map(function (name) {
        var basePath = 'node_modules/' + name;
        var nfo = extend(grunt.file.readJSON(basePath + '/package.json'), overrides[name] || {});
        var styles = (nfo.less || nfo.style);

        if (null == styles) {
            return null;
        } else if (Array.isArray(styles)) {
            return styles.map(function (elm) {
                return buildStyle(basePath, elm);
            });
        } else {
            return [ buildStyle(basePath, styles) ];
        }
    }).filter(function (lst) {
        return null != lst && lst.length > 0;
    }).reduce(function (all, lst) {
        return all.concat(lst);
    }, []);

    styles = grunt.file.expandMapping([ '**/*.less', '**/*.css', '!mixins/**' ], '<%= project.build.outputDirectory %>/css', {
        cwd: project.build.sourceDirectory + '/css'
    }).map(function (elm) {
        return {
            src: elm.src,
            dest: elm.dest.replace(/\.[^.]+$/, '.css')
        };
    }).concat(styles);

    var copyJsFiles = [];



    var requireBaseDeps = {};
    Object.keys(pkg.dependencies || {}).filter(elm => elm !== 'requirejs').forEach(function (dep) {
        var path = '';
        var nfo = extend(require('./node_modules/' + dep + '/package.json'), overrides[dep] || {});
        if (nfo.main) {
            if (typeof nfo.main === 'string') {
                path = nfo.main;
            } else if (nfo.main.find) {
                path = nfo.main.find(elm => elm.match(/\.js$/));
            } else {
                return;
            }
            requireBaseDeps[dep] = 'node_modules/' + dep + path.replace(/^\.?[\/]*/, '/').replace(/\.js$/, '');
        }
    });

    
    var layoutProvidedDependencies = {};
    [ 'bootstrap', 'font-awesome', 'jquery', 'jquery-ui', 'datatables.net', 'datatables.net-bs' ].forEach(dep => layoutProvidedDependencies[dep] = 'empty:');

    var requireEmptyLibs = {};
    Object.keys(requireBaseDeps).concat(Object.keys(layoutProvidedDependencies)).forEach(dep => requireEmptyLibs[dep] = 'empty:');

    var requireBaseLibs = grunt.file.expand({ cwd: project.build.sourceDirectory + '/js' }, [ 'lib/**/*.js' ]).map(function (elm) { return elm.replace(/\.js$/, ''); });


    /*
     * GRUNT configuration
     */
    grunt.initConfig({
        project : project,
        less : {
            dist : {
                options : {
                    modifyVars : {
                        'fa-font-path' : 'fonts',
                        'fa-font-path' : 'fonts'
                    },
                    paths : [ '<%= project.build.sourceDirectory %>/css/mixins', 'node_modules/bootstrap/less', 'node_modules/font-awesome/less' ]
                },
                files : styles
            }
        },
        extract: {
            dist: {
                files: styles
            }
        },
        cssmin : {
            options : {
                keepSpecialComments : 0
            },
            dist : {
                files : [ {
                    expand : true,
                    cwd : project.build.outputDirectory + '/css',
                    src : [ '**/*.css', '!**/*.min.css' ],
                    dest : project.build.outputDirectory + '/css',
                    ext : '.min.css'
                } ]
            }
        },
        copy : {
            dist : {
                options : {
                    process : function(content, srcPath, dstPath) {
                        if (grunt.file.isMatch([ '*/pwstrength-bootstrap/**/*.js' ], srcPath)) {
                            return 'define([ \'jquery\' ], function (jQuery) { ' + content + '});';
                        } else if (grunt.file.isMatch(['*/bootstrap-treeview/**/*.js'], srcPath)) {
                            return 'define([ \'jquery\', \'bootstrap\' ], function (jQuery) { ' + content + '});';
                        } else {
                            return content;
                        }
                    },
                    noProcess : [ '!**/*.js' ]
                },
                files : copyJsFiles
            }
        },
        i18n : { 
            dist : {
                cwd : 'src/main/resources',
                src : '**/*.po',
                dest : '<%= project.build.outputDirectory %>/js/i18n'
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', [ 'less', 'extract', 'cssmin' ]);

}
