/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit.rest.v2.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseReferentialItemBean;
import fr.ge.directory.ws.v2.service.IRefService;

/**
 * Passthrough service with Directory bubble.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class DirectoryPassthroughRestServiceImpl implements IRefService {

    @Autowired
    @Qualifier("refServiceRestServiceV2")
    private IRefService refServiceRestServiceV2;

    /**
     * {@inheritDoc}
     */
    @Override
    public void save(final String refTableName, final List<JsonNode> details) {
        this.refServiceRestServiceV2.save(refTableName, details);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<JsonNode> search(final String refTableName, final String searchedValue) {
        return this.refServiceRestServiceV2.search(refTableName, searchedValue);
    }

    @Override
    public void truncate(final String refTableName) {
        refServiceRestServiceV2.truncate(refTableName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<JsonNode> findAll(String refTableName, long startIndex, long maxResults, List<SearchQueryFilter> filters, List<SearchQueryOrder> orders, String searchTerms) {
        return refServiceRestServiceV2.findAll(refTableName, startIndex, maxResults, filters, orders, searchTerms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseReferentialItemBean get(long id, String refTableName) {
        return refServiceRestServiceV2.get(id, refTableName);
    }

}
