# Nash Web : Dossier d'installation


## Livrables

| GroupID           | Nom du fichier                       | Description                          |
| ----------------- | ------------------------------------ | ------------------------------------ |
| fr.ge.common.nash | nas-client-web-*x.y.z*.war           | Module applicatif                    |
| fr.ge.common.nash | nas-client-web-*x.y.z*-config.zip    | Fichier(s) de paramétrage applicatif |

\* *x.y.z* représente la version livrée


## Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| Noeud applicatif  | Description                            | Composants techniques requis |
| ----------------- | -------------------------------------- | ---------------------------- |
| VH_NASH_WEB       | Frontal du module applicatif ***web*** | Apache                       |
| APP_NASH_WEB      | Module applicatif ***web***            | Tomcat 8.0.x                 |


## Installation du virtual host

| Nom            | Description       | PROD                               |
| -------------- | ----------------- | ---------------------------------- |
| Module **web** | URL de l'instance | nash-client-web.${public_domain}   |

Pour l'exemple, dans le répertoire ``/etc/apache2/sites-enabled``, ajouter un virtualhost (ie **nash-client-web.${public_domain}.conf**) redirigeant vers l'application :

```
# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
 
<VirtualHost *:8080>
  ServerName nash-client-web.${public_domain}
 
  ## Vhost docroot
  DocumentRoot "/var/www/nash"
 
  ## Directories, there should at least be a declaration for /var/www/nash
 
  <Location "/status">
    Require valid-user
    AuthType Basic
    AuthName "status"
    AuthUserFile /usr/passwd/status/htpasswd
  </Location>
 
  <Directory "/var/www/nash">
    Options None
    AllowOverride None
    Require all denied
  </Directory>
 
  ## Logging
  ErrorLog "/var/log/apache2/nash-client-web.${public_domain}_error.log"
  ServerSignature Off
  CustomLog "/var/log/apache2/nash-client-web.${public_domain}_access.log" combined
 
  JkMount   /* nash-client-web
 
</VirtualHost>
```

Il est nécessaire d'activer le module **mod_jk** d'Apache gérant la communication et la répartition de charge entre Apache et les serveurs Tomcat.

Ajouter dans le répertoire ``/etc/apache2`` le fichier ``workers.properties``, celui-ci contenant la configuration du module Apache :

``` properties
worker.list=jkstatus,nash-client-web
 
worker.maintain=240
worker.jkstatus.type=status
worker.server1.port=8009
worker.server1.host=nash-client-web.${public_domain}
worker.server1.type=ajp13
worker.server1.lbfactor=1
worker.server1.connection_pool_size=250
worker.server1.connection_pool_timeout=600
worker.server1.socket_timeout=900
worker.server1.socket_keepalive=True
 
worker.nash-client-web.balance_workers=server1
worker.nash-client-web.type=lb
```



## Installation de l'archive Web

L'installation se fait sous ``/var/lib/tomcat8``.

L'archive Web de l'application doit être déposé sous ``/srv/appli/nash/nash-client-web``.

La configuration tomcat nécessaire pour le démarrage de l'application se trouve sous ``/var/lib/tomcat8/conf/``.

* Configuration du serveur Catalina : ``Catalina/nash-client-web.${public_domain}/ROOT.xml``
Le context doit pointer sur le war décrit plus haut.

``` xml : Catalina/.../ROOT.xml
<?xml version="1.0" encoding="UTF-8"?>

        <Context reloadable="true" docBase="/srv/appli/nash/nash-ws-web/nash-ws-server.war">

        <Resources className="org.apache.catalina.webresources.StandardRoot">
            <PreResources className="org.apache.catalina.webresources.DirResourceSet"
                base="/srv/conf/nash/nash-client-web"
                webAppMount="/WEB-INF/classes" />
        </Resources>

</Context>
```

Ajout de l'hôte virtuel sur la configuration du serveur : ``/var/lib/tomcat8/conf/server.xml`` .

Ajout de la ligne :

``` xml
<Host name="nash-client-web.${public_domain}"
      appBase="webapps_nash-client-web"
      unpackWARs="true"
      autoDeploy="true" />
```


## Configuration de l'application

### Fichier ``application.properties``



### Fichier ``engine.properties``



### Fichier ``log4j2.xml``

| Paramètre           | Description                                                  | Valeur                                  |
| ------------------- | ------------------------------------------------------------ | --------------------------------------- |
| log.level           | Niveau de log par défaut                                     | "debug", "info", ...                    |
| log.level_app       | Niveau de log propre au module applicatif                    | "debug", "info", ...                    |
| log.level_fwk       | Niveau de log des librairies externes                        | "debug", "info", ...                    |
| log.file.technical  | Chemin du fichier contenant les logs techniques              | /srv/log/nash/nash-web/tech.log         |
| log.file.functional | Chemin du fichier contenant les logs fonctionnels            | /srv/log/nash/nash-web/func.log         |
| log.file.thirdParty | Chemin du fichier contenant les logs des libraires externes  | /srv/log/nash/nash-web/thirdParty.log   |
| log.max_size        | Taille maximale des fichiers de logs journalisés             | 10 MB                                   |
| log.max_index       | Index maximum des fichiers de logs journalisés               | 20                                      |

