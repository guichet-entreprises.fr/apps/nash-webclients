/**
 *
 */
package fr.ge.common.nash.webui.service.markov.record.search.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class HomeControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/test-engine-context.xml" })
@WebAppConfiguration
public class MarkovControllerTest extends AbstractTest {

    private static final String RECORD_UID = "2018-04-REC-ORD-42";

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    private Engine engine;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.engine);
        when(this.engine.loader(any())).thenCallRealMethod();
    }

    /**
     * Tests
     * {@link HomeController#importRecord(org.springframework.ui.Model, String)} .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testImportRecord() throws Exception {
        final byte[] asBytes = this.resourceAsBytes("spec.zip");
        final IProvider provider = new ZipProvider(asBytes);
        final SpecificationLoader loader = spy(this.engine.loader(provider));
        when(this.engine.loader(any())).thenReturn(loader);

        // call
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/import", RECORD_UID)) //
                .andExpect(status().isFound()) //
                .andExpect(view().name("redirect:/record/{code}/{step}/page/0")) //
                .andReturn();
    }

}
