/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/clipboard', 'prismjs' ], function($, Clipboard, prism) {

    var pdfLoadingDate;

    function updateFieldNamesVisibility() {
        var searchedName = $("#search-name").val();
        var currPageIndex = parseInt($("#current-page-index").text());
        var techPageIndex = currPageIndex - 1;
        $("#pdfFieldsTable tbody tr").each(function() {
            var tr = $(this);
            var fieldName = tr.find("td.name").text();
            var notOnPage = !coordinatesRatios[fieldName][techPageIndex];
            var notSearched = fieldName.indexOf(searchedName) == -1;
            var notDisplayed = notOnPage || notSearched;
            tr.toggleClass("not-displayed", notDisplayed);
        });
    }

    function setPdfPage(askedPageIndex) {
        var $currPageIndex = $("#current-page-index");
        var currPageIndex = parseInt($currPageIndex.text());
        var minPageIndex = 1;
        var maxPageIndex = parseInt($("#max-page-index").text());
        var processedPageIndex = Math.min(Math.max(minPageIndex, askedPageIndex), maxPageIndex);
        var pdfPreview = $("#pdf-preview");
        if (!pdfPreview.attr("src") || processedPageIndex != currPageIndex) {
            var techPageIndex = processedPageIndex - 1;
            pdfPreview.attr("src", baseUrl + "engine/pdf/display?pageIndex=" + techPageIndex + "&_=" + pdfLoadingDate);
            $currPageIndex.text(processedPageIndex);
            updateFieldNamesVisibility();
            $("#pdf-preview-info").toggleClass("not-displayed", true);
        }
    }

    function setPdfInfo(fieldName) {
        if (fieldName == null) {
            $("#pdf-preview-info").toggleClass("not-displayed", true);
            return;
        }
        var currPageIndex = parseInt($("#current-page-index").text());
        var techPageIndex = currPageIndex - 1;
        var fieldRatios = coordinatesRatios[fieldName][techPageIndex];
        var $pdfPreview = $("#pdf-preview");
        var imgHeight = $pdfPreview.height();
        var imgWidth = $pdfPreview.width();
        var lowerLeftX = fieldRatios[0] * imgWidth;
        var lowerLeftY = imgHeight - fieldRatios[1] * imgHeight;
        var upperRightX = fieldRatios[2] * imgWidth;
        var upperRightY = imgHeight - fieldRatios[3] * imgHeight;
        var upperMiddleX = (lowerLeftX + upperRightX) / 2;
        var upperMiddleY = upperRightY;
        var $pdfPreviewInfo = $("#pdf-preview-info");
        $pdfPreviewInfo.text(fieldName);
        $pdfPreviewInfo.toggleClass("not-displayed", false);
        var infoHeight = $pdfPreviewInfo.outerHeight();
        var infoWidth = $pdfPreviewInfo.outerWidth();
        $pdfPreviewInfo.css("top", upperMiddleY - infoHeight);
        $pdfPreviewInfo.css("left", upperMiddleX - infoWidth / 2);
        $('html, body').animate({
            scrollTop : $("#pdf-preview-info").offset().top
        });
    }

    function findClickedPdfField(imgX, imgY) {
        var currPageIndex = parseInt($("#current-page-index").text());
        var techPageIndex = currPageIndex - 1;
        var $pdfPreview = $("#pdf-preview");
        var imgHeight = $pdfPreview.height();
        var imgWidth = $pdfPreview.width();
        var imgHeightRatio = imgY / imgHeight;
        var imgWidthRatio = imgX / imgWidth;
        for (fieldName in coordinatesRatios) {
            var fieldRatios = coordinatesRatios[fieldName][techPageIndex];
            if (fieldRatios && fieldRatios[0] < imgWidthRatio && imgWidthRatio < fieldRatios[2] && (1 - fieldRatios[3]) < imgHeightRatio && imgHeightRatio < (1 - fieldRatios[1])) {
                return fieldName;
            }
        }
        return null;
    }

    $(function() {
        [ 'prism', 'prism-line-highlight', 'prism-line-numbers' ].forEach(function (css) {
            $('head').append('<link rel="stylesheet" href="' + baseUrl + '/css/libs/prismjs/' + css + '.css" />');
        });

        pdfLoadingDate = new Date().getTime();
        if ($("#pdf-preview").length > 0) {
            setPdfPage(1);
        }
        $("#btn-pdf-previous-page").click(function() {
            var askedPageIndex = parseInt($("#current-page-index").text()) - 1;
            setPdfPage(askedPageIndex);
        });
        $("#btn-pdf-next-page").click(function() {
            var askedPageIndex = parseInt($("#current-page-index").text()) + 1;
            setPdfPage(askedPageIndex);
        });
        $("#search-name").change(function() {
            updateFieldNamesVisibility();
        });
        $("#pdfFieldsTable tbody tr").click(function() {
            var fieldName = $(this).find("td.name").text();
            setPdfInfo(fieldName);
        });
        $('#pdf-preview-wrapper').click(function(e) {
            var offset = $(this).offset();
            var imgX = e.pageX - offset.left;
            var imgY = e.pageY - offset.top;
            var fieldName = findClickedPdfField(imgX, imgY);
            setPdfInfo(fieldName);
        });
        $("#btn-pdf-copy-clipboard").click(function() {
            Clipboard.copy("fields-filling-template");
        });

        $('input[type="file"][data-toggle="file"]').fileUpload();
    });

});
