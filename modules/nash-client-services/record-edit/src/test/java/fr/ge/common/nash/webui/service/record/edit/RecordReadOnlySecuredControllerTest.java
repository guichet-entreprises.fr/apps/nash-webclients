/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Class RecordStepControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/test-storage-context.xml" })
@WebAppConfiguration
@TestPropertySource(properties = { "nash.readOnly=true", })
public class RecordReadOnlySecuredControllerTest extends AbstractRecordControllerTest {

    /** La constante RECORD_UID_UNKNOWN. */
    private static final String RECORD_UID_UNKNOWN = "2016-12-REC-AAA-42";

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test save with read only mode enabled.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSave() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02", "John");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService, never()).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());
    }

    /**
     * Test save validate.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSaveValidate() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 1) //
                .param("group.id", "group02") //
                .param("success", "validate");

        this.mvc.perform(request) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record/" + RECORD_UID + "/2"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService, never()).upload(eq(RECORD_UID), eq("description.xml"), captor.capture());
    }

    /**
     * Test save last group.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveLastGroup() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 1) //
                .param("success", "next") //
                .param("group.id", "group_1");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(MockMvcResultMatchers.forwardedUrl("/dashboard"));
    }

    /**
     * Test edit specific page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditSpecificPage() throws Exception {
        final int page = 1;

        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 1, page)) //
                .andExpect(view().name("record/edit/main")) //
                .andExpect(model().attribute("code", RECORD_UID)) //
                .andExpect(model().attribute("stepIndex", 1)) //
                .andExpect(model().attribute("page", page)); //
    }

    /**
     * Test edit previous step.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditPreviousStep() throws Exception {
        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 1, 0)) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(view().name("record/edit/main")); //
    }

    /**
     * Test edit future step.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditFutureStep() throws Exception {
        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 2, 1)) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(view().name("record/edit/main")); //
    }

    /**
     * Test edit too high page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditTooHighPage() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/page/{page}", RECORD_UID, 100)) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));
    }

    /**
     * Test edit not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditNotFound() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1", RECORD_UID_UNKNOWN)) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/main"));
    }
}
