define([ 'jquery','lib/i18n' ,'lib/template' ], function ($,i18n,Template) {

    function buildErrorMessage(xhr) {
        var msg, contentType = xhr.getResponseHeader('Content-type');

        if (contentType.startsWith('application/json')) {
            msg = JSON.parse(xhr.responseText);
        } else {
            msg = {
                type : 'ERROR',
                title : xhr.responseText
            };
        }

        return msg;
    }

    function displayExceptions(src, placeholder) {
        if (!src) {
            return;
        }
        var $placeholder;
        if (placeholder) {
            $placeholder = $(placeholder);
        } else {
            $placeholder = $("#alertsPlaceholder");
        }
        var alertTemplate = Template.find("alert");
        var msgList = [];
        if (typeof src == "object") {
            msgList = src;
        } else if (typeof src == "string") {
            $(src).each(function() {
                var element = $(this);
                if (element.is("input")) {
                    msgList.push(element.val());
                } else {
                    msgList.push(element.text());
                }
                element.remove();
            });
        }
        $placeholder.empty();
        $.each(msgList, function(index, element) {
            $placeholder.append($("<div>").html(alertTemplate.render({
                type : "danger",
                message : i18n(element)
            })))
        });
    }

    return {
        displayExceptions : displayExceptions
    };

});
