/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.support.i18n.MessageExtractor;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.engine.util.ValueAdapterResourceTypeEnum;

/**
 * The Class EngineController.
 *
 * @author Christian Cougourdan
 */
@Controller("recordEditTypesRestController")
public class TypesRestController {

    /**
     * Gets a type script.
     *
     * @param response
     *            the response
     * @param version
     *            the version
     * @param type
     *            the type
     * @throws IOException
     *             an IO exception
     */
    @RequestMapping(value = "/rest/type/{version}/{type}/script", method = RequestMethod.GET)
    public void script(final HttpServletResponse response, @PathVariable(value = "version") final String version, @PathVariable(value = "type") final String type) throws IOException {
        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(version, type);

        if (null == valueAdapter) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.getWriter().print(String.format("Type '%s' not found in version '%s'", type, version));
            return;
        }

        final Collection<InputStream> resources = new ArrayList<>();
        resources.add(MessageExtractor.extractAsScript(NashMessageReader.getReader(valueAdapter)));

        final byte[] asBytes = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.SCRIPT, valueAdapter.getClass());
        if (null != asBytes) {
            resources.add(new ByteArrayInputStream(asBytes));
        }

        response.setContentType("text/javascript");
        IOUtils.copy(new SequenceInputStream(Collections.enumeration(resources)), response.getOutputStream());
    }

    /**
     * Gets a type style.
     *
     * @param response
     *            the response
     * @param version
     *            the version
     * @param type
     *            the type
     * @throws IOException
     *             an IO exception
     */
    @RequestMapping(value = "/rest/type/{version}/{type}/style", method = RequestMethod.GET)
    public void style(final HttpServletResponse response, @PathVariable(value = "version") final String version, @PathVariable(value = "type") final String type) throws IOException {
        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(version, type);

        if (null == valueAdapter) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.getWriter().print(String.format("Type '%s' not found in version '%s'", type, version));
            return;
        }

        response.setContentType("text/css");

        final byte[] asBytes = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.STYLE, valueAdapter.getClass());
        if (null != asBytes) {
            response.getOutputStream().write(asBytes);
        }
    }

}
