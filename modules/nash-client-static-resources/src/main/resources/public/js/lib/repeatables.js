/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/i18n', 'lib/triggers', 'lib/Model', 'lib/ValueReference', 'lib/func/localScopeModel', 'lib/utils', '_fm_description', '_fm_rules' ], function ($, i18n, Triggers, Model, ValueReference, localScopeModel, Utils, description, rules) {

    var occurs = Object.keys(rules || {}).reduce(function (all, targetPath) {
        var key = Utils.buildRuleKey(targetPath);
        if (undefined === all[key]) {
            var rule = rules[targetPath];
            [ 'minOccurs', 'maxOccurs' ].forEach(function (type) {
                if (undefined !== rule[type] && undefined !== rule[type].predicate) {
                    if (undefined === all[key]) {
                        all[key] = {};
                    }
                    all[key][type] = rule[type].predicate;
                }
            });
        }

        return all;
    }, {});

    console.log('[Repeatables] Occurrence predicates :', occurs);

    var repeatablesActions = {

            /**
             * Move up the current occurrence, move down the previous one.
             */
            'arrow-up': function (action) {
                var elementDivPrev = action.target.prev();
                action.target.fadeOut('slow', function() {
                    var elementDivIndex = action.occurrences.index(action.target);
                    $(this).detach().insertBefore(elementDivPrev).fadeIn('slow');
                    updateRepeatablesIdAndLabel(action);

                    //get the context
                    var value = ValueReference.create(action.path.simpleName.replace(/\[[0-9]*\]$/, '')).get();
                    var tmp = value[elementDivIndex];

                    //update the context
                    value[elementDivIndex] = value[elementDivIndex-1];
                    value[elementDivIndex - 1] = tmp;
                });
            },

            /**
             * Move down the current occurrence, move up the previous one.
             */
            'arrow-down': function (action) {
                var elementDivNext = action.target.next();
                action.target.fadeOut('slow', function() {
                    var elementDivIndex = action.occurrences.index(action.target);
                    $(this).detach().insertAfter(elementDivNext).fadeIn('slow');
                    updateRepeatablesIdAndLabel(action);

                    //-->get the context
                    var value = ValueReference.create(action.path.simpleName.replace(/\[[0-9]*\]$/, '')).get();
                    var tmp = value[elementDivIndex];
                    //-->update the context
                    value[elementDivIndex] = value[elementDivIndex+1];
                    value[elementDivIndex+1] = tmp;
                });
            },

            /**
             * 
             */
            'add': function (action) {
                action.source && action.source.addClass('hidden');
                Tools.add(action, 1, function () {
                    action.source && action.source.removeClass('hidden');
                });
            },

            /**
             * 
             */
            'delete': function (action) {
                if (action.targetType == 'field') {
                    Tools.remove(action);
                } else {
                    //-->Add confirmation modal
                    var dlg = $('#confirmRemoveModal');
                    var occurrenceTitle = action.target.find('.occurrence-title:first').text();
                    $('.modal-header .modal-title', dlg).text(i18n("Confirmation request"));
                    $('.modal-body', dlg).text(i18n("Do you really want to remove the data group named \"{0}\" ?", occurrenceTitle));
                    dlg.off('click', 'button[role="validate"]').on('click', 'button[role="validate"]', function(evt) {
                        dlg.modal('hide');
                        Tools.remove(action);
                    }).modal('show');
                }
            }
    };


    var Tools = {

            /**
             * 
             */
            'add': function (action, cnt, cb) {
                var path = action.path.fullName //
                        .replace(/[.]/g, '/') //
                        .replace(/\[([^\]]*)\]/g, '/$1');

                if (action.maxOccurs <= action.occurrences.length) {
                    cb && cb();
                    return;
                }

                $.ajax({
                    'url': baseUrl + 'record/' + description.recordUid + '/' + description.displayedStep + '/fragment/' + path,
                    'method': 'GET',
                    'data': {
                        'occurrenceIndex': action.occurrences.index(action.target) + 1,
                        'occurrencesTotal': action.occurrences.length + 1
                    }
                }).done(function(result) {
                    var htmlFragment = '';
                    for (var idx = 0; idx < cnt; idx++) {
                        htmlFragment += result;
                    }

                    var insertedResult = updateRepeatablesId($(htmlFragment), action.occurrences.length);
                    
                    if (action.targetType == 'group') {
                        //-->Get json fragment
                        var fragmentGroupAsJSON = atob(insertedResult.attr('data-json'));
                        
                        //-->Get full path where to add the json fragment
                        var valueGroup = ValueReference.create(action.path.simpleName.replace(/\[[0-9]*\]$/, '')).get();

                        //Add the element from the context
                        for (var idx = 0; idx < cnt; idx++) {
                            valueGroup.push(JSON.parse(fragmentGroupAsJSON));
                        }
                    } else {
                        //Add the element from the context
                        var valueRef = ValueReference.create(action.path.collection);
                        var valueField = valueRef.get();
                        if (!$.isArray(valueField)) {
                            valueRef.set(valueField = [ valueField ]);
                        }
                        for (var idx = 0; idx < cnt; idx++) {
                            valueField.push(null);
                        }
                    }

                    $('[data-rule]', insertedResult).each(function () {
                        Triggers.execute($(this));
                    });

                    insertedResult.insertAfter(action.target).hide().show('slow');
                    updateRepeatablesIdAndLabel(action);

                    $('[data-toggle="popover"]', insertedResult).popover();
                    Model.page.initialize(Utils.normalize(insertedResult));

                    cascadeTriggers(action);
                    
                    Model.page.initialize();
                }).always(function(result) {
                    cb && cb(result);
                });
            },

            /**
             * 
             */
            'remove': function (action, cnt, cb) {
                if (action.minOccurs >= action.occurrences.length) {
                    cb && cb();
                    return;
                }

                var indexes = [];
                var ticker = new Ticker(action.target.length, function () {
                    var valueField = ValueReference.create(action.targetType == 'field' ? action.path.collection : action.path.simpleName.replace(/\[[0-9]*\]$/, '')).get();

                    indexes.sort(function (a, b) { return (b - a); });
                    for (var idx = 0; idx < indexes.length; idx++) {
                        valueField.splice(indexes[idx], 1);
                    }

                    action.target.remove();

                    updateRepeatablesIdAndLabel(action);
                    cb && cb();
                    cascadeTriggers(action);
                });

                action.target.each(function () {
                    var target = $(this);
                    var targetIndex = action.occurrences.index(target);
                    console.log('[Repeatables.remove] target index :', targetIndex);
                    indexes.push(targetIndex);
                    target.hide('slow', function() {
                        ticker.tic();
                    });
                });
            }
        };


    function Ticker(total, cb) {
        this.cnt = total;
        this.cb = cb;

        this.tic = function () {
            this.cnt -= 1;
            if (this.cnt <= 0) {
                this.cb && this.cb();
            }
        }
    }


    function cascadeTriggers(action) {
        $('div[data-group], div[data-field]', action.target).each(function (elm) {
            Triggers.execute($(elm));
        });
    }


    function translatePath(root, attrNames, baseValue, idx, builder) {
        var selectorBuilder;
        var m = /^([^*]+)[*](.*)$/.exec(baseValue);
        if (m) {
            selectorBuilder = function (name) { return '*[' + name + '^="' + m[1] + '"][' + name + '$="' + m[2] + '"]'; };
        } else {
            selectorBuilder = function (name) { return '*[' + name + '^="' + baseValue + '"]'; };
        }
        $(attrNames.map(selectorBuilder).join(', '), root).addBack().each(function() {
            var occurrenceOrChildDiv = $(this);
            attrNames.forEach(function (attrName) {
                var attrValue = occurrenceOrChildDiv.attr(attrName);
                if (attrValue && attrValue.indexOf(baseValue) == 0) {
                    var newValue = builder(baseValue, attrValue, idx);
                    occurrenceOrChildDiv.attr(attrName, newValue);
                }
            });
        });
    }


    function defaultBuilder(baseValue, attrValue, i) {
        return baseValue.replace(/([-\[])[0-9]+([-\]])$/, '$1' + i + '$2') + attrValue.substring(baseValue.length);
    }


    function retrieveOccurs(action, type) {
        var predicates = occurs[action.key];
        var nb = 0;

        if (undefined !== action[type]) {
            nb = action[type];
        } else if (undefined === predicates || undefined === predicates[type]) {
            nb = 'maxOccurs' === type ? Number.MAX_VALUE : 0;
        } else {
            nb = predicates[type](localScopeModel(action.path.simpleName));
        }

        if ('maxOccurs' === type) {
            return Math.max(1, nb);
        } else {
            return nb;
        }
    }


    function updateRepeatablesId(occurrences, idx) {
        // update ids of hierarchy
        var occurrencePath = occurrences.attr('data-group');
        if (undefined === occurrencePath) {
            occurrencePath = occurrences.attr('data-field');
        }

        occurrences.each(function () {
            var occurrence = $(this);

            /*
             * Update attributes containing raw element ID
             */
            translatePath(occurrence, ['data-group', 'data-field', 'for', 'id', 'name'], occurrencePath, idx, defaultBuilder);
            
            /*
             * Update attributes containing translated element ID (folding)
             */
            var translatedOccurrencePath = occurrencePath.replace(/[.\[*\]]+/g, '-').replace(/-$/, '');
            
            translatePath(occurrence, [ 'data-target' ], '#' + translatedOccurrencePath + '-', idx, defaultBuilder);
            translatePath(occurrence, [ 'id' ], translatedOccurrencePath + '-', idx, defaultBuilder);
        })

        return occurrences;
    }


    function updateRepeatablesIdAndLabel(action) {
        var occurrences = action.refresh().occurrences;
        var nb = occurrences.length, lastIndex = nb - 1;

        var minOccurs = retrieveOccurs(action, 'minOccurs');
        var maxOccurs = retrieveOccurs(action, 'maxOccurs');

        for (var idx = 0; idx < nb; idx++) {
            var elm = $(occurrences[idx]);

            var path = action.path.collection + '[' + idx + ']';
            var elmId = elm.attr('data-' + action.targetType);
            if (elmId !== path) {
                updateRepeatablesId(elm, idx);
            }

            if ('group' === action.targetType) {
                if (nb > 1) {
                    elm.find('.repeatable-label-index:first').text(' (' + (idx + 1) + '/' + nb + ')');
                } else {
                    elm.find('.repeatable-label-index:first').text('');
                }
            }
        }

        action.parent.toggleClass('repeatable-min-reached', nb <= retrieveOccurs(action, 'minOccurs'));
        action.parent.toggleClass('repeatable-max-reached', nb >= retrieveOccurs(action, 'maxOccurs'));
    }


    function findGroupId() {
        return $('input[name="group.id"]').val();
    }


    function buildActionParametersFromButton(button) {
        var targetType = button.attr('data-target');
        var htmlBlock = button.closest('[data-' + targetType + ']');
        var action = buildActionParametersFromHtmlBlock(htmlBlock, targetType);

        action.source = button;
        action.type = button.attr('data-type');

        return action;
    }

    function buildActionParametersFromHtmlBlock(htmlBlock, targetType) {
        if (undefined === targetType) {
            targetType = undefined !== htmlBlock.attr('data-group') ? 'group' : 'field';
        }

        var rootGroupId = findGroupId();
        var path = htmlBlock.attr('data-' + targetType);
        var fullPath = rootGroupId + '.' + path;
        var htmlBlockParent = htmlBlock.parent();
        var occurrencesPath = path.replace(/\[[^\]]*\]$/, '');

        return {
            /*
             * element key
             */
            'key': Utils.buildRuleKey(path),

            /*
             * source button of the action event
             */
            'source': null,

            /*
             * action type, ie "arrow-up", "arrow-down", ...
             * 
             * old name : actionType
             */
            'type': null,

            /*
             * "group" or "field"
             * 
             * old name : actionTarget
             */
            'targetType': targetType,

            /*
             * group or field HTML block targeted by action
             * 
             * old name : elementDiv
             */
            'target': htmlBlock,

            /*
             * parent HTML block
             * 
             * old name : elementDivParent
             */
            'parent': htmlBlockParent,

            /*
             * list of all occurrences in the same collection, ie matching the same path prefix on same level
             */
            'occurrences': null,

            /*
             * path informations
             */
            'path': {
                /*
                 * old name : path
                 */
                'simpleName': path,

                /*
                 * old name : fullPath
                 */
                'fullName': rootGroupId + '.' + path,
                
                /*
                 * old name : occurrencesPath
                 */
                'collection': occurrencesPath
            },

            /*
             * refresh dynamique properties, ie occurrences list.
             */
            'refresh': function () {
                this.occurrences = this.parent.find('> [data-' + targetType + '^="' + occurrencesPath + '"]');
                return this;
            }

        }.refresh();
    }



    function update(block) {
        var action = buildActionParametersFromHtmlBlock(block).refresh();
        action.minOccurs = retrieveOccurs(action, 'minOccurs');
        action.maxOccurs = retrieveOccurs(action, 'maxOccurs');

        if (action.minOccurs > action.occurrences.length) {
            Tools.add(action, action.minOccurs - action.occurrences.length);
        } else if (action.maxOccurs < action.occurrences.length) {
            action.target = action.occurrences.slice(action.maxOccurs);
            Tools.remove(action);
        } else {
            updateRepeatablesIdAndLabel(action);
        }

        console.log('[Repeatables.update]', action.key, ':', action);
    }



    function init() {
        $('div.repeatable').find('> [data-rule]:first').each(function () {
            update($(this));
        });
    }


    function updateMinOccurs(target, predicate) {
        update(target.fragment().first());
    }


    function updateMaxOccurs(target, predicate) {
        update(target.fragment().first());
    }


    Triggers.registerAction('field:minOccurs', updateMinOccurs);
    Triggers.registerAction('field:maxOccurs', updateMaxOccurs);
    Triggers.registerAction('group:minOccurs', updateMinOccurs);
    Triggers.registerAction('group:maxOccurs', updateMaxOccurs);

    $(function () {
        var $form = $('form[name="record"]');

        init();

        $form.on('click', '.repeatables-action', function(evt) {
            evt.preventDefault();
            evt.stopPropagation();

            var action = buildActionParametersFromButton($(this));
            action.minOccurs = retrieveOccurs(action, 'minOccurs');
            action.maxOccurs = retrieveOccurs(action, 'maxOccurs');

            console.log('[Repeatables] action :', action);

            if (repeatablesActions[action.type]) {
                repeatablesActions[action.type](action);
            }
        });

    });

    return {
        'init': init
    };

});
