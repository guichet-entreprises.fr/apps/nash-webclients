/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit.rest.v2.impl;

import java.util.List;

import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.ge.tracker.bean.StructuredSearchResult;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Passthrough service with Tracker bubble.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TrackerPassthroughRestServiceImpl implements IUidRestService {

    @Autowired
    @Qualifier("uidRestClient")
    private IUidRestService uidRestService;

    /**
     * {@inheritDoc}
     */
    @Override
    public UidNfo getUid(String value) {
        return this.uidRestService.getUid(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String addMessage(String ref, String content, String author) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String addReference(String uid, String ref, String author) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createUid(String author) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOrCreateUid(String arg0, String arg1) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getReferences(String arg0) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UidNfo getUidByRef(String arg0) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StructuredSearchResult<? extends Object> searchUidData(UriInfo arg0) {
        return null;
    }

}
