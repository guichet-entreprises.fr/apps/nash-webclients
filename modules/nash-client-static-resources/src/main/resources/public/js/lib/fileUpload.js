/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/template', 'lib/i18n' ], function($, Template, i18n) {

    var DLG_HTML = //
            '<div id="dlgFileUploadWarning" class="modal fade" role="dialog" tabindex="-1">' //
         +  '    <div class="modal-dialog" role="document">' //
         +  '        <div class="modal-content">' //
         +  '            <div class="modal-header">' //
         +  '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' //
         +  '                <h4 class="modal-title"></h4>' //
         +  '            </div>' //
         +  '            <div class="modal-body"></div>' //
         +  '        </div>' //
         +  '    </div>' //
         +  '</div>';


    function showModal(msg, args) {
        var dlg = $('#dlgFileUploadWarning');
        if (0 === dlg.length) {
            dlg = $(DLG_HTML).appendTo('body');
            $('.modal-title').html(i18n('Warning'));
        }

        $('.modal-body', dlg).html('<p>' + i18n(msg, args) + '</p>');

        dlg.modal();
    }

    function booleanValue(value) {
        if (undefined === value) {
            return undefined;
        } else {
            return null != value && (true === value || 'yes' == value.toLowerCase());
        }
    }

    function isTrue(value) {
        return booleanValue(value) || false;
        ;
    }
    
    function buildErrorMessage(xhr) {
        var msg, contentType = xhr.getResponseHeader('Content-type');

        if (contentType.startsWith('application/json')) {
            msg = JSON.parse(xhr.responseText);
        } else {
            msg = {
                type : 'ERROR',
                title : xhr.responseText
            };
        }

        return msg;
    }


    function isUndefinedOrTrue(value) {
        return undefined === value || booleanValue(value);
    }

    function button() {
        return $('<button type="button" class="btn btn-default"></button>');
    }

    function upload(elm) {
        var action, method, data;

        if (elm.get(0).nodeName == 'FORM') {
            action = elm.attr('action');
            method = elm.attr('method');
            data = new FormData(elm.get(0));
        } else {
            action = elm.data('action');
            method = 'post';
            data = new FormData();
            data.append(elm.attr('name'), elm.get(0).files[0]);
        }

        return $.ajax(action, {
            type : method,
            contentType : false,
            data : data,
            cache : false,
            processData : false
        }).fail(function(xhr, status, error) {
            var msg = buildErrorMessage(xhr);
            showModal(msg.title, (msg.args || null) );
        });
    }

    var defaultOptions = {
        action : null,
        ajax : false,
        autoUpload : false,
        control : true,
        remove : true,
        removeLabel : '<i class="fa fa-trash" />',
        selectLabel : '<i class="fa fa-file" />',
        selectClass : null,
        upload : true,
        uploadLabel : '<i class="fa fa-upload" />'
    };

    function optionsFromElement(input) {
        var opts = {
            action : input.data('action'),
            autoUpload : booleanValue(input.data('auto-upload')),
            control : booleanValue(input.data('control')),
            remove : booleanValue(input.data('remove')),
            removeLabel : input.data('remove-label'),
            selectLabel : input.data('select-label'),
            selectClass : input.data('select-class'),
            upload : booleanValue(input.data('upload')),
            uploadLabel : input.data('upload-label'),
        };

        var ajaxCallback = input.data('ajax');
        if (ajaxCallback) {
            if (typeof ajaxCallback === 'function') {
                opts.ajax = ajaxCallback;
            } else if (undefined === window[ajaxCallback]) {
                opts.ajax = isTrue(ajaxCallback);
            } else {
                opts.ajax = window[ajaxCallback];
            }
        }

        return opts;
    }

    $.fn.fileUpload = function(opts) {
        return this.each(function(idx, input) {
            var btn, inputFile = $(input), //
            inputFileClass = inputFile.attr('class'), //
            inputGroup = $('<div class="input-group" />'), //
            groupButton = $('<span class="input-group-btn" />').appendTo(inputGroup), //
            formControl;

            var localOpts = $.extend({}, defaultOptions, opts || optionsFromElement(inputFile));

            if (inputFileClass) {
                inputGroup.addClass(inputFileClass);
            }

            if (localOpts.control) {
                formControl = $('<input type="text" readonly="readonly" class="form-control" />').prependTo(inputGroup);
            }

            if (!localOpts.autoUpload && localOpts.remove) {
                btn = button().appendTo(groupButton) //
                .off('click.bs.fileInput') //
                .on('click.bs.fileInput', function(evt) {
                    inputFile.val('').trigger('change');
                });
                btn.html(localOpts.removeLabel);
            }

            btn = button().appendTo(groupButton) //
            .off('click.bs.fileInput') //
            .on('click.bs.fileInput', function(evt) {
                inputFile.trigger('click');
            });

            btn.html(localOpts.selectLabel);

            if (localOpts.selectClass) {
                btn.removeClass('btn-default').addClass(localOpts.selectClass);
            }

            if (!localOpts.autoUpload && localOpts.upload) {
                btn = button().appendTo(groupButton) //
                .off('click.bs.fileInput') //
                .on('click.bs.fileInput', function(evt) {
                    inputFile.get(0).form.submit();
                });
                btn.html(localOpts.uploadLabel);
            }

            inputGroup.insertBefore(inputFile.hide());

            if (localOpts.control) {
                inputFile.off('change.bs.fileInput').on('change.bs.fileInput', function(evt) {
                    if (evt.target.files.length > 0) {
                        formControl.val(evt.target.files[0].name);
                    } else {
                        formControl.val('');
                    }
                });
            }

            if (localOpts.autoUpload) {
                if (localOpts.ajax) {
                    inputFile.on('change.bs.fileInput', function(evt) {
                        var elm;
                        if (localOpts.action) {
                            elm = inputFile.data('action', localOpts.action);
                        } else {
                            elm = $(evt.target.form);
                        }

                        if (inputFile.val()) {
                            upload(elm, localOpts).always(function() {
                                inputFile.val('').trigger('change');
                            }).done(function(data) {
                                $.isFunction(localOpts.ajax) && localOpts.ajax(data);
                            });
                        }
                    });
                } else {
                    inputFile.on('change.bs.fileInput', function(evt) {
                        evt.target.form.submit();
                    });
                }
            }

        });
    };

    function init() {

        $('input[type="file"][data-toggle="file"]').fileUpload();

        $('#sidebar').on('hide.bs.collapse', function(evt) {
            $('#main').css('margin-left', 0);
        }).on('show.bs.collapse', function(evt) {
            $('#main').css('margin-left', '');
        });

    }

    return $.fn.fileUpload;

});
