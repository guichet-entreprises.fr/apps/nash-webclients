/**
 *
 */
package fr.ge.common.nash.webui.service.help.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.utils.test.AbstractTest;

/**
 * Class HelpControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class HelpControllerTest extends AbstractTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    /**
     * Test simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSimple() throws Exception {
        final byte[] expected = resourceAsBytes("/help/HelpControllerTest.html");
        this.mvc.perform(get("/help/HelpControllerTest")) //
                .andExpect(status().isOk()) //
                .andExpect(content().string(new String(expected, StandardCharsets.UTF_8).replace("\n\n", "\n")));
    }

    /**
     * Test not found.
     *
     * @throws Exception
     *             exception
     */

    @Test
    public void testNotFound() throws Exception {
        this.mvc.perform(get("/help/NotFoundTest")) //
                .andExpect(status().isOk()) //
                .andExpect(content().string("Not found"));
    }

}
