/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.markov.record.search.controller;

import java.util.Collections;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.ws.provider.RecordServiceProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;

/**
 * @author Christian Cougourdan
 */
@Controller("markovRecordSearchMarkovController")
public class MarkovController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarkovController.class);

    @Autowired
    private IRecordService recordRestService;

    @Autowired
    private Engine engine;

    @Value("${engine.user.type}")
    private String engineUserType;

    @RequestMapping(value = "/record/{code}/import", method = RequestMethod.GET)
    public String importRecord(final Model model, @PathVariable("code") final String code) {
        final SpecificationLoader loader = this.engine.loader(RecordServiceProvider.create(this.recordRestService, code));
        final FormSpecificationDescription description = loader.description();

        final StepElement step = Optional.ofNullable(description) //
                .map(FormSpecificationDescription::getSteps) //
                .map(StepsElement::getElements) //
                .orElseGet(Collections::emptyList) //
                .stream() //
                .filter(s -> StepStatusEnum.TO_DO.getStatus().equals(s.getStatus()) || StepStatusEnum.IN_PROGRESS.getStatus().equals(s.getStatus())) //
                .findFirst() //
                .orElse(null);

        if (step != null) {
            step.setUser(this.engineUserType);
            description.getSteps().getElements().set(step.getPosition(), step);
            loader.flush();
        }
        if (description != null) {
            model.addAttribute("code", code);
            model.addAttribute("step", loader.currentStepPosition());
            return "redirect:/record/{code}/{step}/page/0";
        }
        return "record/notfound/main";
    }

}
