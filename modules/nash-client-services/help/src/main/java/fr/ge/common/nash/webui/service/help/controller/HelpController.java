/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.help.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.pegdown.PegDownProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class HelpController.
 *
 * @author Christian Cougourdan
 */
@Controller
public class HelpController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HelpController.class);

    /** The peg down processor. */
    @Autowired
    private PegDownProcessor pegDownProcessor;

    /**
     * Default contructors.
     */
    public HelpController() {
        // Nothing to do
    }

    /**
     * Help.
     *
     * @param section
     *            the section
     * @return the string
     */
    @RequestMapping(value = "/help/{section}", method = RequestMethod.GET)
    @ResponseBody
    public String help(@PathVariable("section") final String section) {
        String helpAsHtml = "Not found";

        final InputStream helpInputStream = this.getClass().getResourceAsStream("/help/" + section + ".md");
        if (null == helpInputStream) {
            LOGGER.error("Rendering engine conditions test page : [{}] help not found", section);
        } else {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(helpInputStream, StandardCharsets.UTF_8))) {
                final String helpAsMarkdown = reader.lines().collect(Collectors.joining("\n"));
                helpAsHtml = this.pegDownProcessor.markdownToHtml(helpAsMarkdown);
            } catch (final IOException ex) {
                LOGGER.error("Rendering engine conditions test page : error while loading help content", ex);
            }
        }

        return helpAsHtml;
    }

    // @RequestMapping(value = "/dev", method = RequestMethod.GET)
    // public String dev() {
    // return "help/dev";
    // }

}
