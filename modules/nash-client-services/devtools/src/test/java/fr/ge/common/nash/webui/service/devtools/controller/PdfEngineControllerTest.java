/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.file.Paths;
import java.util.Map;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.common.io.Files;

import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class PdfEngineControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class PdfEngineControllerTest extends AbstractTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
    }

    /**
     * Test init without document.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInitWithoutDocument() throws Exception {
        this.mvc.perform(get("/engine/pdf")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("engine/pdf/main")) //
                .andExpect(model().attributeDoesNotExist("numberOfPages", "fields", "coordinatesRatios"));
    }

    /**
     * Test upload.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpload() throws Exception {
        final byte[] asBytes = this.resourceAsBytes("document.pdf");
        final MockHttpSession session = new MockHttpSession();
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/engine/pdf") //
                .file(new MockMultipartFile("file", "Cerfa_XXX", "application/pdf", asBytes)) //
                .session(session);

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/engine/pdf"));

        assertEquals(session.getAttribute("devToolPdf"), asBytes);
    }

    /**
     * Test init with document.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInitWithDocument() throws Exception {
        final MvcResult actual = this.mvc.perform(get("/engine/pdf").sessionAttr("devToolPdf", this.resourceAsBytes("document.pdf"))) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("engine/pdf/main")) //
                .andReturn();

        final Map<String, Object> model = actual.getModelAndView().getModel();

        assertEquals(model.get("numberOfPages"), 2);
    }

    /**
     * Test display page.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayPage() throws Exception {
        final MockHttpSession session = new MockHttpSession();
        session.setAttribute("devToolPdf", this.resourceAsBytes("document.pdf"));

        this.mvc.perform(get("/engine/pdf/display").session(session)) //
                .andExpect(status().isOk());

        this.mvc.perform(get("/engine/pdf/display").session(session).param("pageIndex", "0")) //
                .andExpect(status().isOk());

        this.mvc.perform(get("/engine/pdf/display").session(session).param("pageIndex", "1")) //
                .andExpect(status().isOk());

        this.mvc.perform(get("/engine/pdf/display").session(session).param("pageIndex", "2")) //
                .andExpect(status().is5xxServerError());

    }

    /**
     * Tests {@link PdfEngineController#generateAutoForm()}.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testGenerateAutoForm() throws Exception {
        final MockHttpSession session = new MockHttpSession();
        session.setAttribute("devToolPdf", this.resourceAsBytes("document2.pdf"));

        final MvcResult result = this.mvc.perform(get("/engine/pdf/autoForm").session(session)) //
                .andExpect(status().isOk()).andReturn();

        final byte[] zipContent = result.getResponse().getContentAsByteArray();
        final IProvider provider = new ZipProvider(zipContent);

        assertTrue(provider.asBytes("description.xml").length > 0);
        assertTrue(provider.asBytes("4-finish/greetings.xml").length > 0);
        assertTrue(provider.asBytes("1-data/data.xml").length > 0);
        assertTrue(provider.asBytes("3-review/preprocess.js").length > 0);
        assertTrue(provider.asBytes("3-review/models/model.pdf").length > 0);

        Files.write(provider.asBytes("1-data/data.xml"), Paths.get("target/data.xml").toFile());
        Files.write(provider.asBytes("3-review/preprocess.js"), Paths.get("target/preprocess.js").toFile());
    }

    @Test
    public void testAllPdfFieldType() throws Exception {
        final MockHttpSession session = new MockHttpSession();
        session.setAttribute("devToolPdf", this.resourceAsBytes("all.pdf"));

        final MvcResult result = this.mvc.perform(get("/engine/pdf/autoForm").session(session)) //
                .andExpect(status().isOk()).andReturn();

        final byte[] zipContent = result.getResponse().getContentAsByteArray();
        final IProvider provider = new ZipProvider(zipContent);

        assertTrue(provider.asBytes("description.xml").length > 0);
        assertTrue(provider.asBytes("1-data/data.xml").length > 0);
        assertTrue(provider.asBytes("3-review/preprocess.js").length > 0);
        assertTrue(provider.asBytes("3-review/models/model.pdf").length > 0);

        Files.write(provider.asBytes("1-data/data.xml"), Paths.get("target/data.xml").toFile());
        Files.write(provider.asBytes("3-review/preprocess.js"), Paths.get("target/preprocess.js").toFile());
    }
}
