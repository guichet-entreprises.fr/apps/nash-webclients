/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/error', 'lib/template', 'lib/remoteLoader' ], function($, error, Template) {

    $(function() {

        $('#records').on('click', 'td.code > a', function(evt) {
            var $a = $(this);
            var code = $a.attr('href').substr(1);

            evt.preventDefault();

            $('form[name="exprForm"] input[name="record"]').val(code);

            $('#helpContainer').removeClass('in').attr('aria-expanded', 'false');
            $('#recordContainer').addClass('in').attr('aria-expanded', 'true');

            $.ajax(baseUrl + '/record/' + code + '/1/values').done(function(html) {
                var groupId = $('form[name="exprForm"] input[name="grp"]').val();
                $('#recordContainer').html(html);
                var tr = $('#recordContainer tr[data-group-id]').filter(function(idx, tr) {
                    return $(tr).data('group-id') == groupId;
                });
                if (tr.length > 0) {
                    tr.first().addClass('active');
                } else {
                    $('form[name="exprForm"] input[name="grp"]').val('');
                }
            });
        });

        $('#recordContainer').on('click', 'tr[data-group-id]', function(evt) {
            var $tr = $(this);
            var specId = $tr.data('spec-id');
            var groupId = $tr.data('group-id');
            $('form[name="exprForm"] input[name="spc"]').val(specId);
            $('form[name="exprForm"] input[name="grp"]').val(groupId);
            $('#recordContainer tr[data-group-id].active').removeClass('active');
            $tr.addClass('active');
        });

        $('form[name="exprForm"]').on('submit', function(evt) {
            var $form = $(this);
            var data = new FormData($form.get(0));

            evt.preventDefault();

            $('input[name="result"]', $form).val(null);

            return $.ajax($form.attr('action'), {
                type : $form.attr('method'),
                contentType : false,
                data : data,
                cache : false,
                processData : false
            }).done(function(json) {
                $('input[name="result"]', $form).val(json.boolean);
                error.displayExceptions(json.displayableExceptions);
            }).fail(function(xhr, status, error) {
                var msg = buildErrorMessage(xhr);
                $('#alertPlaceholder').html(Template.find('alert').render({
                    type : 'warning',
                    message : '<i class="fa fa-exclamation-triangle fa-lg"></i> &nbsp; ' + msg.title
                }));
            });
        });
    });

});
