/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.support.record.dashboard.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.MessageFormat;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.webui.core.bean.ErrorMessage;
import fr.ge.common.nash.ws.provider.RecordServiceProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;

/**
 * @author Christian Cougourdan
 */
@Controller("supportRecordDashboardMarkovController")
public class MarkovController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarkovController.class);

    private static final String ERROR_MARKOV = "Error when exporting record to markov,Markov service is temporarily unavailable";

    @Autowired
    private IRecordService recordRestService;

    @Autowired
    private IQueueMessageRestService queueMessageRestService;

    @Value("${engine.user.type}")
    private String engineUserType;

    @Autowired
    private Engine engine;

    @RequestMapping(value = "/record/{code}/export", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<Object> exportRecord(final Model model, @PathVariable("code") final String code) {
        final SpecificationLoader loader = this.engine.loader(RecordServiceProvider.create(this.recordRestService, code));

        try {
            if (null != loader && null != loader.stepsMgr()) {
                loader.stepsMgr().current().setStatus(StepStatusEnum.DONE.getStatus());
                loader.flush();
                LOGGER.debug("Update current step status to 'done'");

                final InputStream record = new ByteArrayInputStream(loader.getProvider().asBytes());
                this.queueMessageRestService.upload("AWAIT", true, new Attachment("file", "application/octet-stream", record));
                this.recordRestService.remove(code);
            }
        } catch (NotFoundException | ProcessingException | Fault e) {
            LOGGER.error(MessageFormat.format("Error calling markov for record \"{0}\" : {1}", code, e.getMessage()));
            return new ResponseEntity<>(new ErrorMessage<>(ERROR_MARKOV, HttpStatus.SERVICE_UNAVAILABLE), HttpStatus.SERVICE_UNAVAILABLE);
        }
        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/record/{code}/remove", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<Object> removeRecord(final Model model, @PathVariable("code") final String code) {
        try {
            this.recordRestService.remove(code);
        } catch (NotFoundException | ProcessingException | Fault e) {
            LOGGER.error(MessageFormat.format("Error calling markov for record \"{0}\" : {1}", code, e.getMessage()));
            return new ResponseEntity<>(new ErrorMessage<>(ERROR_MARKOV, HttpStatus.SERVICE_UNAVAILABLE), HttpStatus.SERVICE_UNAVAILABLE);
        }
        return ResponseEntity.ok("ok");
    }

}
