/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.ws.v1.service.IRecordService;

/**
 * The Class RecordDataControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordDataControllerTest extends AbstractRecordControllerTest {

    /** record rest service. */
    @Autowired
    private IRecordService recordRestService;

    /**
     * Test show values.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testShowValues() throws Exception {
        this.mvc.perform(get("/record/{code}/1/values", RECORD_UID)) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("record/data/inner")) //
                .andExpect(model().attributeExists("specs"));
    }

    /**
     * Test show values error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testShowValuesError() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/record/{code}/1/values", RECORD_UID);

        when(this.recordRestService.download(any(String.class), any(String.class))).thenThrow(new TechnicalException("oups"));

        final MvcResult actual = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is5xxServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/default/main")) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("uuid")) //
                .andReturn();

        System.err.println(actual.getResponse().getContentAsString());
    }

}
