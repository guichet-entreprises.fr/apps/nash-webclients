requirejs([ 'jquery', 'lib/func/renderer', 'lib/pagination' ], function ($, Renderer) {

    const PAGE_SIZE = 10;

    function init(elementTemplate) {
        var form = $('form[name="modelSearchTerms"]');
        var input = $('input[name="searchTerms"]', form);
        var searchUrl = elementTemplate.data('search');
        var pageSize = elementTemplate.data('page-size');
        var template = $('<div />').append(elementTemplate.clone() //
                .removeAttr('data-search')
                .removeAttr('data-page-size') //
            ).html();
        var root = elementTemplate.parent();
        var render = Renderer(template);

        var searchTimeoutId;
        input.on('keyup', function (evt) {
            clearTimeout(searchTimeoutId);
            searchTimeoutId = setTimeout(function () {
                root.pagination('refresh', 0);
            }, 500);
        });

        form.on('submit', function (evt) {
            evt.preventDefault();
            clearTimeout(searchTimeoutId);
            root.pagination('refresh', 0);
        });

        root.data('pageSize', undefined === pageSize ? PAGE_SIZE : pageSize);
        root.data('pageOffset', 0);

        root.empty().show().pagination({
            source : function (pageOffset, cb) {
                $.ajax(searchUrl, {
                    data: {
                        startIndex: root.data('pageSize') * root.data('pageOffset'),
                        maxResults: root.data('pageSize'),
                        orders: [ 'priority:desc', 'reference_id:asc' ],
                        searchTerms: input.val()
                    },
                    dataType: 'json'
                }).done(function (searchResult) {
                    cb(searchResult);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    console.error(arguments);
                });
            },
            renderItem : function (item, idx) {
                return render(item);
            },
            showPagination: true,
            navbarSize: 7
        });
    }

    $(function () {
        $('[data-search]').each(function (idx, elm) { return init($(elm)); });
    });

});
