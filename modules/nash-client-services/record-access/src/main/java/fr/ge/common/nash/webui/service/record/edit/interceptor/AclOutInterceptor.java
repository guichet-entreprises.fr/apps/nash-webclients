/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.interceptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.OutInterceptors;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.authentication.manager.AccountManager;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.nash.ws.v1.support.feature.NashInterceptor;
import fr.ge.common.utils.CoreUtil;

@Provider
@OutInterceptors
public class AclOutInterceptor extends AbstractPhaseInterceptor<Message> implements NashInterceptor<Message> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private HabilitationService service;

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    @Autowired
    private AccountManager accountManager;

    public AclOutInterceptor() {
        super(Phase.PREPARE_SEND);
    }

    @Override
    public void handleMessage(final Message message) throws Fault {
        final String userId = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);

        if (null != userId) {
            final Map<String, List<String>> headers = CoreUtil.cast(message.get(Message.PROTOCOL_HEADERS));
            try {
                final String roles = this.findUserRoles(userId);
                if (null != roles) {
                    headers.put("X-Roles", Arrays.asList(roles));
                    accountManager.persistUserContext(AccountContext.currentUser().setRoles(roles));
                }
            } catch (final IOException ex) {
                throw new Fault(ex);
            }
        }
    }

    @Cacheable(key = "#userId")
    private String findUserRoles(final String userId) throws IOException {
        final Map<String, List<String>> roles = new HashMap<>(this.service.getRoles(userId));
        roles.put(StringUtils.join(this.defaultPrefixAuthor, userId), Arrays.asList("*"));

        return mapper.writeValueAsString(roles);
    }

    @Override
    public int getType() {
        return NashInterceptor.OUT;
    }

}
