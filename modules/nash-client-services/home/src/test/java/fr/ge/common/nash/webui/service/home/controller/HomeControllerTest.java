/**
 *
 */
package fr.ge.common.nash.webui.service.home.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;

/**
 * The Class HomeControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class HomeControllerTest extends AbstractTest {

    private static final String SPEC_UID = "2018-03-ABC-DEF-27";

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService service;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /** The user service. */
    @Autowired
    private HabilitationService habilitationService;

    /** Authentification configuration. **/
    @Autowired
    private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.service, this.habilitationService, this.authentificationConfigurationWebBean);
    }

    /**
     * Test home.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHome() throws Exception {
        Map<String, List<String>> rights = new HashMap<String, List<String>>();
        rights.put("all", Arrays.asList("all"));
        when(this.habilitationService.getRoles(Mockito.anyString())).thenReturn(rights);

        this.mvc.perform(MockMvcRequestBuilders.get("/")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("home/main"));
    }

    @Test
    public void testModelSearchEnhanced() throws Exception {
        final SpecificationSearchResult searchResult = new SpecificationSearchResult(0, 6);
        Map<String, String> details = new HashMap<>();
        details.put("description", "Déclaration en mairie des meublés de tourisme");
        JsonNode json = new ObjectMapper().convertValue(details, JsonNode.class);
        searchResult.setContent(Arrays.asList(new SpecificationInfoBean().setCode(SPEC_UID).setTitle("Déclarer un gîte").setDetails(json)));

        when(this.service.fullTextSearch(any(long.class), any(long.class), any(), any(), any())) //
                .thenReturn(searchResult);

        when(this.service.download(SPEC_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("fullTextSearch/description.xml")).build());

        final MvcResult mvcResult = this.mvc.perform(get("/model/search/enhanced/data").accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk()) //
                .andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        final JsonNode actual = new ObjectMapper().readTree(mvcResult.getResponse().getContentAsByteArray());

        final JsonNode firstElement = actual.get("content").get(0);
        assertThat(firstElement.get("action").asText(), equalTo("Déclarer un gîte"));
        assertThat(firstElement.get("description").asText(), equalTo("Déclaration en mairie des meublés de tourisme"));
    }

    @Test
    public void testSearchEnhanced() throws Exception {
        final List<FormSpecificationDescription> models = new ArrayList<>();

        final byte[] model01AsBytes = this.resourceAsBytes("fullTextSearch/description-01.xml");
        models.add(JaxbFactoryImpl.instance().unmarshal(model01AsBytes, FormSpecificationDescription.class));

        final byte[] model02AsBytes = this.resourceAsBytes("fullTextSearch/description-01.xml");
        models.add(JaxbFactoryImpl.instance().unmarshal(model02AsBytes, FormSpecificationDescription.class));

        final Calendar now = Calendar.getInstance();

        Map<String, String> details = new HashMap<>();
        details.put("description", "Model 01 detailed description");
        JsonNode json = new ObjectMapper().convertValue(details, JsonNode.class);

        final SpecificationSearchResult providedSearchResult = new SpecificationSearchResult(10L, 20L);
        providedSearchResult.setTotalResults(12L);
        providedSearchResult.setContent( //
                Arrays.asList( //
                        new SpecificationInfoBean() //
                                .setCode(models.get(0).getFormUid()) //
                                .setTitle(models.get(0).getTitle()) //
                                .setCreated(now) //
                                .setUpdated(now) //
                                .setTagged(true) //
                                .setDetails(json)//
                                .setReference(models.get(0).getReference().getCode()) //
                                .setRevision(models.get(0).getReference().getRevision()), //
                        new SpecificationInfoBean() //
                                .setCode(models.get(1).getFormUid()) //
                                .setTitle(models.get(1).getTitle()) //
                                .setCreated(now) //
                                .setUpdated(now) //
                                .setTagged(true) //
                                .setDetails(json)//
                                .setReference(models.get(1).getReference().getCode()) //
                                .setRevision(models.get(1).getReference().getRevision()) //
                ) //
        );

        when(this.service.fullTextSearch(eq(10L), eq(20L), any(), any(), eq("testing"))).thenReturn(providedSearchResult);
        when(this.service.download(providedSearchResult.getContent().get(0).getCode(), "description.xml")).thenReturn(Response.ok(model01AsBytes).build());
        when(this.service.download(providedSearchResult.getContent().get(1).getCode(), "description.xml")).thenReturn(Response.ok(model02AsBytes).build());

        final MvcResult result = this.mvc.perform( //
                get("/model/search/enhanced/data") //
                        .accept(MediaType.APPLICATION_JSON) //
                        .param("startIndex", Long.toString(providedSearchResult.getStartIndex())) //
                        .param("maxResults", Long.toString(providedSearchResult.getMaxResults())) //
                        .param("searchTerms", "testing") //
                        .param("filters", "tagged:yes") //
        ) //
                .andExpect(status().isOk()) //
                .andReturn();

        final JsonNode actual = new ObjectMapper().readTree(result.getResponse().getContentAsString());
        assertThat(actual.get("startIndex").asLong(), equalTo(providedSearchResult.getStartIndex()));
        assertThat(actual.get("maxResults").asLong(), equalTo(providedSearchResult.getMaxResults()));
        assertThat(actual.get("totalResults").asLong(), equalTo(providedSearchResult.getTotalResults()));
        assertThat(actual.get("content").size(), equalTo(2));

        for (int idx = 0; idx < providedSearchResult.getContent().size(); idx++) {
            final SpecificationInfoBean expected = providedSearchResult.getContent().get(idx);
            final JsonNode item = actual.get("content").get(idx);
            assertThat(item.get("title").asText(), equalTo(expected.getTitle()));
            assertThat(item.get("reference").asText(), equalTo(expected.getReference()));
            assertThat(item.get("description").asText(), equalTo(models.get(idx).getDescription()));
        }
    }

    @Test
    public void testHomeError() throws Exception {
        when(this.service.fullTextSearch(any(long.class), any(long.class), any(), any(), any())) //
                .thenThrow(new TechnicalException("oups"));

        this.mvc.perform(get("/model/search/enhanced/data").accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().is5xxServerError()) //
                .andExpect(view().name("commons/error/default/main")) //
                .andExpect(model().attributeExists("uuid"));
    }

    @Test
    public void testModelSearchEnhancedWithIncompleteSpec() throws Exception {
        final SpecificationSearchResult searchResult = new SpecificationSearchResult(0, 6);
        Map<String, String> details = new HashMap<>();
        // details.put("description", "Model 01 detailed description");
        JsonNode json = new ObjectMapper().convertValue(details, JsonNode.class);

        searchResult.setContent(Arrays.asList(new SpecificationInfoBean().setCode(SPEC_UID).setDetails(json)));

        when(this.service.fullTextSearch(any(long.class), any(long.class), any(), any(), any())) //
                .thenReturn(searchResult);

        when(this.service.download(SPEC_UID, "description.xml")).thenReturn(Response.noContent().build());

        final MvcResult mvcResult = this.mvc.perform(get("/model/search/enhanced/data").accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk()) //
                .andReturn();

        final JsonNode actual = new ObjectMapper().readTree(mvcResult.getResponse().getContentAsByteArray());

        final JsonNode firstElement = actual.get("content").get(0);
        assertTrue(firstElement.get("action").isNull());
        assertTrue(firstElement.get("description").isNull());
    }

    /**
     * Test home profiler.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHomeProfiler() throws Exception {
        when(this.authentificationConfigurationWebBean.isCookieAnonymousAllow()).thenReturn(true);

        this.mvc.perform(MockMvcRequestBuilders.get("/")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("home/main"));

        verify(this.habilitationService, never()).getRoles(any(String.class));
    }
}
