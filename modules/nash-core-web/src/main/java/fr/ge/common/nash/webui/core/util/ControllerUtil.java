/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class ControllerUtil.
 *
 * @author Christian Cougourdan
 */
public final class ControllerUtil {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerUtil.class);

    /**
     * Default constructor.
     */
    private ControllerUtil() {
        // Nothing to do
    }

    /**
     * Extract path suffix.
     *
     * @param request
     *            the request
     * @param queryPath
     *            the query path
     * @return the string
     */
    public static String extractPathSuffix(final HttpServletRequest request, final String queryPath) {
        final String requestPath = Optional.ofNullable(request.getPathInfo()).orElse(request.getServletPath());
        return requestPath.replaceFirst("/record/[^/]+/[^/]+" + queryPath, "");
    }

    /**
     * Translate id to html.
     *
     * @param id
     *            the id
     * @return the string
     */
    public static String translateIdToHtml(final String id) {
        return id.replaceAll("[.]", "_");
    }

    /**
     * Translate id from html.
     *
     * @param id
     *            the id
     * @return the string
     */
    public static String translateIdFromHtml(final String id) {
        final String res = id.replaceAll("[_/-]+", ".") //
                .replaceAll("\\.([0-9])+", "[$1]");
        try {
            return URLDecoder.decode(res, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            LOGGER.warn("Could not convert id '{}' from HTML", id, e);
            return res;
        }
    }

}
