/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.ws.v1.bean.EntryInfoBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class RecordControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordControllerTest extends AbstractTest {

    /** The Constant RECORD_UID. */
    private static final String RECORD_UID = "2016-03-AAA-AAA-27";

    /** The mvc. */
    private MockMvc mvc;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        AccountContext.currentUser(null);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.recordService);

        when(this.recordService.download(RECORD_UID, Engine.FILENAME_DESCRIPTION)).thenReturn(Response.ok(this.resourceAsBytes(Engine.FILENAME_DESCRIPTION)).build());
    }

    /**
     * Test raw data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRawData() throws Exception {
        final byte[] recordAsByteArray = this.resourceAsBytes("record.xml");

        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(recordAsByteArray).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/view/1-data/data.xml", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.content().bytes(recordAsByteArray));
    }

    /**
     * Test raw unknown.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRawUnknown() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/view/___", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test raw data not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRawDataNotFound() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/view/1-data/data.xml", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test raw default.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Ignore
    public void testRawDefault() throws Exception {
        final String resourcePath = "css/data.css";
        final byte[] contentAsByteArray = this.resourceAsBytes("/" + resourcePath);

        when(this.recordService.download(RECORD_UID, resourcePath))
                .thenReturn(Response.ok(contentAsByteArray).header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", resourcePath)).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/view/" + resourcePath, RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=%s", resourcePath)))
                .andExpect(MockMvcResultMatchers.content().bytes(contentAsByteArray));
    }

    /**
     * Test download unknown.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknown() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/record/{code}/download.zip", RECORD_UID);

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test download.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownload() throws Exception {
        final byte[] recordAsByteArray = this.resourceAsBytes("record.xml");

        when(this.recordService.download(RECORD_UID)).thenReturn(Response.ok(recordAsByteArray).build());

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/record/{code}/download.zip", RECORD_UID);

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s.zip", RECORD_UID)))
                .andExpect(MockMvcResultMatchers.content().bytes(recordAsByteArray));

        verify(this.recordService).download(RECORD_UID);
    }

    @Test
    public void testDownloadDocumentsNoneAndNotValidate() throws Exception {
        final String code = "2016-03-REC-AAA-27";
        Arrays.asList( //
                "description.xml", //
                "step01/data.xml", "step01/uploaded/grp01.fld03-1-test01.txt", //
                "step02/data.xml", "step02/uploaded/grp01.fld01-1-test01.txt", "step02/uploaded/grp01.fld01-2-test02.txt", "step02/uploaded/grp01.fld02-1-test03.txt", //
                "step03/data.xml" //
        ).forEach(elm -> when(this.recordService.download(eq(code), eq(elm))).thenReturn(Response.ok(this.resourceAsBytes("documents/" + elm)).build()));

        this.mvc.perform(get("/record/{code}/documents.zip", code)) //
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDownloadDocumentsNoneButValidate() throws Exception {
        final String code = "2016-03-REC-AAA-27";
        when(this.recordService.download(eq(code), eq("description.xml"))).thenReturn(Response.ok(this.resourceAsBytes("documents/description-all-done.xml")).build());
        Arrays.asList( //
                "step01/data.xml", "step01/uploaded/grp01.fld03-1-test01.txt", //
                "step02/data.xml", "step02/uploaded/grp01.fld01-1-test01.txt", "step02/uploaded/grp01.fld01-2-test02.txt", "step02/uploaded/grp01.fld02-1-test03.txt", //
                "step03/data.xml" //
        ).forEach(elm -> when(this.recordService.download(eq(code), eq(elm))).thenReturn(Response.ok(this.resourceAsBytes("documents/" + elm)).build()));

        final MvcResult response = this.mvc.perform(get("/record/{code}/documents.zip", code)) //
                .andExpect(status().isOk()) //
                .andReturn();

        final byte[] bytes = response.getResponse().getContentAsByteArray();

        assertThat(ZipUtil.entries(bytes), //
                contains( //
                        "grp01.fld01-1-test01.txt", //
                        "grp01.fld01-2-test02.txt", //
                        "grp01.fld02-1-test03.txt" //
                ) //
        );
    }

    @Test
    public void testDownloadDocumentsByMeta() throws Exception {
        final String code = "2016-03-REC-AAA-27";
        Arrays.asList( //
                "description.xml", "meta.xml", //
                "step01/data.xml", "step01/uploaded/grp01.fld03-1-test01.txt", //
                "step02/data.xml", "step02/uploaded/grp01.fld01-1-test01.txt", "step02/uploaded/grp01.fld01-2-test02.txt", "step02/uploaded/grp01.fld02-1-test03.txt", //
                "step03/data.xml" //
        ).forEach(elm -> when(this.recordService.download(eq(code), eq(elm))).thenReturn(Response.ok(this.resourceAsBytes("documents/" + elm)).build()));

        final MvcResult response = this.mvc.perform(get("/record/{code}/documents.zip", code)) //
                .andExpect(status().isOk()) //
                .andReturn();

        final byte[] bytes = response.getResponse().getContentAsByteArray();

        assertThat(ZipUtil.entries(bytes), contains("grp01.fld03-1-test01.txt"));
    }

    /**
     * Test files.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFiles() throws Exception {
        when(this.recordService.load(RECORD_UID)).thenReturn(new RecordInfoBean().setCode(RECORD_UID).setEntries( //
                Arrays.asList( //
                        new EntryInfoBean().setName(Engine.FILENAME_DESCRIPTION), //
                        new EntryInfoBean().setName("0-context/context.xml"), //
                        new EntryInfoBean().setName("1-data/data.xml"), //
                        new EntryInfoBean().setName("2-attachment/preprocess.xml"), //
                        new EntryInfoBean().setName("1-data/data.xml".replace("\\.[^.]+", ".css")), //
                        new EntryInfoBean().setName("0-context/context.xml".replace("\\.[^.]+", ".css")) //
                ) //
        ));

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/record/{code}/files", RECORD_UID);
        final byte[] actualAsBytes = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andReturn().getResponse().getContentAsByteArray();

        final List<String> actual = new ObjectMapper().readValue(actualAsBytes, new TypeReference<List<String>>() {
        });

        assertThat(actual, //
                allOf( //
                        allOf( //
                                hasItem(Engine.FILENAME_DESCRIPTION), //
                                hasItem("0-context/context.xml"), //
                                hasItem("1-data/data.xml"), //
                                hasItem("2-attachment/preprocess.xml") //
                        ), //
                        allOf( //
                                hasItem("1-data/data.xml".replace("\\.[^.]+", ".css")), //
                                hasItem("0-context/context.xml".replace("\\.[^.]+", ".css")) //
                        ) //
                ) //
        );

        assertThat(actual, hasSize(6));
    }

    /**
     * Test files filtered.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFilesFiltered() throws Exception {
        when(this.recordService.load(RECORD_UID)).thenReturn(new RecordInfoBean().setCode(RECORD_UID).setEntries( //
                Arrays.asList( //
                        new EntryInfoBean().setName(Engine.FILENAME_DESCRIPTION), //
                        new EntryInfoBean().setName("0-context/context.xml"), //
                        new EntryInfoBean().setName("1-data/data.xml"), //
                        new EntryInfoBean().setName("2-attachment/preprocess.xml"), //
                        new EntryInfoBean().setName("1-data/data.xml".replaceAll("\\.[^.]+$", ".css")), //
                        new EntryInfoBean().setName("0-context/context.xml".replaceAll("\\.[^.]+$", ".css")) //
                ) //
        ));

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/record/{code}/files", RECORD_UID).param("filter", "xml");
        final byte[] actualAsBytes = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andReturn().getResponse().getContentAsByteArray();

        final List<String> actual = new ObjectMapper().readValue(actualAsBytes, new TypeReference<List<String>>() {
        });

        assertThat(actual, //
                allOf( //
                        hasItem(Engine.FILENAME_DESCRIPTION), //
                        hasItem("0-context/context.xml"), //
                        hasItem("1-data/data.xml"), //
                        hasItem("2-attachment/preprocess.xml") //
                ) //
        );

        assertThat(actual, hasSize(4));
    }

    /**
     * Test edit specific page with step id name.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditSpecificPageWithStepId() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/step/id/{stepId}", RECORD_UID, "0-context")) //
                .andExpect(MockMvcResultMatchers.view().name("redirect:/record/{code}/{step}/page/0")).andExpect(MockMvcResultMatchers.model().attribute("code", RECORD_UID))
                .andExpect(MockMvcResultMatchers.model().attribute("step", 0));
    }

}
