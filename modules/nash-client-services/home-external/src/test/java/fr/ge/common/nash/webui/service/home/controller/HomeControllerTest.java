/**
 *
 */
package fr.ge.common.nash.webui.service.home.controller;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class HomeControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class HomeControllerTest extends AbstractTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
    }

    /**
     * Test home.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHome() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/")) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("http://welcome"));
    }
}
