/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.core.provider;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.ws.v1.bean.EntryInfoBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class RecordServiceProviderTest.
 *
 * @author Christian Cougourdan
 */
public class RecordServiceProviderTest extends AbstractTest {

    /** La constante UID_RECORD. */
    private static final String UID_RECORD = "2016-12-REC-AAA-42";

    /** service. */
    private IRecordService service;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.service = mock(IRecordService.class);
    }

    /**
     * Test load.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoad() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final FileEntry actual = provider.load(resourcePath);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertThat(actual.asBytes(), equalTo(resourceAsBytes));
        assertEquals(resourcePath, actual.name());
    }

    /**
     * Test load twice.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadTwice() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final FileEntry actualFirst = provider.load(resourcePath);
        final FileEntry actualSecond = provider.load(resourcePath);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertEquals(actualFirst, actualSecond);
        assertThat(actualFirst.asBytes(), equalTo(resourceAsBytes));
        assertEquals(resourcePath, actualFirst.name());
    }

    /**
     * Test load no response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadNoResponse() throws Exception {
        final String resourcePath = "description.xml";

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final FileEntry actual = provider.load(resourcePath);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertNull(actual);
    }

    /**
     * Test load not content response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadNotContentResponse() throws Exception {
        final String resourcePath = "description.xml";

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.noContent().build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final FileEntry actual = provider.load(resourcePath);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertNull(actual);
    }

    /**
     * Test load as bytes.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadAsBytes() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final byte[] actual = provider.asBytes(resourcePath);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertThat(actual, equalTo(resourceAsBytes));
    }

    /**
     * Test load as bean.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadAsBean() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final FormSpecificationDescription actual = provider.asBean(resourcePath, FormSpecificationDescription.class);

        verify(this.service).download(UID_RECORD, resourcePath);

        assertThat(actual, //
                allOf( //
                        hasProperty("formUid", equalTo("2016-12-SPE-AAA-42")), //
                        hasProperty("recordUid", equalTo("2016-12-REC-AAA-42")), //
                        hasProperty("title", equalTo("Specification 01")) //
                ) //
        );
    }

    /**
     * Test save.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSave() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        provider.save(resourcePath, resourceAsBytes);

        verify(this.service).upload(UID_RECORD, resourcePath, resourceAsBytes);
    }

    /**
     * Test save after loaded with.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSaveAfterLoadedWith() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());
        when(this.service.upload(UID_RECORD, resourcePath, resourceAsBytes)).thenReturn(Response.ok().build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        provider.load(resourcePath);
        provider.save(resourcePath, resourceAsBytes);

        verify(this.service).upload(UID_RECORD, resourcePath, resourceAsBytes);
    }

    /**
     * Test save after loaded with no response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSaveAfterLoadedWithNoResponse() throws Exception {
        final String resourcePath = "description.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/" + resourcePath);

        when(this.service.download(UID_RECORD, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        provider.load(resourcePath);
        provider.save(resourcePath, resourceAsBytes);

        verify(this.service).upload(UID_RECORD, resourcePath, resourceAsBytes);
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemove() throws Exception {
        final String resourcePath = "description.xml";

        when(this.service.remove(UID_RECORD, resourcePath)).thenReturn(Response.ok().build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final boolean actual = provider.remove(resourcePath);

        verify(this.service).remove(UID_RECORD, resourcePath);
        assertTrue(actual);
    }

    /**
     * Test remove no response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveNoResponse() throws Exception {
        final String resourcePath = "description.xml";

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final boolean actual = provider.remove(resourcePath);

        verify(this.service).remove(UID_RECORD, resourcePath);
        assertFalse(actual);
    }

    /**
     * Test remove not modified response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveNotModifiedResponse() throws Exception {
        final String resourcePath = "description.xml";

        when(this.service.remove(UID_RECORD, resourcePath)).thenReturn(Response.notModified().build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final boolean actual = provider.remove(resourcePath);

        verify(this.service).remove(UID_RECORD, resourcePath);
        assertFalse(actual);
    }

    /**
     * Test remove unknown response.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveUnknownResponse() throws Exception {
        final String resourcePath = "description.xml";

        when(this.service.remove(UID_RECORD, resourcePath)).thenReturn(Response.status(Status.BAD_REQUEST).build());

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final boolean actual = provider.remove(resourcePath);

        verify(this.service).remove(UID_RECORD, resourcePath);
        assertFalse(actual);
    }

    @Test
    public void testResources() throws Exception {
        final List<String> allResources = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");
        final RecordInfoBean infoBean = new RecordInfoBean() //
                .setCode(UID_RECORD) //
                .setEntries(allResources.stream().map(elm -> new EntryInfoBean().setName(elm)).collect(Collectors.toList()));

        when(this.service.load(UID_RECORD)).thenReturn(infoBean);

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final List<String> actual = provider.resources(null);

        assertThat(actual, containsInAnyOrder(allResources.stream().map(Matchers::equalTo).collect(Collectors.toList())));
    }

    @Test
    public void testResourcesFile() throws Exception {
        final List<String> allResources = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");
        final RecordInfoBean infoBean = new RecordInfoBean() //
                .setCode(UID_RECORD) //
                .setEntries(allResources.stream().map(elm -> new EntryInfoBean().setName(elm)).collect(Collectors.toList()));

        when(this.service.load(UID_RECORD)).thenReturn(infoBean);

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final List<String> actual = provider.resources(allResources.get(3));

        assertThat(actual, containsInAnyOrder(allResources.get(3)));
    }

    @Test
    public void testResourcesFileNone() throws Exception {
        final List<String> allResources = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");
        final RecordInfoBean infoBean = new RecordInfoBean() //
                .setCode(UID_RECORD) //
                .setEntries(allResources.stream().map(elm -> new EntryInfoBean().setName(elm)).collect(Collectors.toList()));

        when(this.service.load(UID_RECORD)).thenReturn(infoBean);

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final List<String> actual = provider.resources("xxx/xxx/xxx.pdf");

        assertThat(actual, hasSize(0));
    }

    @Test
    public void testResourcesFilePattern() throws Exception {
        final List<String> allResources = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");
        final RecordInfoBean infoBean = new RecordInfoBean() //
                .setCode(UID_RECORD) //
                .setEntries(allResources.stream().map(elm -> new EntryInfoBean().setName(elm)).collect(Collectors.toList()));

        when(this.service.load(UID_RECORD)).thenReturn(infoBean);

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final List<String> actual = provider.resources("step02/*.xml");

        assertThat(actual, containsInAnyOrder(allResources.get(2), allResources.get(5)));
    }

    @Test
    public void testResourcesFolderPattern() throws Exception {
        final List<String> allResources = Arrays.asList("description.xml", "step01/data.xml", "step02/data.xml", "step02/models/model01.pdf", "step02/models/model02.pdf", "step02/pre01.xml",
                "step02/pre01.js");
        final RecordInfoBean infoBean = new RecordInfoBean() //
                .setCode(UID_RECORD) //
                .setEntries(allResources.stream().map(elm -> new EntryInfoBean().setName(elm)).collect(Collectors.toList()));

        when(this.service.load(UID_RECORD)).thenReturn(infoBean);

        final IProvider provider = RecordServiceProvider.create(this.service, UID_RECORD);
        final List<String> actual = provider.resources("**/*.pdf");

        assertThat(actual, containsInAnyOrder(allResources.get(3), allResources.get(4)));
    }

}
