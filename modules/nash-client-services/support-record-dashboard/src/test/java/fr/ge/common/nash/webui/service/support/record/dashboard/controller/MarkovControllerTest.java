/**
 *
 */
package fr.ge.common.nash.webui.service.support.record.dashboard.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;

/**
 * The Class HomeControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/test-engine-context.xml" })
@WebAppConfiguration
public class MarkovControllerTest extends AbstractTest {

    private static final String RECORD_UID_1 = "2017-05-REC-ORD-42";

    private static final String RECORD_UID_2 = "2018-11-REC-ORD-43";

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** Record service. */
    @Autowired
    private IRecordService recordRestService;

    /** Queue service. */
    @Autowired
    private IQueueMessageRestService queueMessageRestService;

    @Autowired
    private Engine engine;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();

        reset(this.engine);
        when(this.engine.loader(any())).thenCallRealMethod();
    }

    /**
     * Tests
     * {@link HomeController#exportRecord(org.springframework.ui.Model, String)} .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testExportRecord() throws Exception {
        // prepare
        final byte[] asBytes = this.resourceAsBytes("spec.zip");
        final IProvider provider = new ZipProvider(asBytes);
        final SpecificationLoader loader = spy(this.engine.loader(provider));
        when(this.engine.loader(any())).thenReturn(loader);

        // call
        this.mvc.perform(get("/record/{code}/export", RECORD_UID_1)) //
                .andExpect(status().isOk()) //
                .andReturn();

        // verify
        verify(this.queueMessageRestService).upload(eq("AWAIT"), eq(true), any());
        verify(this.recordRestService).remove(RECORD_UID_1);
    }

    @Test
    public void testRemoveRecord() throws Exception {
        // prepare
        when(this.recordRestService.remove(eq(RECORD_UID_2))).thenReturn(Response.ok().build());

        // call
        this.mvc.perform(get("/record/{code}/remove", RECORD_UID_2)) //
                .andExpect(status().isOk()) //
                .andReturn();

        // verify
        verify(this.recordRestService).remove(RECORD_UID_2);
    }
}
