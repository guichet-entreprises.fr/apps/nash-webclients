/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated 
 * with loading,  using,  modifying and/or developing or reproducing the 
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore 
 * encouraged to load and test the software's suitability as regards their 
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */ 
'use strict';

var path = require('path');

var project = {
        build: {
            directory: 'target/classes',
            outputDirectory: 'target/classes/public',
            sourceDirectory: 'src/main/resources/public'
        }
};

/**
 * Build CSS file item.
 * 
 * @param basePath source base path
 * @param name resource name
 * @returns file item
 */
function buildStyle(basePath, name) {
    return {
        src: [ basePath + '/' + name ],
        dest: '<%= project.build.outputDirectory %>/css/libs/' + path.basename(basePath) + '/' + path.basename(name).replace(/\.[^.]+$/, '.css')
    }
}


function extend() {
    var sources = Array.from(arguments);
    var dst = {}, src;

    if (1 === sources.length && Array.isArray(sources[0])) {
        sources = sources[0];
    }

    while (src = sources.shift()) {
        Object.keys(src).forEach(function (key) {
            dst[key] = src[key];
        });
    }
    return dst;
}


var overrides = {
        'bootstrap-datepicker': {
            'style': 'dist/css/bootstrap-datepicker3.css'
        },
        'bootstrap-fileinput': {
           'style': 'css/fileinput.min.css'
        },
        'bootstrap-treeview': {
//          'main': 'dist/bootstrap-treeview.min.js',
            'style': 'dist/bootstrap-treeview.min.css',
            'requirejs': {
                'deps': {
                    'jquery': 'jQuery',
                    'bootstrap': null
                }
            }
        },
        'handlebars': {
            'main': 'dist/handlebars.js'
        },
        'intl-tel-input': {
            'main': 'build/js/intlTelInput.js',
            'style': 'build/css/intlTelInput.css'
        },
        'jquery-bar-rating': {
            'style': 'dist/themes/fontawesome-stars.css'
        },
        'inputmask': {
            'main': 'dist/inputmask/inputmask.js'
        },
        'prismjs': {
            'style': 'themes/prism.css',
            'requirejs': {
                'export': 'Prism'
            }
        },
        'prismjs/line-highlight': {
            'style': 'themes/prism.css',
            'requirejs': {
                'deps': {
                    'prismjs': 'Prism',
                    'prismjs/line-numbers': null
                },
                'export': 'Prism'
            }
        },
        'prismjs/line-numbers': {
            'style': 'themes/prism.css',
            'requirejs': {
                'deps': {
                    'prismjs': 'Prism'
                },
                'export': 'Prism'
            }
        },
        'pwstrength-bootstrap': {  
            'main': 'dist/pwstrength-bootstrap.js',
            'requirejs': {
                'deps': {
                    'jquery': 'jQuery',
                    'bootstrap': null
                }
            }
        },
        /*
         * Current version is 1.1.2
         */
        'awesome-phonenumber': {
            'main': 'lib/index.js',
            'requirejs': {
                'deps': {}
            }
        }
    };
 
module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');

    var dev = false;

    var scripts = grunt.file.expandMapping([ '**/*.js' ], '<%= project.build.outputDirectory %>/js', {
        cwd: project.build.sourceDirectory + '/js'
    });

    var vendorsStyles = Object.keys(pkg.dependencies || {}).map(function (name) {
        var basePath = 'node_modules/' + name;
        var nfo = extend(grunt.file.readJSON(basePath + '/package.json'), overrides[name] || {});
        var styles = (nfo.less || nfo.style);

        if (null == styles) {
            return null;
        } else if (Array.isArray(styles)) {
            return styles.map(function (elm) {
                return buildStyle(basePath, elm);
            });
        } else {
            return [ buildStyle(basePath, styles) ];
        }
    }).filter(function (lst) {
        return null != lst && lst.length > 0;
    }).reduce(function (all, lst) {
        return all.concat(lst);
    }, []).concat(grunt.file.expandMapping([ 'css/libs/**/*.less' ], '<%= project.build.outputDirectory %>', {
            cwd: project.build.sourceDirectory
        }).map(function (elm) {
            return {
                src: elm.src,
                dest: elm.dest.replace(/\.[^.]+$/, '.css')
            };
        })
    );

    var applicationStyles = grunt.file.expandMapping([ '**/*.less', '**/*.css', '!**/libs/**', '!mixins/**' ], '<%= project.build.outputDirectory %>/css', {
        cwd: project.build.sourceDirectory + '/css'
    }).map(function (elm) {
        return {
            src: elm.src,
            dest: elm.dest.replace(/\.[^.]+$/, '.css')
        };
    });

    /*
     * PrismJS Plugins
     */
    vendorsStyles = vendorsStyles.concat([ 'line-highlight', 'line-numbers' ].map(function (elm) {
        return {
            src: 'node_modules/prismjs/plugins/' + elm + '/prism-' + elm + '.css',
            dest: '<%= project.build.outputDirectory %>/css/libs/prismjs/prism-' + elm + '.css',
        }
    }));

    var styles = vendorsStyles.concat(applicationStyles);



    /*
     * Build RequireJS dependencies
     */
    var vendorsDependencies = Object.keys(pkg.dependencies || {}) //
        .filter(elm => elm !== 'requirejs') //
        .filter(elm => elm !== 'inputmask') //
        .reduce(function (dst, dep) {
            var path = '';
            var nfo = extend(require('./node_modules/' + dep + '/package.json'), overrides[dep] || {});
            if (nfo.main) {
                if (typeof nfo.main === 'string') {
                    path = nfo.main;
                } else if (nfo.main.find) {
                    path = nfo.main.find(elm => elm.match(/\.js$/));
                } else {
                    return dst;
                }
                dst[dep] = 'node_modules/' + dep + path.replace(/^\.?[\/]*/, '/').replace(/\.js$/, '');
            }
            return dst;
        }, {});

    vendorsDependencies = extend(vendorsDependencies, {
        'prismjs/line-numbers': 'node_modules/prismjs/plugins/line-numbers/prism-line-numbers',
        'prismjs/line-highlight': 'node_modules/prismjs/plugins/line-highlight/prism-line-highlight'
    }, grunt.file.expand({ cwd: 'node_modules/inputmask/dist/inputmask' }, [ '**/*.js', '!**/*.jqlite.js' ]) //
        .reduce(function (dst, dep) {
            dst[dep.replace(/\.js$/, '')] = 'node_modules/inputmask/dist/inputmask/' + dep.replace(/\.js$/, '');
            return dst;
        }, {})
    );

    var requireJsEncapsulation = Object.keys(vendorsDependencies || {}) //
        .reduce(function (dst, dep) {
            var nfo = overrides[dep] || {};
            if (nfo.requirejs) {
                dst[dep] = nfo.requirejs;
            }
            return dst;
        }, {});

    var layoutProvidedDependencies = [ 'bootstrap', 'font-awesome', 'jquery', 'jquery-ui', 'datatables.net', 'datatables.net-bs' ] //
        .reduce(function (dst, dep) {
            dst[dep] = 'empty:';
            return dst;
        }, {});

    layoutProvidedDependencies['version'] = 'node_modules/jquery-ui/ui/version';

    var requireEmptyLibs = extend(Object.keys(vendorsDependencies) //
        .reduce(function (dst, dep) {
            dst[dep] = 'empty:';
            return dst;
        }, {}) //
    , layoutProvidedDependencies);

    var applicationDependencies = grunt.file.expand({ cwd: project.build.sourceDirectory + '/js' }, [ 'lib/**/*.js' ]).map(function (elm) { return elm.replace(/\.js$/, ''); });



    function encapsulateModule(moduleName, contents) {
        var nfo = requireJsEncapsulation[moduleName], deps = [], params = [];

        if (undefined === nfo) {
            return contents;
        }

        Object.keys(nfo.deps || {}).forEach(function (depName) {
            if (nfo.deps[depName]) {
                deps.unshift(depName);
                params.unshift(nfo.deps[depName]);
            } else {
                deps.push(depName);
            }
        });

        if (undefined !== nfo['export']) {
            contents += '\n\treturn ' + nfo['export'];
        }

        return "define('" + moduleName + "', [ " + deps.map(depName => "'" + depName + "'").join(', ') + " ], function (" + params.join(', ') + ") {\n" + contents + "\n});";
    }



    /*
     * GRUNT configuration
     */
    grunt.initConfig({
        project : project,
        requirejs: {
            options: {
                nodeRequire: require,
                baseUrl: '.',
                optimize: 'none',
                onBuildRead: function (moduleName, path, contents) {
                    return encapsulateModule(moduleName, contents);
                }
            },
            vendors: {
                options: {
                    include: Object.keys(vendorsDependencies),
                    paths: extend(layoutProvidedDependencies, vendorsDependencies),
                    out: '<%= project.build.outputDirectory %>/js/vendors.js'
                }
            },
            application: {
                options: {
                    baseUrl: project.build.sourceDirectory + '/js',
                    include: applicationDependencies,
                    paths: extend(requireEmptyLibs, {
                        'app': '../app',
                        'lib': 'lib',
                        'i18n': '../i18n',
                        'rest': '../../rest',
                        '_fm_data': 'empty:',
                        '_fm_description': 'empty:',
                        '_fm_rules': 'empty:',
                        '_fm_adapters': 'empty:',
                        '_fm_adapters_options': 'empty:',
                        '_fm_referentials': 'empty:',
                        'engine/Value': 'empty:',
                    }),
                    out: '<%= project.build.outputDirectory %>/js/libs.js'
                }
            }
        },
        copy: {
            dist: {
                files: {
                    '<%= project.build.outputDirectory %>/js/main.js': '<%= project.build.sourceDirectory %>/js/main.js'
                }
            }
        },
        uglify: {
            options: {
                sourceMap: true
            },
            vendors: {
                files: [
                    {
                        src: [ '<%= project.build.outputDirectory %>/js/require.js', '<%= project.build.outputDirectory %>/js/vendors.js' ],
                        dest: '<%= project.build.outputDirectory %>/js/vendors.min.js'
                    }
                ]
            },
            application: {
                files: [
                    {
                        src: [ '<%= project.build.outputDirectory %>/js/main.js', '<%= project.build.outputDirectory %>/js/libs.js' ],
                        dest: '<%= project.build.outputDirectory %>/js/libs.min.js'
                    }
                ]
            }
        },
        less : {
            dev: {
                options: {
                    modifyVars: {
                        'fa-font-path': 'fonts',
                        'fa-font-path': 'fonts'
                    },
                    paths : [ '<%= project.build.sourceDirectory %>/css/mixins', 'node_modules/bootstrap/less', 'node_modules/font-awesome/less' ]
                },
                files : styles
            }
        },
        extract: {
            dist: {
                files: styles
            }
        },
        cssmin: {
            options: {
                level: {
                    1: {
                        all: true,
                        roundingPrecision: false,
                        selectorsSortingMethod: 'standard',
                        specialComments: 0
                    }
                },
                sourceMap: true
            },
            dev: {
                files: styles.map(function (elm) {
                    return {
                        src: elm.dest,
                        dest: elm.dest.replace(/\.([^.]+)$/, '.min.$1')
                    };
                })
            },
            dist: {
                files: [
                    {
                        src: vendorsStyles.map(elm => elm.dest),
                        dest: '<%= project.build.outputDirectory %>/css/vendors.min.css'
                    },
                    {
                        src: applicationStyles.map(elm => elm.dest),
                        dest: '<%= project.build.outputDirectory %>/css/libs.min.css'
                    }
                ]
            }
        },
        i18n: {
            dist: {
                cwd: 'src/main/resources',
                src: '**/*.po',
                dest: '<%= project.build.outputDirectory %>/js/i18n'
            }
        },
        template: {
            dist: {
                options: {
                    context: {
                        scripts: [ 'vendors.min.js' ].concat(dev ? [ 'main.js', 'libs.js' ] : [ 'libs.min.js' ]),
                        styles: vendorsStyles.map(elm => elm.dest.replace(/^.*\/(css\/)/, '').replace(/\.([^.]+)$/, '.min.$1'))
                    }
                },
                files: grunt.file.expandMapping([ 'views/**/*.html' ], '<%= project.build.directory %>', {
                    cwd: project.build.sourceDirectory + '/..'
                })
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    // grunt.registerTask('default', [ 'changed:requirejs', 'changed:less', 'extract', 'changed:cssmin', 'changed:uglify' ]);
    grunt.registerTask('default', [ 'requirejs', 'less', 'extract', 'cssmin', 'copy', 'uglify', 'template' ]);

}
