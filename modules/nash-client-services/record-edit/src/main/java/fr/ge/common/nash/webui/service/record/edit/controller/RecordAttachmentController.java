/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.webui.core.bean.Message;
import fr.ge.common.nash.webui.core.controller.AbstractRecordController;
import fr.ge.common.nash.webui.core.util.ControllerUtil;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.AttachmentWidgetSecuredController;

/**
 * The Class RecordAttachmentController.
 *
 * @author Christian Cougourdan
 */
@Controller("baseRecordAttachmentController")
public class RecordAttachmentController extends AbstractRecordController {

    @Autowired
    private AttachmentWidgetSecuredController secured;

    /**
     * Default constructor.
     */
    public RecordAttachmentController() {
        // Nothing to do
    }

    /**
     * Upload.
     *
     * @param code
     *            the code
     * @param stepIndex
     *            the step index
     * @param file
     *            the file
     * @param request
     *            the request
     * @return the response entity
     * @throws FunctionalException
     *             the functional exception
     */
    @RequestMapping(value = "/upload/**", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Message<AttachmentWidgetSecuredController.UploadResponse>> upload(@PathVariable("code") final String code, @PathVariable("step") final int stepIndex,
            @RequestParam("attachment") final MultipartFile file, final HttpServletRequest request, final Locale locale) throws FunctionalException {

        final String path = ControllerUtil.translateIdFromHtml(ControllerUtil.extractPathSuffix(request, "/upload/"));

        return this.secured.upload(this.loader(code), stepIndex, path, file, locale);
    }

    /**
     * Download.
     *
     * @param code
     *            the code
     * @param stepIndex
     *            the step index
     * @param id
     *            the id
     * @param idx
     *            the idx
     * @param request
     *            the request
     * @return the response entity
     * @throws FunctionalException
     *             the functional exception
     */
    @RequestMapping(value = "/download/{id}/{idx}/**", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> download(@PathVariable("code") final String code, @PathVariable("step") final int stepIndex, @PathVariable("id") final String id, @PathVariable("idx") final int idx,
            final HttpServletRequest request) throws FunctionalException {

        final String name = ControllerUtil.extractPathSuffix(request, "/download/[^/]+/[^/]+/");

        return this.secured.download(this.loader(code), stepIndex, id, idx, name);
    }

    /**
     * Empty.
     *
     * @param code
     *            the code
     * @param stepIndex
     *            the step index
     * @param request
     *            the request
     * @return the response entity
     * @throws FunctionalException
     *             the functional exception
     */
    @RequestMapping(value = "/empty/**", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> empty(@PathVariable("code") final String code, @PathVariable("step") final int stepIndex, final HttpServletRequest request) throws FunctionalException {
        final String id = ControllerUtil.translateIdFromHtml(ControllerUtil.extractPathSuffix(request, "/empty/"));
        return this.secured.empty(this.loader(code), stepIndex, id);
    }

    /**
     * Reorder.
     *
     * @param code
     *            the code
     * @param stepIndex
     *            the step index
     * @param keys
     *            the keys
     * @param request
     *            the request
     * @return the response entity
     */
    @RequestMapping(value = "/reorder/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Message<AttachmentWidgetSecuredController.UploadResponse>> reorder(@PathVariable("code") final String code, @PathVariable("step") final int stepIndex,
            @RequestParam("values") final List<Integer> keys, final HttpServletRequest request) {

        final String id = ControllerUtil.translateIdFromHtml(ControllerUtil.extractPathSuffix(request, "/reorder/"));
        return this.secured.reorder(this.loader(code), stepIndex, id, keys);
    }

}
