/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.specification.management;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.ws.v1.bean.ReferenceInfoBean;
import fr.ge.common.nash.ws.v1.service.IReferenceService;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class ReferenceSearchControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class ReferenceSearchControllerTest extends AbstractTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    @Autowired
    private IReferenceService referenceService;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.referenceService);
    }

    @Test
    public void testSearchMain() throws Exception {
        this.mvc.perform(get("/reference")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("reference/search/main"));
    }

    @Test
    public void testSearchMainWithParent() throws Exception {
        when(this.referenceService.path(1006L)).thenReturn( //
                Arrays.asList( //
                        new ReferenceInfoBean().setId(1001L).setCategory("SCN"), //
                        new ReferenceInfoBean().setId(1002L).setCategory("GQ"), //
                        new ReferenceInfoBean().setId(1006L).setCategory("PE04") //
                ) //
        );

        this.mvc.perform(get("/reference/{refId}", 1006)) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("reference/search/main"));
    }

}
