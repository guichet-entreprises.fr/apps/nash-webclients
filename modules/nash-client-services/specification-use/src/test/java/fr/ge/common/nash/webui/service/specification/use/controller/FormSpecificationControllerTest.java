/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.use.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class FormSpecificationControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FormSpecificationControllerTest.SpringConfiguration.class)
@WebAppConfiguration
public class FormSpecificationControllerTest extends AbstractTest {

    /** La constante RECORD_UID. */
    private static final String RECORD_UID = "2016-03-GHJ-KLM-42";

    /** La constante SPEC_UID. */
    private static final String SPEC_UID = "2016-03-ABC-DEF-27";

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The form spec service. */
    @Autowired
    private ISpecificationService formSpecService;

    /** The user service. */
    @Autowired
    private HabilitationService habilitationService;

    @Configuration
    @ImportResource({ "classpath:spring/test-context.xml" })
    public static class SpringConfiguration {

        @Bean
        public HabilitationService getHabilitationService() {
            return new HabilitationService();
        }

        @Bean(name = "defaultJsonProvider")
        public JacksonJaxbJsonProvider getJsonProvider() {
            final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });
        }

    }

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.formSpecService, this.recordService, this.habilitationService);
    }

    /**
     * Build specification info bean.
     *
     * @return specification info bean
     */
    private SpecificationInfoBean buildSpecificationInfoBean() {
        final Calendar now = Calendar.getInstance();

        return new SpecificationInfoBean() //
                .setCode("2016-11-AAA-AAA-42") //
                .setTitle("Test Specification") //
                .setTagged(false) //
                .setCreated(now) //
                .setUpdated(now) //
                .setGroupe("all");
    }

    /**
     * Test create instance.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testCreateInstance() throws Exception {
        final byte[] asBytes = "specification zip content".getBytes(StandardCharsets.UTF_8);

        final Map<String, List<String>> defaultRights = new HashMap<String, List<String>>();
        defaultRights.put("all", Arrays.asList("all"));

        when(this.habilitationService.getRoles(Mockito.any())).thenReturn(defaultRights);
        when(this.formSpecService.load(SPEC_UID)).thenReturn(this.buildSpecificationInfoBean());

        when(this.formSpecService.download(SPEC_UID)).thenReturn(Response.ok(asBytes).build());
        when(this.recordService.upload(eq(asBytes), any(), any())).thenReturn(Response.created(URI.create(RECORD_UID)).build());

        this.mvc.perform(get("/form/{code}/use", SPEC_UID)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record/" + RECORD_UID));

        verify(this.recordService).upload(eq(asBytes), any(), any());
    }

    /**
     * Test create instance.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testCreateInstanceByReference() throws Exception {
        final Map<String, List<String>> defaultRights = new HashMap<String, List<String>>();
        defaultRights.put("all", Arrays.asList("all"));

        when(this.habilitationService.getRoles(Mockito.any())).thenReturn(defaultRights);
        when(this.formSpecService.loadByReference("ABS/JPA/CCG/ABB")).thenReturn(this.buildSpecificationInfoBean());

        final byte[] asBytes = "specification zip content".getBytes(StandardCharsets.UTF_8);

        when(this.formSpecService.downloadByReference("ABS/JPA/CCG/ABB")).thenReturn(Response.ok(asBytes).build());
        when(this.recordService.upload(eq(asBytes), any(), any())).thenReturn(Response.created(URI.create(RECORD_UID)).build());

        this.mvc.perform(get("/form/use").param("reference", "ABS/JPA/CCG/ABB")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record/" + RECORD_UID));

        verify(this.recordService).upload(eq(asBytes), any(), any());
    }

    /**
     * Test create unknown instance.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testCreateUnknownInstance() throws Exception {
        final Map<String, List<String>> defaultRights = new HashMap<String, List<String>>();
        defaultRights.put("all", Arrays.asList("all"));

        when(this.formSpecService.load(SPEC_UID)).thenReturn(this.buildSpecificationInfoBean());
        when(this.habilitationService.getRoles(Mockito.any())).thenReturn(defaultRights);

        when(this.formSpecService.download(SPEC_UID)).thenReturn(Response.noContent().build());

        this.mvc.perform(get("/form/{code}/use", SPEC_UID)) //
                .andExpect(status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("form/error/notfound/main"));
    }

    /**
     * Test create unknown instance.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testCreateUnknownInstanceByReference() throws Exception {
        when(this.formSpecService.downloadByReference("ABS/JPA/CCG/ABB")).thenReturn(Response.noContent().build());

        this.mvc.perform(get("/form/use").param("reference", "ABS/JPA/CCG/ABB")) //
                .andExpect(status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("form/error/notfound/main"));
    }

}
