/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.webui.core.controller.AbstractRecordController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordReadOnlySecuredController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordRoutingSecuredController;

/**
 * The Class RecordStepController.
 *
 * @author Christian Cougourdan
 */
@Controller("baseRecordStepController")
public class RecordStepController extends AbstractRecordController {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordStepController.class);

    @Autowired
    private RecordRoutingSecuredController secured;

    @Autowired
    private RecordReadOnlySecuredController securedReadOnly;

    @Value("${nash.readOnly:false}")
    private boolean readOnly;

    /** The engine user type. */
    @Value("${engine.user.type}")
    private String engineUserType;

    /**
     * Inits the.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @param stepIndex
     *            the step
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable(ATTR_STEP) final int stepIndex //
    ) {
        return this.secured.displayStepByIndex(model, this.loader(code), stepIndex);
    }

    /**
     * Page.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @param page
     *            the page
     * @param stepIndex
     *            the step
     * @return the string
     */
    @RequestMapping(path = "/page/{page:[0-9]+}", method = RequestMethod.GET)
    public String page(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable("page") final int page, //
            @PathVariable(ATTR_STEP) final int stepIndex //
    ) {
        final SpecificationLoader loader = this.loader(code);
        return (this.readOnly || loader.hasErrorState()) ? this.securedReadOnly.displayPageFromIndex(model, loader, stepIndex, page)
                : this.secured.displayPageFromIndex(model, loader, stepIndex, page);
    }

    @RequestMapping(path = "/process/{processPhase:pre|post}/{processId}", method = RequestMethod.GET)
    public String page(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable(ATTR_STEP) final String stepId, //
            @PathVariable("processPhase") final String processPhase, //
            @PathVariable("processId") final String processId //
    ) throws TemporaryException {
        return this.secured.displayPageFromProcessId(model, this.loader(code), stepId, processPhase, processId);

    }

    /**
     * Save.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @param page
     *            the page
     * @param success
     *            the success
     * @param stepIndex
     *            the step
     * @param request
     *            the request
     * @return the string
     * @throws TemporaryException
     *             temporary exception
     */
    @RequestMapping(path = "/page/{page:[0-9]+}", method = RequestMethod.POST)
    public String save(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable("page") final Integer page, //
            @RequestParam(value = "success", required = false) final String success, //
            @PathVariable(ATTR_STEP) final int stepIndex, //
            final HttpServletRequest request //
    ) throws TemporaryException {

        final SpecificationLoader loader = this.loader(code);
        if (this.readOnly || loader.hasErrorState()) {
            return this.securedReadOnly.next(model, loader, stepIndex, page, success, request);
        } else {
            return this.secured.save(model, loader, stepIndex, page, success, request);
        }
    }

    /**
     * Validate a step.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @param page
     *            the page
     * @param stepIndex
     *            the step
     * @return the string
     */
    @RequestMapping(path = "/validate", method = RequestMethod.GET)
    public String validateStep(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable(ATTR_STEP) final int stepIndex, //
            final HttpServletRequest request //
    ) throws TemporaryException {
        final SpecificationLoader loader = this.loader(code);
        return this.secured.validate(model, loader, stepIndex);
    }
}