/**
 *
 */
package fr.ge.common.nash.webui.service.record.edit.rest;

import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.v1_2.StringValueAdapter;
import fr.ge.common.nash.engine.support.i18n.MessageExtractor;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class TypesRestControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class TypesRestControllerTest extends AbstractTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    /**
     * Test script.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScript() throws Exception {
        final String type = "String";
        final String version = "1.2";
        final byte[] actual = this.mvc.perform(get("/rest/type/{version}/{type}/script.js", version, type).locale(Locale.getDefault())) //
                .andExpect(status().is(HttpStatus.OK.value())) //
                .andExpect(header().string("Content-Type", "text/javascript")) //
                .andReturn().getResponse().getContentAsByteArray();

        assertEquals( //
                new String(IOUtils.toByteArray(MessageExtractor.extractAsScript(NashMessageReader.getReader(ValueAdapterFactory.type(version, type)))), StandardCharsets.UTF_8) + //
                        this.resourceAsString("/" + StringValueAdapter.class.getName().replace('.', '/') + "Resources/script.js"), //
                new String(actual, StandardCharsets.UTF_8) //
        );
    }

    /**
     * Test script unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScriptUnknown() throws Exception {
        final String type = "Unknown";
        final String version = "1.2";
        this.mvc.perform(get("/rest/type/{version}/{type}/script.js", version, type)) //
                .andExpect(status().is(HttpStatus.NOT_FOUND.value())) //
                .andExpect(header().string("Content-Type", not("text/javascript"))) //
                .andExpect(content().string(String.format("Type '%s' not found in version '%s'", type, version)));
    }

    /**
     * Test stylesheet.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testStylesheet() throws Exception {
        final String type = "String";
        final String version = "1.2";
        this.mvc.perform(get("/rest/type/{version}/{type}/style.css", version, type)) //
                .andExpect(status().is(HttpStatus.OK.value())) //
                .andExpect(header().string("Content-Type", "text/css")) //
                .andExpect(content().bytes(this.resourceAsBytes("/" + StringValueAdapter.class.getName().replace('.', '/') + "Resources/style.css")));
    }

    /**
     * Test stylesheet unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testStylesheetUnknown() throws Exception {
        final String type = "Unknown";
        final String version = "1.2";
        this.mvc.perform(get("/rest/type/{version}/{type}/style.css", version, type)) //
                .andExpect(status().is(HttpStatus.NOT_FOUND.value())) //
                .andExpect(header().string("Content-Type", not("text/css"))) //
                .andExpect(content().string(String.format("Type '%s' not found in version '%s'", type, version)));
    }

}
