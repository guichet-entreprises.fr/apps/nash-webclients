/**
 *
 */
package fr.ge.common.nash.webui.core.loader.event.listener;

import java.net.SocketTimeoutException;
import java.text.MessageFormat;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.ServiceUnavailableException;

import org.apache.cxf.interceptor.Fault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TrackerListener {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TrackerListener.class);

    /** Tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /**
     * Mutateur sur l'attribut {@link #trackerFacade}.
     *
     * @param trackerFacade
     *            la nouvelle valeur de l'attribut trackerFacade
     */
    public void setTrackerFacade(final ITrackerFacade trackerFacade) {
        this.trackerFacade = trackerFacade;
    }

    @Subscribe
    public void onMessageEvent(final MessageEvent event) {
        final String code = event.getLoader().description().getRecordUid();
        final String enhancedMessage = MessageFormat.format("Le dossier a été modifié par \"{0}\". {1}", event.getAuthor(), event.getMessage());

        try {
            this.trackerFacade.post(code, enhancedMessage);
        } catch (ServiceUnavailableException | NotFoundException | ProcessingException | Fault e) {
            if (e instanceof ProcessingException && e.getCause() instanceof SocketTimeoutException) {
                LOGGER.warn("Timeout error while contacting TRACKER");
            } else {
                LOGGER.warn("Error calling TRACKER with code \"{}\" and message \"{}\" : {}", code, enhancedMessage, e.getMessage(), e);
            }
        }
    }

}
