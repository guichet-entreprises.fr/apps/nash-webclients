package fr.ge.common.nash.webui.service.devtools.engine;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalUrlElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;

/**
 * @author Christian Cougourdan
 */
@Component("specificationLoaderMock")
public class SpecificationLoaderMock extends SpecificationLoader {

    /** Step position. */
    private static final int STEP_POSITION = 42;

    /** The version. */
    private final String version;

    private FormSpecificationDescription rootElement;

    private FormSpecificationReferential referential;

    /**
     * Constructor.
     */
    private SpecificationLoaderMock() {
        this(null);
    }

    /**
     * Constructor.
     *
     * @param version
     *            the version
     */
    private SpecificationLoaderMock(final String version) {
        super(new Configuration(new Properties()), null);
        this.version = null == version ? "latest" : version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormSpecificationDescription description() {
        if (null == this.rootElement) {
            final StepElement step = new StepElement();
            step.setStatus(StepStatusEnum.TO_DO.getStatus());
            step.setUser("ge");

            final FormSpecificationDescription description = new FormSpecificationDescription();
            description.setVersion(this.version);
            description.setTitle("Test");
            description.setDescription("Test");
            description.setLang("en");
            description.setTranslations(new TranslationsElement());
            description.setSteps(new StepsElement(Arrays.asList(step)));

            this.rootElement = description;
        }

        return this.rootElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormSpecificationReferential referential(final String id) {
        if (null == this.referential) {
            final ReferentialExternalUrlElement externalUrl = new ReferentialExternalUrlElement();
            externalUrl.setInputVarName("");
            externalUrl.setValue("types/byversion/datatable?order[0].column=0&order[0].dir=asc&start=0&length=5&version=latest&name={{searchTerms}}");

            final ReferentialExternalElement external = new ReferentialExternalElement();
            external.setUrl(externalUrl);
            external.setBase("data");
            external.setId("");
            external.setLabel("");

            final FormSpecificationReferential ref = new FormSpecificationReferential();
            ref.setReferentialExternalElement(external);
            ref.setReferentialTextElements( //
                    Arrays.asList( //
                            new ReferentialTextElement("ba", "Banana"), //
                            new ReferentialTextElement("or", "Orange"), //
                            new ReferentialTextElement("peac", "Peach"), //
                            new ReferentialTextElement("ma", "Mango"), //
                            new ReferentialTextElement("pear", "Pear"), //
                            new ReferentialTextElement("pu", "Pumpkin") //
                    ) //
            );

            this.referential = ref;
        }

        return this.referential;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream translationFile(final String lang) {
        if ("fr".equals(lang)) {
            return this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".po");
        } else {
            return null;
        }
    }

    @Override
    public <T> void save(final String resourcePath, final T bean) {
    }

    /**
     * Create a new specification loader mock.
     *
     * @param applicationContext
     *            Spring application context
     * @return loader instance
     */
    public static SpecificationLoader create(final ApplicationContext applicationContext) {
        return applicationContext.getBean(SpecificationLoaderMock.class);
    }

    /**
     * Create a new specification loader mock.
     *
     * @param applicationContext
     *            Spring application context
     * @param version
     *            version
     * @return loader instance
     */
    public static SpecificationLoader create(final ApplicationContext applicationContext, final String version) {
        return applicationContext.getBean(SpecificationLoaderMock.class, version);
    }

}
