/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.rest;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.engine.loader.ResourceLoader;
import fr.ge.common.utils.CoreUtil;

/**
 * Resources rest controller.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Controller("recordEditResourcesRestController")
public class ResourcesRestController {

    /**
     * Gets a public resource.
     *
     * @param request
     *            request
     * @param response
     *            the response
     * @throws IOException
     *             an IO exception
     */
    @RequestMapping(value = "/rest/engine/**", method = RequestMethod.GET)
    public void resource(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final String requestPath = CoreUtil.coalesce(request.getPathInfo(), request.getServletPath());
        final String resourcePath = requestPath.replaceFirst("/rest/engine/([^/]+)/", "$1/engine/");

        final InputStream resource = ResourceLoader.getResourceStream(resourcePath);
        response.setContentType("text/javascript");
        IOUtils.copy(resource, response.getOutputStream());
    }

}
