/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/template','lib/pagination', 'lib/i18n', 'lib/dragAndDropUpload', 'jquery-ui/widgets/draggable', 'jquery-ui/widgets/droppable'], function($, Template, pagination, i18n) {

    console.debug = function () {};

    var PAGE_SIZE = 10;

    var queueRefreshTimeoutId;
    var queues = $("#queue-success, #queue-error, #queue-await, #queue-inferno");

    var droppableIds = [];
    droppableIds["queue-success"] = "#queue-await div.queue-card";
    droppableIds["queue-error"] = "#queue-await div.queue-card";
    droppableIds["queue-inferno"] = "#queue-await div.queue-card";
    //droppableIds["queue-await"] = "#queue-success div.queue-card, #queue-error div.queue-card";

    var draggableAppendTo = $('#queue-success').parent();
    queues.on("mouseenter", "div.queue-card:not(.initiated)", function() {
        var queueCard = $(this);
        queueCard.draggable({
            cancel: "a.ui-icon", 
            revert: "invalid",
            helper: "clone",
            cursor: "move",
            appendTo: draggableAppendTo,
            start: function(event, ui) {
                $(ui.helper)
                    .css('width', `${ $(event.target).width() }px`);
                },
            drag: function(event, ui) {
                if (queueRefreshTimeoutId) {
                    queueRefreshTimeoutId = clearTimeout(queueRefreshTimeoutId);
                }
            }
        }).addClass("initiated");
    }).each(function() {
       var queue = $(this);
       var queueId = queue.attr("id");
       queue.droppable({
           accept: droppableIds[queueId],
           drop: function( event, ui ) {
               var destinationQueueCode = queueId.split("-")[1].toUpperCase();
               moveQueueCards(destinationQueueCode, $(this), ui);
           }
       });
    }).on('click', 'a[role="remove"]', function(evt){
        evt.preventDefault();
        deleteQueueCard($(this).closest(".queue-card"));
    });
    
    $("#queue-success, #queue-error, #queue-await, #queue-inferno, #queue-inprogress").on('click', 'a[role="pause"]', function(evt) {
        evt.preventDefault();
        var queue = $(this).closest(".queue");
        pausePlayQueueCard(queue, false);
    }).on('click', 'a[role="play"]', function(evt) {
        evt.preventDefault();
        var queue = $(this).closest(".queue");
        pausePlayQueueCard(queue, true);
    });

    $("#queue-pending-files").on('click', 'a[role="count"]', function(evt) {
        evt.preventDefault();
        displayPendingFiles();
    });

    function displayPendingFiles() {
        var root = $("#queue-pending-files");
        $.ajax(markovBaseUrl + '/v1/monitoring/pending/files', {
            method: 'GET'
        }).done(function (count) {
            $('.total-entries', root).text(count || 0);
        });
    }
    
    $(function () {
        $('#queue-await .queue-cards').dragAndDropUpload({
            url: markovBaseUrl + '/v1/queue/AWAIT'
        });
        updateQueues(initQueueCards);
        displayPendingFiles();
    });

    function updateQueues(cmd) {
        if (!cmd) {
            cmd = updateQueueCards;
        }

        if (queueRefreshTimeoutId) {
            queueRefreshTimeoutId = clearTimeout(queueRefreshTimeoutId);
        }

        cmd('inferno');
        cmd('error');
        cmd('success');
        cmd('await', [ 'queueCode:AWAIT', 'worker:' ]);
        cmd('inprogress', [ 'queueCode:AWAIT', 'worker!' ], false);

        queueRefreshTimeoutId = setTimeout(updateQueues, 1000);
    }

    function updateQueueCards(queueCode) {
        $('#queue-' + queueCode.toLowerCase() + ' .queue-cards').pagination('refresh');
    }

    function initQueueCards(queueCode, filters, showPagination) {
        var root = $('#queue-' + queueCode.toLowerCase());
        var pageOffset = root.data('page-offset') || 0;

        $('.queue-cards', root).pagination({
            source : function (pageOffset, cb) {
                $.ajax(markovBaseUrl + '/v1/queue', {
                    data: {
                        startIndex: PAGE_SIZE * pageOffset,
                        maxResults: PAGE_SIZE,
                        filters: filters || [ 'queueCode:' + queueCode.toUpperCase() ]
                    },
                    dataType: 'json'
                }).done(function (searchResult) {
                    cb(searchResult);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    console.error(arguments);
                });
            },
            renderItems : function (container, searchResult) {
                var htmlQueueContent = new Template('queue-content').render(searchResult);
                container.html(htmlQueueContent);
                $('.total-entries', root).text(searchResult.totalResults || 0);
                //-->Update queue status (play or pause)
                $.ajax(markovBaseUrl + '/v1/monitoring/feature/' + queueCode.toUpperCase(), {
                    method: 'GET'
                }).done(function (result) {
                    if (undefined != result) {
                        result['feature'] = queueCode.toUpperCase();
                        $('.queue-status', root).html(new Template('queue-card').render(result));
                    }
                });
                //<--
            },
            showPagination: false !== showPagination
        });
    }

    function moveQueueCards(destinationQueueCode, queueCard, ui) {
        var draggable = ui.draggable.detach();
        var idMessage = draggable.data("queue-message-id");
        $.ajax(markovBaseUrl + '/v1/queue/message/' + idMessage + '?dst=' + destinationQueueCode, {
            method: 'PUT',
            dataType: 'json'
        }).always(function (data) {
            $(queueCard)
                .find( "div.droppable.queue-cards")
                .append(draggable);
            updateQueues();
        });
    }

    function deleteQueueCard(queueCard) {
        var idMessage = queueCard.data("queue-message-id");
        var dlg = $('#confirmRemoveModal');
        $('.modal-header .modal-title', dlg).text(i18n('Record {0}', idMessage));

        dlg.off('click').on('click', 'button[role="validate"]', function(evt) {
            $.ajax(markovBaseUrl + '/v1/queue/message/' + idMessage, {
                method: 'DELETE',
                dataType: 'json'
            }).always(function() {
                dlg.modal('hide');
            }).always(function (data) {
                $(queueCard)
                    .find( "div.droppable.queue-cards")
                    .remove();
                updateQueues();
            });
        }).modal('show');
    }

    function pausePlayQueueCard(queue, nextState) {
        var dlg = $('#confirmPlayPauseQueue');
        var queueId = queue.attr("id");
        var queueCode = queueId.split("-")[1].toUpperCase();
        $('.modal-header .modal-title', dlg).text(i18n('Queue {0}', queueCode));
        if (nextState) {
        	$('.modal-body', dlg).text(i18n('Do you really want to play this queue ?'));
        } else {
        	$('.modal-body', dlg).text(i18n('Do you really want to pause this queue ?'));
        }

        dlg.off('click').on('click', 'button[role="validate"]', function(evt) {
            $.ajax(markovBaseUrl + '/v1/monitoring/feature/' + queueCode + '/state/' + nextState, {
                method: 'PUT'
            }).always(function() {
                dlg.modal('hide');
                location.reload();
            });
        }).modal('show');
    }
});
