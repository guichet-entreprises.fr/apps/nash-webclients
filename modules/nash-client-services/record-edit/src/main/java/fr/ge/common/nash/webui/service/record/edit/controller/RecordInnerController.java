/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import fr.ge.common.nash.webui.core.controller.AbstractRecordController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordEditingSecuredController;

/**
 * The Class RecordStepController.
 *
 * @author Christian Cougourdan
 */
@Controller("baseRecordInnerController")
public class RecordInnerController extends AbstractRecordController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordInnerController.class);

    @Autowired
    private RecordEditingSecuredController secured;

    @Autowired
    private ServletContext servletContext;

    /**
     * Default constructor.
     */
    public RecordInnerController() {
        // Nothing to do
    }

    /**
     * Page.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @param stepIndex
     *            the step
     * @param page
     *            the page
     * @param response
     *            the response
     * @return the string
     */
    @RequestMapping(value = "/page/{page:[0-9]+}/inner", method = RequestMethod.GET, headers = { "X-Requested-With=XMLHttpRequest" })
    public String page(final Model model, @PathVariable(ATTR_CODE) final String code, @PathVariable("step") final int stepIndex, @PathVariable(ATTR_PAGE) final int page,
            final HttpServletRequest request, final HttpServletResponse response) {

        return this.secured.displayInnerPageFromIndex(request, response, this.servletContext, model, this.loader(code), stepIndex, page);
    }

}
