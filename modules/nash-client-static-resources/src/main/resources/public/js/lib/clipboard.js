define([], function() {

    function addSelection(element) {
        var doc = document, text = doc.getElementById(element), range, selection;
        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    function clearSelection() {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }
    }

    function copyClipBoard(element) {
        addSelection(element);
        try {
            var successful = document.execCommand('copy');
        } catch (err) {
            if (console) {
                console.error("Unsupported Browser!");
            }
        }
        clearSelection();
    }

    return {
        copy : copyClipBoard
    };

});
