/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.provider;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.ws.v1.bean.EntryInfoBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class RecordServiceProvider.
 *
 * @author Christian Cougourdanp
 * @deprecated use {@link fr.ge.common.nash.ws.provider.RecordServiceProvider}
 *             instead
 */
@Deprecated
public class RecordServiceProvider implements IProvider {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordServiceProvider.class);

    /** The files cache. */
    private final Map<String, FileEntry> filesCache = new HashMap<>();

    /** The mapper files cache. */
    private final Map<String, Object> mappedFilescache = new HashMap<>();

    /** The service. */
    private final IRecordService service;

    /** The code. */
    private final String code;

    /**
     * Instantiates a new record service provider.
     *
     * @param service
     *            the service
     * @param code
     *            the code
     */
    public RecordServiceProvider(final IRecordService service, final String code) {
        this.service = service;
        this.code = code;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T asBean(final String name, final Class<T> clazz) {
        final String absolutePath = this.getAbsolutePath(name);
        T asBean = null;
        if (this.mappedFilescache.containsKey(absolutePath)) {
            asBean = CoreUtil.cast(this.mappedFilescache.get(absolutePath));
        } else {
            final byte[] asBytes = this.asBytes(absolutePath);
            if (ArrayUtils.isNotEmpty(asBytes)) {
                asBean = JaxbFactoryImpl.instance().unmarshal(asBytes, clazz);
            }
            this.mappedFilescache.put(absolutePath, asBean);
        }
        return asBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FileEntry load(final String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }

        final String absolutePath = this.getAbsolutePath(name);
        FileEntry entry = this.filesCache.get(absolutePath);

        if (null == entry) {
            final Response response = this.service.download(this.code, absolutePath);

            if (null == response || Status.OK.getStatusCode() != response.getStatus()) {
                return null;
            }

            entry = new FileEntry(absolutePath, response.getHeaderString(HttpHeaders.CONTENT_TYPE));
            entry.asBytes(response.readEntity(byte[].class));
            entry.lastModified(response.getLastModified());

            this.filesCache.put(absolutePath, entry);
        }

        return entry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save(final String name, final String mimetype, final byte[] content) {
        if (StringUtils.isEmpty(name)) {
            return;
        }

        final String absolutePath = this.getAbsolutePath(name);

        final Response response = this.service.upload(this.code, absolutePath, content);

        final FileEntry entry = this.filesCache.get(absolutePath);
        if (null != entry && null != response) {
            entry.asBytes(content);
            entry.lastModified(response.getLastModified());
        }
        this.mappedFilescache.remove(absolutePath);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(final String name) {
        if (StringUtils.isEmpty(name)) {
            return false;
        }

        final String absolutePath = this.getAbsolutePath(name);

        this.filesCache.remove(absolutePath);
        this.mappedFilescache.remove(absolutePath);

        final Response response = this.service.remove(this.code, absolutePath);

        return null != response && Status.OK.getStatusCode() == response.getStatus();
    }

    /**
     * Creates the.
     *
     * @param service
     *            the service
     * @param code
     *            the code
     * @return the i specification provider
     */
    public static IProvider create(final IRecordService service, final String code) {
        return new RecordServiceProvider(service, code);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] asBytes() {
        final Response response = this.service.download(this.code);
        byte[] resourceAsBytes = null;
        if (response != null) {
            if (Status.INTERNAL_SERVER_ERROR.getStatusCode() == response.getStatus()) {
                LOGGER.warn("Unable to load record '{}' content : {}", this.code, response.readEntity(String.class));
            } else {
                resourceAsBytes = response.readEntity(byte[].class);
            }
        }
        return resourceAsBytes;
    }

    @Override
    public String getAbsolutePath(final String name) {
        if (null != name && '/' == name.charAt(0)) {
            return name.substring(1);
        } else {
            return name;
        }
    }

    @Override
    public List<String> resources(final String stepFolder) {
        final RecordInfoBean info = this.service.load(this.code);

        return Optional.of(info) //
                .map(RecordInfoBean::getEntries) //
                .map(lst -> lst.stream().map(EntryInfoBean::getName).filter(ResourceUtil.filterAsPredicate(stepFolder)).collect(Collectors.toList())) //
                .orElse(Collections.emptyList());
    }

}
