package fr.ge.common.nash.webui.core.loader.event.listener;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.StorageEvent;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferenceElement;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.storage.ws.rest.IStorageRestService;

/**
 * Storage listener tests.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StorageListenerTest {

    /** The event. */
    @Mock
    private StorageEvent event;

    /** The listener. */
    @InjectMocks
    private StorageListener listener;

    /** Loader. */
    @Mock
    private SpecificationLoader loader;

    /** The record service. */
    @Mock
    private IRecordService recordService;

    /** The record service. */
    @Mock
    private IRecordStorage recordStorage;

    /** Storage REST Service. **/
    @Mock
    private IStorageRestService storageRestService;

    /** Specification description. **/
    private final FormSpecificationDescription description = new FormSpecificationDescription();

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.description.setRecordUid("ABC");
        final ReferenceElement reference = new ReferenceElement();
        reference.setCode("test");
        this.description.setReference(reference);
    }

    /**
     * Prepare nash store record.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void prepareStoreIntoFileSystem(final boolean enabled) throws TemporaryException {
        this.listener.setStorageRecordRoot(enabled ? "test" : null);
    }

    /**
     * Verify nash store record.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void assertStoreIntoFileSystem(final boolean enabled) throws TemporaryException {
        if (enabled) {
            verify(this.recordStorage).storeRecord(any(String.class), any(InputStream.class), any(String.class));
        } else {
            verify(this.recordStorage, never()).storeRecord(any(String.class), any(InputStream.class), any(String.class));
        }
    }

    /**
     * Prepare STORAGE service.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void prepareStorageArchivePartners(final boolean enabled) throws TemporaryException {
        this.listener.setRecordStorageArchivePartners(enabled);
    }

    /**
     * Verify STORAGE service.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void assertStorageArchivePartners(final boolean enabled) throws TemporaryException {
        if (enabled) {
            verify(this.storageRestService).store(any(String.class), any(Attachment.class), any(String.class), any(String.class), anyList());
        } else {
            verify(this.storageRestService, never()).store(any(String.class), any(Attachment.class), any(String.class), any(String.class), anyList());
        }
    }

    /**
     * Prepare service keep record.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void prepareKeepRecord(final boolean enabled) throws TemporaryException {
        this.listener.setRecordMemoryKeep(enabled);
    }

    /**
     * Verify service keep record.
     *
     * @param enabled
     * @throws TemporaryException
     */
    private void assertKeepRecord(final boolean enabled) throws TemporaryException {
        if (enabled) {
            verify(this.recordService, never()).remove(any(String.class));
        } else {
            verify(this.recordService).remove(any(String.class));
        }
    }

    /**
     * Mocking service.
     *
     * @throws TemporaryException
     */
    private void mockService() throws TemporaryException {
        doNothing().when(this.recordStorage).storeRecord(any(String.class), any(InputStream.class), any(String.class));
        when(this.recordService.remove(any(String.class))).thenReturn(Response.ok().build());
        when(this.recordService.download(any(String.class))).thenReturn(Response.ok(new ByteArrayInputStream("test".getBytes())).build());
        when(this.loader.description()).thenReturn(this.description);
        when(this.event.getLoader()).thenReturn(this.loader);
        when(this.storageRestService.store(any(String.class), any(Attachment.class), any(String.class), any(String.class), anyList()))
                .thenReturn(Response.ok(new ByteArrayInputStream("Test".getBytes())).build());
    }

    /**
     * Store record for Nash Web.
     *
     * @throws TemporaryException
     */
    public void testEndOfRecord(final boolean keepRecord, final boolean storeIntoFileSystem, final boolean storeArchivePartners) throws TemporaryException {
        this.prepareStoreIntoFileSystem(storeIntoFileSystem);
        this.prepareStorageArchivePartners(storeArchivePartners);
        this.prepareKeepRecord(keepRecord);

        // mock
        this.mockService();

        // call
        this.listener.onStorageEvent(this.event);

        // check
        this.assertKeepRecord(keepRecord);
        this.assertStoreIntoFileSystem(storeIntoFileSystem);
        this.assertStorageArchivePartners(storeArchivePartners);
    }

    /**
     * Store record using web configuration.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashWeb() throws TemporaryException {
        // Nash web
        this.testEndOfRecord(true, true, false);
    }

    /**
     * Store record.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashBo() throws TemporaryException {
        // Nash bo
        this.testEndOfRecord(false, false, true);
    }

    /**
     * Store record using support configuration.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashSupport() throws TemporaryException {
        // Nash support
        this.testEndOfRecord(false, true, false);
    }

    /**
     * Store record using directory configuration.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashDirectory() throws TemporaryException {
        // Nash directory
        this.testEndOfRecord(false, true, false);
    }

    /**
     * Store record using profiler configuration.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashProfiler() throws TemporaryException {
        // Nash profiler
        this.testEndOfRecord(false, false, false);
    }

    /**
     * Store record using forge configuration.
     *
     * @throws TemporaryException
     */
    @Test
    public void testOnEventNashForge() throws TemporaryException {
        // Nash forge
        this.testEndOfRecord(true, false, false);
    }
}
