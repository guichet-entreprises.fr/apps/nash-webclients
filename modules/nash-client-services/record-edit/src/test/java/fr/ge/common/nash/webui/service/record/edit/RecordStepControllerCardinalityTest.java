/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Class RecordStepControllerCardinalityTest.
 *
 * @author aolubi
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
@Ignore("TODO Must be fixed")
public class RecordStepControllerCardinalityTest extends AbstractRecordControllerTest {

    /**
     * Test save cardinality one to one.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveCardinalityOneToOne() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02[0]", "Luke") //
                .param("group01data02[1]", "David");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityFromEmpty() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-empty.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02[0]", "Luke") //
                .param("group01data02[1]", "David");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityAddOne() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-three.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02[0]", "Luke") //
                .param("group01data02[1]", "Anakin") //
                .param("group01data02[2]", "Obiwan");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityRemoveDataElement() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-multiple-dataelement.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-one.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02[0]", "Luke") //
                .param("subGroup01[0].group01data01", "John") //
        ;

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityToDataEmpty() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-empty.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityUpdateSubGroup() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-subgroup.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-subgroup.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("subGroup01[0].subGroup01Item01", "James") //
        ;

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityAddSubGroup() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-subgroup.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-new-subgroup.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("subGroup01[0].subGroup01Item01", "David") //
                .param("subGroup01[1].subGroup01Item01", "Kevin") //
        ;

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityRemoveGroupElement() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-multiple-groupelement.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-updated-multiple-groupelement.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("enfant[0].nom", "Smith") //
                .param("enfant[0].prenom[0]", "Louis") //
                .param("enfant[0].prenom[1]", "Olivier") //
                .param("enfant[0].adresse[0].numeroVoie", "1") //
                .param("enfant[0].adresse[0].typeVoie", "rue") //
                .param("enfant[0].adresse[0].nomVoie", "Diderot") //
                .param("enfant[0].adresse[1].numeroVoie", "1") //
                .param("enfant[0].adresse[1].typeVoie", "boulevard") //
                .param("enfant[0].adresse[1].nomVoie", "de la République") //
                .param("enfant[0].telephone[0]", "0123456789") //
                .param("enfant[0].telephone[1]", "0623456789") //
                .param("enfant[0].profession[0].nomProfession", "Agriculteur") //
                .param("enfant[0].profession[0].adresseProfession[0].numeroVoie", "2") //
                .param("enfant[0].profession[0].adresseProfession[0].typeVoie", "rue") //
                .param("enfant[0].profession[0].adresseProfession[0].nomVoie", "Diderot") //
        ;

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }

    @Test
    public void testSaveCardinalityToGroupElementEmpty() throws Exception {
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-multiple-groupelement.xml")).build());

        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/1-data/data-empty-multiple-groupelement.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01"); //

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray), new String(captor.getValue()));
    }
}
