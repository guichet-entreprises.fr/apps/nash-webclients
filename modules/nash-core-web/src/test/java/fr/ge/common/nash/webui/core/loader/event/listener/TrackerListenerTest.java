package fr.ge.common.nash.webui.core.loader.event.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Tracker listener tests.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TrackerListenerTest {

    /** The event. */
    @Mock
    private MessageEvent event;

    /** The listener. */
    @InjectMocks
    private TrackerListener listener;

    /** Loader. */
    @Mock
    private SpecificationLoader loader;

    /** Tracker facade. */
    @Mock
    private ITrackerFacade trackerFacade;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnEvent() {
        // mock
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setRecordUid("My record id");
        when(this.loader.description()).thenReturn(description);
        when(this.event.getLoader()).thenReturn(this.loader);
        when(this.event.getAuthor()).thenReturn("My user id");
        when(this.event.getMessage()).thenReturn("My message");
        when(this.loader.historyPath()).thenReturn("history.xml");
        when(this.loader.history()).thenReturn(new FormSpecificationHistory());

        // call
        this.listener.onMessageEvent(this.event);

        // check
        final String messagePattern = "Le dossier a été modifié par \"My user id\". My message";
        final ArgumentCaptor<String> message = ArgumentCaptor.forClass(String.class);
        verify(this.trackerFacade).post(eq("My record id"), message.capture());
        final String actual = message.getValue();

        assertNotNull(actual);
        assertEquals(actual, messagePattern);
    }
}
