/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.core.advice;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.RecordStepDataNotFoundException;
import fr.ge.common.nash.core.exception.RecordStepNotFoundException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;
import fr.ge.common.nash.webui.core.advice.GenericErrorHandler;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;

/**
 * Class GenericErrorHandlerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { GenericErrorHandlerTest.TestContextConfiguration.class })
@WebAppConfiguration
public class GenericErrorHandlerTest {

    @Configuration
    static class TestContextConfiguration {

        @Bean
        public TestController testController() {
            return new TestController();
        }

        @Bean
        public Object genericErrorHandler() {
            return new GenericErrorHandler();
        }

    }

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Before
    public void setUp() throws Exception {
        AccountContext.currentUser(null);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    @Test
    public void testUnauthorizedException() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/unauthorized")) //
                .andExpect(MockMvcResultMatchers.status().isForbidden()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/access/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(UnauthorizedAccessException.class), //
                        hasProperty("message", equalTo("Unauthorized access exception for test user")) //
                ) //
        );
    }

    @Test
    public void testUnauthorizedExceptionInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/unauthorized/inner")) //
                .andExpect(MockMvcResultMatchers.status().isForbidden()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/access/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(UnauthorizedAccessException.class), //
                        hasProperty("message", equalTo("Unauthorized access exception for test user")) //
                ) //
        );
    }

    @Test
    public void testRecordNotFound() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/notfound")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordNotFoundException.class), //
                        hasProperty("message", equalTo("Record not found exception for test user")) //
                ) //
        );
    }

    @Test
    public void testRecordNotFoundInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/notfound/inner")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordNotFoundException.class), //
                        hasProperty("message", equalTo("Record not found exception for test user")) //
                ) //
        );
    }

    @Test
    public void testRecordStepNotFound() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/step/notfound")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/step-notfound/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordStepNotFoundException.class), //
                        hasProperty("message", equalTo("Record step not found exception for test user")), //
                        hasProperty("stepIndex", equalTo(3)) //
                ) //
        );
    }

    @Test
    public void testRecordStepNotFoundInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/step/notfound/inner")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/step-notfound/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordStepNotFoundException.class), //
                        hasProperty("message", equalTo("Record step not found exception for test user")), //
                        hasProperty("stepIndex", equalTo(3)) //
                ) //
        );
    }

    @Test
    public void testRecordStepDataNotFound() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/step/data/notfound")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/step-data-notfound/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordStepDataNotFoundException.class), //
                        hasProperty("message", equalTo("Record step data not found exception for test user")), //
                        hasProperty("stepIndex", equalTo(3)) //
                ) //
        );
    }

    @Test
    public void testRecordStepDataNotFoundInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/step/data/notfound/inner")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/step-data-notfound/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(RecordStepDataNotFoundException.class), //
                        hasProperty("message", equalTo("Record step data not found exception for test user")), //
                        hasProperty("stepIndex", equalTo(3)) //
                ) //
        );
    }

    @Test
    public void testTemporary() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/temporary")) //
                .andExpect(MockMvcResultMatchers.status().isInternalServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/retry/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(TemporaryException.class), //
                        hasProperty("message", equalTo("Temporary exception for test user")) //
                ) //
        );
    }

    @Test
    public void testTemporaryInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/temporary/inner")) //
                .andExpect(MockMvcResultMatchers.status().isInternalServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/retry/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(TemporaryException.class), //
                        hasProperty("message", equalTo("Temporary exception for test user")) //
                ) //
        );
    }

    @Test
    public void testUnknown() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/unknown")) //
                .andExpect(MockMvcResultMatchers.status().isInternalServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/default/main")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(Exception.class), //
                        hasProperty("message", equalTo("Unknown exception for test user")) //
                ) //
        );
    }

    @Test
    public void testUnknownInner() throws Exception {
        final String uuid = UUID.randomUUID().toString();
        MDC.put("correlationId", uuid);

        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/unknown/inner")) //
                .andExpect(MockMvcResultMatchers.status().isInternalServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/default/inner")) //
                .andReturn();

        assertThat(actual.getModelAndView().getModel().get("uuid"), equalTo(uuid));
        assertThat(actual.getModelAndView().getModel().get("error"), //
                allOf( //
                        instanceOf(Exception.class), //
                        hasProperty("message", equalTo("Unknown exception for test user")) //
                ) //
        );
    }

    @Controller
    private static class TestController {

        @RequestMapping(path = { "/unauthorized", "/unauthorized/inner" }, method = RequestMethod.GET)
        public String throwUnauthorizedAccessException() throws Exception {
            throw new UnauthorizedAccessException("Unauthorized access exception for test user");
        }

        @RequestMapping(path = { "/record/notfound", "/record/notfound/inner" }, method = RequestMethod.GET)
        public String throwRecordNotFoundException() throws Exception {
            throw new RecordNotFoundException("Record not found exception for test user");
        }

        @RequestMapping(path = { "/record/step/notfound", "/record/step/notfound/inner" }, method = RequestMethod.GET)
        public String throwRecordStepNotFoundException() throws Exception {
            throw new RecordStepNotFoundException("Record step not found exception for test user", 3);
        }

        @RequestMapping(path = { "/record/step/data/notfound", "/record/step/data/notfound/inner" }, method = RequestMethod.GET)
        public String throwRecordStepDataNotFoundException() throws Exception {
            throw new RecordStepDataNotFoundException("Record step data not found exception for test user", 3);
        }

        @RequestMapping(path = { "/temporary", "/temporary/inner" }, method = RequestMethod.GET)
        public String throwTemporaryException() throws Exception {
            throw new TemporaryException("Temporary exception for test user");
        }

        @RequestMapping(path = { "/unknown", "/unknown/inner" }, method = RequestMethod.GET)
        public String throwUnknownException() throws Exception {
            throw new Exception("Unknown exception for test user");
        }

    }

}
