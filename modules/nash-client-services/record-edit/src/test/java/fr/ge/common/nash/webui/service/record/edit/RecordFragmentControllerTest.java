/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import fr.ge.common.nash.webui.service.record.edit.controller.RecordFragmentController;

/**
 * Tests {@link RecordFragmentController}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordFragmentControllerTest extends AbstractRecordControllerTest {

    /**
     * Tests
     * {@link RecordFragmentController#fragment(org.springframework.ui.Model, String, int, String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFragmentGroups() throws Exception {
        final String fragmentAsString = this.resourceAsString("groups.html");
        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/fragment/{path}", RECORD_UID, "group01[]") //
                .header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/fragment/main")) //
                .andReturn();

        assertEquals(fragmentAsString.replaceAll("[\r\n]+", " "), actual.getResponse().getContentAsString().replaceAll("(\n[ ]*)+\n", "\n").replaceAll("[\r\n]+", " "));
    }

    /**
     * Tests
     * {@link RecordFragmentController#fragment(org.springframework.ui.Model, String, int, String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFragmentData() throws Exception {
        final String fragmentAsString = this.resourceAsString("data.html");
        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/fragment/{path}", RECORD_UID, "group01[].group01sub[].group01data02[]") //
                .header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/fragment/main")) //
                .andReturn();

        assertEquals(fragmentAsString.replaceAll("[\r\n]+", " "), actual.getResponse().getContentAsString().replaceAll("(\n[ ]*)+\n", "\n").replaceAll("[\r\n]+", " "));
    }

    @Test
    public void testFragmentDataWithoutIndex() throws Exception {
        final String fragmentAsString = this.resourceAsString("data-no-index.html");
        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/fragment/{path}", RECORD_UID, "group01.group01sub.group01data02") //
                .header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/fragment/main")) //
                .andReturn();

        assertEquals(fragmentAsString.replaceAll("[\r\n]+", " "), actual.getResponse().getContentAsString().replaceAll("(\n[ ]*)+\n", "\n").replaceAll("[\r\n]+", " "));
    }

    /**
     * Tests
     * {@link RecordFragmentController#fragment(org.springframework.ui.Model, String, int, String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFragmentDataUsingSlash() throws Exception {
        final String fragmentAsString = this.resourceAsString("data-third.html");
        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/fragment/{path}", RECORD_UID, "group01/9/group01sub/3/group01data02/1") //
                .header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/fragment/main")) //
                .andReturn();

        assertEquals(fragmentAsString.replaceAll("[\r\n]+", " "), actual.getResponse().getContentAsString().replaceAll("(\n[ ]*)+\n", "\n").replaceAll("[\r\n]+", " "));
    }

    /**
     * Tests
     * {@link RecordFragmentController#fragment(org.springframework.ui.Model, String, int, String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFragmentDataUsingSlashWithoutIndex() throws Exception {
        final String fragmentAsString = this.resourceAsString("data-no-index.html");
        final MvcResult actual = this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/fragment/{path}", RECORD_UID, "group01/group01sub/group01data02") //
                .header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/fragment/main")) //
                .andReturn();

        assertEquals(fragmentAsString.replaceAll("[\r\n]+", " "), actual.getResponse().getContentAsString().replaceAll("(\n[ ]*)+\n", "\n").replaceAll("[\r\n]+", " "));
    }

}
