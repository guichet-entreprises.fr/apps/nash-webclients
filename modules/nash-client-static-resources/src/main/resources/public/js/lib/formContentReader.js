/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define(['jquery'], function($) {

    function FormContentData(map, currentPath) {
        if (currentPath) {
            this.currentPath = currentPath;
        } else {
            this.currentPath = '';
        }
        this.map = map;
    }

    FormContentData.prototype.withPath = function(path) {
        var newPath = this.currentPath;
        if (newPath != '' && path.indexOf('.') != 0 && path.indexOf('[') != 0) {
            newPath += '.';
        }
        newPath += path;
        return new FormContentData(this.map, newPath);
    }

    FormContentData.prototype.asMap = function(path) {
        if (path) {
            return this.withPath(path).asMap();
        }
        var res = {};
        for (var key in this.map) {
            if (!this.map.propertyIsEnumerable(key)) {
                continue;
            }

            if (key.indexOf(this.currentPath) == 0) {
                var keyCurrentPath = key.substring(this.currentPath.length);
                if (keyCurrentPath.startsWith('.')) {
                    keyCurrentPath = keyCurrentPath.substring(1);
                }
                res[keyCurrentPath] = this.map[key];
            }
        }
        return res;
    }

    FormContentData.prototype.asList = function(path) {
        if (path) {
            return this.withPath(path).asList();
        }
        var res = [];
        var value = this.map[this.currentPath];
        if (!value) {
            // field with several occurrences ([0] or .0)
            var listElementKeys = [];
            Object.keys(this.map, true).forEach(function (key) {
                if (key.indexOf(this.currentPath) == 0) {
                    var keyCurrentPath = key.substring(this.currentPath.length);
                    var regexGroups = /^(\[[0-9]+\]|\.[0-9]+).*$/g.exec(keyCurrentPath);
                    if (regexGroups) {
                        var listElementKey = regexGroups[1];
                        if (listElementKeys.indexOf(listElementKey) == -1) {
                            listElementKeys.push(listElementKey);
                            res.push(this.withPath(regexGroups[1]));
                        }
                    }
                }
            });
        } else {
            // field of type multiple
            for (var i = 0; i < value.length; ++i) {
                var subMap = {};
                subMap[''] = [value[i]];
                res.push(new FormContentData(subMap, ''));
            }
        }
        return res;
    }

    FormContentData.prototype.asObject = function (path) {
        if (path) {
            return this.withPath(path).asObject();
        }

        var res = null;
        var value = this.map[this.currentPath];
        if (!value) {
            if (this.asList().length > 0) {
                throw "Cannot get value as object when it is a list";
            }
        } else {
            if (value.length == 1) {
                res = value[0];
            } else {
                throw "Cannot get value as object when it is a list";
            }
        }

        return res;
    };

    FormContentData.prototype.asString = function(path) {
        return this.asObject(path);
    }

    FormContentData.prototype.asInt = function(path) {
        if (path) {
            return this.withPath(path).asInt();
        }
        var res = null;
        var stringValue = null;
        try {
            stringValue = this.asString();
        } catch (e) {
            throw "Cannot get value as Int when it is a list";
        }
        if (stringValue && /^-?[0-9]+$/.test(stringValue)) {
            var intValue = parseInt(stringValue);
            if (!isNaN(intValue)) {
                res = intValue;
            }
        }
        return res;
    }

    FormContentData.prototype.asBoolean = function(path) {
        if (path) {
            return this.withPath(path).asBoolean();
        }
        var res = false;
        var stringValue = null;
        try {
            stringValue = this.asString();
        } catch (e) {
            throw "Cannot get value as Boolean when it is a list";
        }
        var TRUE = [ 'true', 't', 'yes', 'y', 'on', '1' ];
        res = (TRUE.indexOf(stringValue) >= 0);
        return res;
    }

    function read(selector) {
        var frm = $(selector);
        var map = {};

        function add(key, value) {
            if (map[key]) {
                map[key].push(value);
            } else {
                map[key] = [ value ];
            }
        }

        frm.find('input, textarea').each(function (idx, elm) {
            switch (elm.type) {
            case 'checkbox':
            case 'radio':
                if (elm.checked) {
                    add(elm.name, { id : elm.value, label : $(elm).parent().text() });
                }
                break;
            default:
                add(elm.name, elm.value);
            }
        });

        frm.find('select').each(function (idx, elm) {
            $('option:selected', elm).each(function (_, opt) {
                add(elm.name, { id : opt.value, label : $(opt).text() });
            });
        });

        return new FormContentData(map);
    }

    return {
        read : read
    };

});
