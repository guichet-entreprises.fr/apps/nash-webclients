package fr.ge.common.nash.webui.service.record.edit.filter;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.HandlerMapping;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;
import fr.ge.ct.authentification.utils.CookieUtils;

/**
 * Assign record interceptor test.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordAssignInterceptorTest {

    /** Anonymous cookie name for testing. **/
    private static final String ANONYMOUS_COOKIE_NAME = "profiler";

    /** Record identifier for testing. **/
    private static final String UID_RECORD_1 = "2017-03-JON-DOE-00";

    /** Anonymous identifier for testing. **/
    private static final String UID_ANONYMOUS_AUTHOR = "2017-03-ABC-DEF-42";

    /** Authenticated identifier for testing. **/
    private static final String UID_AUTHENTICATED_AUTHOR = "2017-03-ABC-DEF-53";

    @InjectMocks
    private RecordAssignInterceptor interceptor;

    /** The record service. */
    @Mock
    protected IRecordService recordServiceClient;

    /** Anonymous profiler cookie name. **/
    @Mock
    private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

    /** HTTP request. **/
    @Mock
    private HttpServletRequest request;

    /** HTTP response. **/
    @Mock
    private HttpServletResponse response;

    /** HTTP session. **/
    @Mock
    private HttpSession httpSession;

    /** Servlet context. **/
    @Mock
    private ServletContext servletContext;

    /** User bean for testing. **/
    private LocalUserBean user = new LocalUserBean(UID_AUTHENTICATED_AUTHOR);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.request.getSession()).thenReturn(this.httpSession);
        Mockito.when(this.httpSession.getServletContext()).thenReturn(this.servletContext);
        Mockito.when(this.authentificationConfigurationWebBean.getCookieAnonymousName()).thenReturn(ANONYMOUS_COOKIE_NAME);
        Mockito.when(this.authentificationConfigurationWebBean.isCookieAnonymousAllow()).thenReturn(false);
        AccountContext.currentUser(this.user);
    }

    /**
     * No records assignment.
     * 
     * @throws Exception
     */
    @Test
    public void testAssignWithoutAnyCookie() throws Exception {
        Mockito.when(this.request.getCookies()).thenReturn(new Cookie[] {});

        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("code", UID_RECORD_1);
        Mockito.when(this.request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).thenReturn(pathVariables);

        RecordInfoBean recordInfo = new RecordInfoBean();
        recordInfo.setCode(UID_RECORD_1);
        recordInfo.setAuthor(UID_AUTHENTICATED_AUTHOR);
        Mockito.when(this.recordServiceClient.load(any())).thenReturn(recordInfo);

        assertTrue(interceptor.preHandle(request, response, null));

        Mockito.verify(this.recordServiceClient, Mockito.never()).assign(any(), any());
    }

    /**
     * Assign all records from anonymous user to authenticated user getting from
     * anonymous cookie.
     * 
     * @throws Exception
     */
    @Test
    public void testAssignWithCookie() throws Exception {
        final Cookie cookie = new Cookie(ANONYMOUS_COOKIE_NAME, CookieUtils.hash(UID_ANONYMOUS_AUTHOR));
        Mockito.when(this.request.getCookies()).thenReturn(new Cookie[] { cookie });

        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("code", UID_RECORD_1);
        Mockito.when(this.request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).thenReturn(pathVariables);

        final RecordSearchResult result = new RecordSearchResult();
        final RecordInfoBean recordInfo = new RecordInfoBean();
        recordInfo.setCode(UID_RECORD_1);
        recordInfo.setAuthor(UID_ANONYMOUS_AUTHOR);
        result.setTotalResults(1L);
        result.setContent(Arrays.asList(recordInfo));

        Mockito.when(this.recordServiceClient.own(UID_RECORD_1, UID_ANONYMOUS_AUTHOR)).thenReturn(true);
        Mockito.when(this.recordServiceClient.assign(UID_ANONYMOUS_AUTHOR, UID_AUTHENTICATED_AUTHOR)).thenReturn(Response.ok().build());

        assertTrue(interceptor.preHandle(this.request, this.response, null));

        Mockito.verify(this.recordServiceClient, Mockito.times(1)).own(UID_RECORD_1, UID_ANONYMOUS_AUTHOR);
        Mockito.verify(this.recordServiceClient).assign(UID_ANONYMOUS_AUTHOR, UID_AUTHENTICATED_AUTHOR);
    }

    /**
     * No assignment from application which does not allow anonymous session.
     * 
     * @throws Exception
     */
    @Test
    public void testNoAssignAnyRecordsWithNoCookie() throws Exception {
        Mockito.when(this.authentificationConfigurationWebBean.isCookieAnonymousAllow()).thenReturn(true);

        assertTrue(interceptor.preHandle(this.request, this.response, null));

        Mockito.verify(this.recordServiceClient, Mockito.never()).assign(any(), any());
    }
}
