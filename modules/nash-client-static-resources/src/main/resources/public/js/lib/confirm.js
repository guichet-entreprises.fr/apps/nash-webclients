define([ 'jquery' ], function ($) {

	$(document).on('click', 'a[data-toggle="confirm"]', function(evt) {
		var btn = $(this), url = btn.attr('href'), dlg = $('#confirmModal');

		evt.preventDefault();

		$('.modal-header > .modal-title', dlg).html(btn.data('confirm-title'));
		$('.modal-body', dlg).html(btn.data('confirm-content'));

		dlg.on('click', 'button[role="validate"]', function(evt) {
			var redirect = btn.attr("data-confirm-ajax");
			if(redirect!= "yes"){
				document.location = url;    
			}
		}).modal('show');
	});

});