/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.use.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class FormSpecificationController.
 *
 * @author Christian Cougourdan
 */
@Controller("specificationUseController")
public class FormSpecificationController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormSpecificationController.class);

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService formSpecService;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    @Autowired
    private HabilitationService habilitationService;

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    /**
     * Use.
     *
     * @param model
     *            the model
     * @param code
     *            the code
     * @return the string
     */
    @RequestMapping(path = "/form/{code}/use", method = RequestMethod.GET)
    public String useByCode(final Model model, @PathVariable("code") final String code) {

        final SpecificationInfoBean infoBean = this.formSpecService.load(code);
        if (null == infoBean) {
            return "form/error/notfound/main";
        }

        if (!this.canUseSpecification(infoBean)) {
            return "form/error/notallowed/main";
        }

        final Response downloaded = this.formSpecService.download(code);

        if (Status.NO_CONTENT.getStatusCode() == downloaded.getStatus()) {
            LOGGER.info("Try to instanciante unknown specification '{}'", code);
            return "form/error/notfound/main";
        }

        final String author = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse("*");
        final Response uploaded = this.recordService.upload(downloaded.readEntity(byte[].class), author, Arrays.asList(new SearchQueryFilter(StringUtils.join(defaultPrefixAuthor, author) + ":*")));

        if (Status.CREATED.getStatusCode() == uploaded.getStatus()) {
            model.addAttribute("code", uploaded.getHeaderString(HttpHeaders.LOCATION).replaceAll("^.*/([^/]+)$", "$1"));
            return "redirect:/record/{code}";
        }

        return "form/error/notfound/main";
    }

    /**
     * Reference.
     *
     * @param model
     *            the model
     * @param reference
     *            the reference
     * @return the string
     */
    @RequestMapping(path = "/form/use", method = RequestMethod.GET)
    public String useByReference(final Model model, @QueryParam("reference") final String reference) {

        final SpecificationInfoBean infoBean = this.formSpecService.loadByReference(reference);
        if (null == infoBean) {
            return "form/error/notfound/main";
        }

        if (!this.canUseSpecification(infoBean)) {
            return "form/error/notallowed/main";
        }

        final Response downloaded = this.formSpecService.downloadByReference(reference);

        if (Status.NO_CONTENT.getStatusCode() == downloaded.getStatus()) {
            LOGGER.info("Try to instanciante unknown specification '{}'", reference);
            return "form/error/notfound/main";
        }

        final String author = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse("*");
        final Response uploaded = this.recordService.upload(downloaded.readEntity(byte[].class), author, Arrays.asList(new SearchQueryFilter(StringUtils.join(defaultPrefixAuthor, author) + ":*")));

        if (Status.CREATED.getStatusCode() == uploaded.getStatus()) {
            model.addAttribute("code", uploaded.getHeaderString(HttpHeaders.LOCATION).replaceAll("^.*/([^/]+)$", "$1"));
            return "redirect:/record/{code}";
        }

        return "form/error/notfound/main";
    }

    /**
     * Comparing user and specification role. Redirect the user to an error page
     * if there is no matching. Let download the current specification if its
     * role is called "all".
     *
     * @param infoBean
     *            The specification information
     * @return true if the current user can use a specification
     */
    private boolean canUseSpecification(final SpecificationInfoBean infoBean) {
        final String specGroupe = Optional.ofNullable(infoBean).map(SpecificationInfoBean::getGroupe).orElse(null);
        final Map<String, List<String>> userRoles = this.habilitationService.getRoles(Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null));
        final Set<String> groups = new HashSet<>(userRoles.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
        return CollectionUtils.isNotEmpty(groups) && groups.contains(specGroupe.toLowerCase());
    }
}
