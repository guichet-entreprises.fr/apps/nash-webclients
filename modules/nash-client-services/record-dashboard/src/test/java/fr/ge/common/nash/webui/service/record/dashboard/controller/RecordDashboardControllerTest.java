/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.dashboard.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordDashboardControllerTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    private RecordDashboardController controller;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    @Test
    public void testWithoutChecker() throws Exception {
        this.mvc.perform(get("/dashboard")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("http://dashboard"));
    }

    @Test
    public void testWithOkCheckerAndDashboardUrl() throws Exception {
        final String dashboardUrl = "http://external/dashboard";

        this.controller.setUiDashboardUrl(dashboardUrl);

        this.mvc.perform(get("/dashboard")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl(dashboardUrl));
    }

    @Test
    public void testWithOkCheckerAndNoDashboardUrl() throws Exception {
        this.controller.setUiDashboardUrl(null);

        this.mvc.perform(get("/dashboard")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record"));
    }

    @Test
    public void testWithKoChecker() throws Exception {
        this.controller.setUiDashboardUrl(null);

        this.mvc.perform(get("/dashboard")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record"));
    }

}
