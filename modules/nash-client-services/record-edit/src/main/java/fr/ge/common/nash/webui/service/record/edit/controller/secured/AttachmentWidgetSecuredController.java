/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.webui.core.bean.ErrorMessage;
import fr.ge.common.nash.webui.core.bean.Message;
import fr.ge.common.nash.webui.core.bean.SuccessMessage;
import fr.ge.common.nash.webui.core.util.ControllerUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * File document controller managing @link {@link FileValueAdapter} operation.
 *
 * @author Christian Cougourdan
 */
@Component
public class AttachmentWidgetSecuredController {

    /** No corresponding attachment specification. */
    private static final String DATA_NOT_FOUND = "No corresponding attachment specification";

    private static final String UNEXPECTED_DESTINATION_TYPE = "Unexpected destination data type";

    private static final String UNEXPECTED_FILE_TYPE = "The file types expected : {0}";

    private static final String MAX_DOCUMENTS_REACHED = "{0} documents allowed";

    private static final String MAX_SIZE_REACHED = "Maximum allowed size : {0} Mo";

    /** The Constant FILE_PATTERN. */
    private static final String FILE_PATTERN = "%s%s-%d-%s";

    /**
     * Upload a new file document.
     *
     * @param loader
     *            record loader
     * @param stepIndex
     *            record step
     * @param path
     *            element path where type attribute is "File" or any inherit data
     *            type
     * @param file
     *            resource file to upload
     * @return data element value, representing contained documents and their order
     */
    public ResponseEntity<Message<AttachmentWidgetSecuredController.UploadResponse>> upload(final SpecificationLoader loader, final int stepIndex, final String path, final MultipartFile file,
            final Locale locale) {
        final StepElement step = loader.stepsMgr().find(stepIndex);
        final FormSpecificationData spec = loader.data(step);
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>("No attachment to save"));
        }

        final DataElement dataElement = (DataElement) SpecificationLoader.find(spec.getGroups(), path);

        if (null == dataElement) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>(DATA_NOT_FOUND, new UploadResponse(ControllerUtil.translateIdToHtml(path))));
        }

        final String name = file.getOriginalFilename();

        final IValueAdapter<?> adapter = ValueAdapterFactory.type(loader, dataElement);
        if (!(adapter instanceof FileValueAdapter)) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>(UNEXPECTED_DESTINATION_TYPE, new UploadResponse(ControllerUtil.translateIdToHtml(path))));
        }

        final FileValueAdapter fileType = (FileValueAdapter) adapter;

        if (!this.validateUploadedFileExtension(fileType, name)) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>(UNEXPECTED_FILE_TYPE, new UploadResponse(ControllerUtil.translateIdToHtml(path)), fileType.getExt()));
        }

        final List<FileValueAdapter.Item> values = fileType.get(dataElement);

        // -->Control maximum documents allowed
        if (null != fileType.getSplit() && fileType.getSplit() <= values.size()) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>(MAX_DOCUMENTS_REACHED, new UploadResponse(ControllerUtil.translateIdToHtml(path)), fileType.getSplit()));
        }
        // <--

        // -->Control maximum size allowed if necessary
        if (null != fileType.getSize() && (fileType.getSize() * 1024L * 1024L) <= file.getSize()) {
            return ResponseEntity.badRequest().body(new ErrorMessage<>(MAX_SIZE_REACHED, new UploadResponse(ControllerUtil.translateIdToHtml(path)), fileType.getSize()));
        }
        // <--

        try {
            final byte[] contentAsBytes = file.getBytes();
            final FileValueAdapter.Item item = FileValueAdapter.createItem(-1, name, file.getContentType(), () -> contentAsBytes);
            values.add(item);
            fileType.set(loader, loader.provider(step.getData()), dataElement, values);
        } catch (final IOException ex) {
            throw new TechnicalException("Attachment upload : unable to read file", ex);
        }

        loader.save(stepIndex, spec);

        return ResponseEntity.ok(new SuccessMessage<>("Attachment " + name + " saved", new AttachmentWidgetSecuredController.UploadResponse(ControllerUtil.translateIdToHtml(path),
                values.stream().map(AttachmentWidgetSecuredController.ResponseItem::new).collect(Collectors.toList()))));
    }

    /**
     * Check for valid document file extension.
     *
     * @param fileType
     *            file widget
     * @param name
     *            document name
     * @return true if file extension accepted
     */
    private boolean validateUploadedFileExtension(final FileValueAdapter fileType, final String name) {
        if (StringUtils.isEmpty(fileType.getExt())) {
            return true;
        }

        return Arrays.asList(fileType.getExt().split("[, ]+")).stream().anyMatch(name::endsWith);
    }

    /**
     * Download document file resource.
     *
     * @param loader
     *            record loader
     * @param stepIndex
     *            step index
     * @param id
     *            data element full identifier
     * @param idx
     *            document index
     * @param name
     *            document name
     * @return resource binary as HTTP response
     */
    public ResponseEntity<?> download(final SpecificationLoader loader, final int stepIndex, final String id, final int idx, final String name) {

        final String cleanId = ControllerUtil.translateIdFromHtml(id);

        final StepElement step = loader.stepsMgr().find(stepIndex);
        final FormSpecificationData spec = loader.data(step);

        final DataElement dataElement = (DataElement) SpecificationLoader.find(spec.getGroups(), cleanId);

        if (null == dataElement) {
            return ResponseEntity.noContent().build();
        }

        final FileEntry fileEntry = loader.provider(step.getData()).load(String.format(FILE_PATTERN, ValueAdapterFactory.type(spec, dataElement).externalPath(), cleanId, idx, name));

        if (fileEntry == null) {
            return ResponseEntity.noContent().build();
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("inline;  filename=%s", name));
        // headers.add(HttpHeaders.CONTENT_TYPE, fileEntry.type());

        return new ResponseEntity<>(fileEntry.asBytes(), headers, HttpStatus.OK);
    }

    /**
     * Reorder documents in data element.
     *
     * @param loader
     *            record loader
     * @param stepIndex
     *            step index
     * @param id
     *            data element full identifier
     * @param keys
     *            order keys
     * @return data element values
     */
    public ResponseEntity<Message<AttachmentWidgetSecuredController.UploadResponse>> reorder(final SpecificationLoader loader, final int stepIndex, final String id, final List<Integer> keys) {
        final StepElement step = loader.stepsMgr().find(stepIndex);
        final IProvider stepProvider = loader.provider(step.getData());
        final FormSpecificationData spec = loader.data(step);

        final DataElement dataElement = (DataElement) SpecificationLoader.find(spec.getGroups(), id);

        if (null == dataElement) {
            return ResponseEntity.ok(new ErrorMessage<>(DATA_NOT_FOUND, null));
        }

        final FileValueAdapter fileType = new FileValueAdapter();
        final List<FileValueAdapter.Item> values = fileType.get(stepProvider, dataElement);

        final Map<Integer, FileValueAdapter.Item> byRef = new HashMap<>();
        values.forEach(v -> byRef.put(v.getId(), v));

        final List<FileValueAdapter.Item> orderedValues = keys.stream().map(byRef::get).map(AttachmentWidgetSecuredController.ResponseItem::new).collect(Collectors.toList());

        if (orderedValues.size() != values.size()) {
            return ResponseEntity.ok(new ErrorMessage<>("Bad parameter IDs list", null));
        }

        fileType.set(loader, stepProvider, dataElement, orderedValues);
        loader.save(stepIndex, spec);

        final String htmlId = ControllerUtil.translateIdToHtml(id);
        return ResponseEntity.ok( //
                new SuccessMessage<>( //
                        String.format("Attachments reordered on [%s]", htmlId), //
                        new AttachmentWidgetSecuredController.UploadResponse(htmlId, orderedValues) //
                ) //
        );
    }

    public ResponseEntity<?> empty(final SpecificationLoader loader, final int stepIndex, final String id) {
        final StepElement step = loader.stepsMgr().find(stepIndex);
        final FormSpecificationData spec = loader.data(step);

        if (null == spec) {
            return ResponseEntity.noContent().build();
        }

        final DataElement dataElement = (DataElement) SpecificationLoader.find(spec.getGroups(), id);

        if (null == dataElement) {
            return ResponseEntity.ok(new ErrorMessage<>(DATA_NOT_FOUND, null));
        }

        final FileValueAdapter fileType = new FileValueAdapter();

        final int cnt = fileType.get(dataElement).size();
        fileType.set(loader, loader.provider(step.getData()), dataElement, null);

        loader.save(stepIndex, spec);

        return ResponseEntity
                .ok(new SuccessMessage<>(String.format("%d attachments removed on [%s]", cnt, id), new AttachmentWidgetSecuredController.UploadResponse(ControllerUtil.translateIdToHtml(id))));
    }

    /**
     * The Class UploadResponse.
     */
    public static class UploadResponse {

        /** The id. */
        private final String id;

        /** The attachments. */
        private final List<FileValueAdapter.Item> attachments;

        /**
         * Instantiates a new upload response.
         *
         * @param id
         *            the id
         */
        public UploadResponse(final String id) {
            this(id, new ArrayList<>());
        }

        /**
         * Instantiates a new upload response.
         *
         * @param id
         *            the id
         * @param attachments
         *            the attachments
         */
        public UploadResponse(final String id, final List<FileValueAdapter.Item> attachments) {
            this.id = id;
            this.attachments = Optional.ofNullable(attachments).map(src -> new ArrayList<>(src)).orElse(null);
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public String getId() {
            return this.id;
        }

        /**
         * Gets the attachments.
         *
         * @return the attachments
         */
        public List<FileValueAdapter.Item> getAttachments() {
            return this.attachments;
        }

    }

    /**
     * The Class ResponseItem.
     */
    @JsonIgnoreProperties({ "mimeType", "content", "hash" })
    public static class ResponseItem extends FileValueAdapter.Item {

        /**
         * Instantiates a new response item.
         *
         * @param src
         *            the src
         */
        public ResponseItem(final FileValueAdapter.Item src) {
            super(src.getId(), src.getLabel(), null, () -> null);
        }

    }

}
