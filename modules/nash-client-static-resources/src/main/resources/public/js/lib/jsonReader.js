/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'bootstrap-treeview' ], function($) {

    function buildData(input, parentText, i18n) {
        var data;
        if (input != null && typeof input === 'object') {
            data = [];
            for (var obj in input) {
                var displayText;
                if (Array.isArray(input)) {
                    displayText = parentText + ' ' + (parseInt(obj) + 1) + '/' + input.length;
                } else {
                    displayText = i18n(obj);
                }
                var recData = buildData(input[obj], displayText, i18n);
                if (typeof recData === 'object' && Array.isArray(recData)) {
                    var node = {};
                    node['text'] = displayText;
                    node['nodes'] = recData;
                    if (typeof input[obj] === 'object' && Array.isArray(input[obj])) {
                        node['tags'] = [input[obj].length];
                    }
                    data.push(node);
                } else {
                    data.push(recData);
                }
            }
        } else {
            var node = {};
            var displayText = i18n(parentText) + ' : ';
            if (input == null || (typeof input === 'string' && input.length == 0)) {
                node['state'] = {
                    'disabled': true
                };
            } else if (typeof 'boolean') {
                displayText += i18n(input);
            } else {
                displayText += input;
            }
            node['text'] = displayText;
            return node;
        }
        return data;
    }

    function update(readerDiv, json, i18n) {
        // https://github.com/jonmiles/bootstrap-treeview#options
        readerDiv.treeview({
            data: buildData(json, null, i18n),
            color: '#333',
            highlightSelected: false,
            levels: 1,
            showBorder: false,
            showTags: true
        }).on('click', function (e) {
            var nodeid = $(e.target).data('nodeid');
            $(this).treeview('toggleNodeExpanded', nodeid, {
                silent: true
            });
        });
    }

    return {
        update : update
    };

});
