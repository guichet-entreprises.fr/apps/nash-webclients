/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.authentication.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.utils.test.AbstractRestTest;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;
import fr.ge.directory.ws.v1.service.IAuthorityService;

/**
 * Testing habilitations service.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HabilitationServiceTest.SpringConfiguration.class)
public class HabilitationServiceTest extends AbstractRestTest {

    private static final String USER_ID = "2018-02-ZNC-FNQ-49";

    private static final String AUTHORITY_ENTITY_ID = "CA78158";

    @Autowired
    private HabilitationService habilitationService;

    /** Service to manage authority. **/
    @Autowired
    private IAuthorityService authorityService;

    /** Service to manage user rights. **/
    @Autowired
    private AuthorityServicesWebService authorityUserRightsRestService;

    @Configuration
    @ImportResource({ "classpath:spring/test-context.xml" })
    public static class SpringConfiguration {

        @Bean
        public HabilitationService getHabilitationService() {
            return new HabilitationService();
        }

        @Bean(name = "defaultJsonProvider")
        public JacksonJaxbJsonProvider getJsonProvider() {
            final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });
        }

    }

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    @Before
    public void setUp() throws Exception {
        reset(this.authorityService, this.authorityUserRightsRestService);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBeans(Arrays.asList(this.authorityService, this.authorityUserRightsRestService));
        serverFactory.setProvider(jsonProvider);

        this.server = serverFactory.create();

        this.providers = Arrays.asList(jsonProvider);
    }

    @Test
    public void testDirectoryThrowsExceptionNoUserId() throws TechniqueException, FunctionalException {
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenThrow(new TechniqueException("Directory not found"));
        final Map<String, List<String>> roles = this.habilitationService.getRoles(USER_ID);
        assertEquals(roles.size(), 1);
    }

    @Test
    public void testNoUserId() {
        final Map<String, List<String>> roles = this.habilitationService.getRoles(null);
        assertNull(roles);
    }

    @Test
    public void testNoRolesDefined() throws TechniqueException, FunctionalException {
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(null);
        final Map<String, List<String>> roles = this.habilitationService.getRoles(USER_ID);
        assertEquals(roles.size(), 1);
    }

    @Test
    public void testOneRoleDefined() throws TechniqueException, FunctionalException {
        final ResponseAuthorityRoleBean responseAuthorityRoleBean = new ResponseAuthorityRoleBean();
        responseAuthorityRoleBean.setRoles(Arrays.asList("all"));

        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(Arrays.asList(responseAuthorityRoleBean));
        final Map<String, List<String>> roles = this.habilitationService.getRoles(USER_ID);
        assertEquals(roles.size(), 1);
        assertThat(roles.keySet(), hasItems(USER_ID));
    }

    @Test
    public void testMutipleRolesDefined() throws TechniqueException, FunctionalException {
        final Map<String, List<String>> rights = new HashMap<String, List<String>>();
        rights.put(USER_ID, Arrays.asList("all", "referent", "accountant"));

        when(this.authorityUserRightsRestService.findUserRights(Mockito.anyString())).thenReturn(rights);
        final Map<String, List<String>> roles = this.habilitationService.getRoles(USER_ID);
        assertEquals(roles.size(), 1);
    }

    @Test
    public void testHasRoleForAuthority() throws TechniqueException, FunctionalException {
        final ResponseAuthorityRoleBean responseAuthorityRoleBean = new ResponseAuthorityRoleBean();
        responseAuthorityRoleBean.setEntityId(AUTHORITY_ENTITY_ID);
        responseAuthorityRoleBean.setRoles(Arrays.asList("user", "referent"));
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(Arrays.asList(responseAuthorityRoleBean));
        assertTrue(this.habilitationService.hasRoleForAuthority(USER_ID, AUTHORITY_ENTITY_ID, Arrays.asList("user", "referent")));

        responseAuthorityRoleBean.setRoles(Arrays.asList("user"));
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(Arrays.asList(responseAuthorityRoleBean));
        assertTrue(this.habilitationService.hasRoleForAuthority(USER_ID, AUTHORITY_ENTITY_ID, Arrays.asList("user", "referent")));
    }

    @Test
    public void testWrongRoleForAuthority() throws TechniqueException, FunctionalException {
        final ResponseAuthorityRoleBean responseAuthorityRoleBean = new ResponseAuthorityRoleBean();
        responseAuthorityRoleBean.setEntityId(AUTHORITY_ENTITY_ID);
        responseAuthorityRoleBean.setRoles(Arrays.asList("accountant"));
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(Arrays.asList(responseAuthorityRoleBean));
        assertFalse(this.habilitationService.hasRoleForAuthority(USER_ID, AUTHORITY_ENTITY_ID, Arrays.asList("user", "referent")));
    }

    @Test
    public void testNoRoleForAuthority() throws TechniqueException, FunctionalException {
        final ResponseAuthorityRoleBean responseAuthorityRoleBean = new ResponseAuthorityRoleBean();
        responseAuthorityRoleBean.setEntityId(AUTHORITY_ENTITY_ID);
        when(this.authorityService.findAuthoritiesByUserId(Mockito.anyString())).thenReturn(Arrays.asList(responseAuthorityRoleBean));
        assertFalse(this.habilitationService.hasRoleForAuthority(USER_ID, AUTHORITY_ENTITY_ID, Arrays.asList("user", "referent")));
    }
}
