/**
 *
 */
package fr.ge.common.nash.webui.core.loader.event.listener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.StorageEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component
public class StorageListener {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageListener.class);

    /** The storage record root. */
    @Value("${storage.record.root:#{null}}")
    private String storageRecordRoot;

    /** The storage record root. */
    @Value("${record.memory.keep:true}")
    private boolean recordMemoryKeep;

    /** The storage record root. */
    @Value("${record.archive.partners:false}")
    private boolean recordStorageArchivePartners;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The record service. */
    @Autowired
    private IRecordStorage recordStorage;

    /** Storage REST Service. **/
    @Autowired
    private IStorageRestService storageRestService;

    /**
     * Mutateur sur l'attribut {@link #storageRecordRoot}.
     *
     * @param storageRecordRoot
     *            la nouvelle valeur de l'attribut storageRecordRoot
     */
    public void setStorageRecordRoot(final String storageRecordRoot) {
        this.storageRecordRoot = storageRecordRoot;
    }

    /**
     * Mutateur sur l'attribut {@link #recordMemoryKeep}.
     *
     * @param recordMemoryKeep
     *            la nouvelle valeur de l'attribut recordMemoryKeep
     */
    public void setRecordMemoryKeep(final boolean recordMemoryKeep) {
        this.recordMemoryKeep = recordMemoryKeep;
    }

    /**
     * Mutateur sur l'attribut {@link #recordStorageArchivePartners}.
     *
     * @param recordStorageArchivePartners
     *            la nouvelle valeur de l'attribut recordStorageArchivePartners
     */
    public void setRecordStorageArchivePartners(final boolean recordStorageArchivePartners) {
        this.recordStorageArchivePartners = recordStorageArchivePartners;
    }

    @Subscribe
    public void onStorageEvent(final StorageEvent event) throws TechnicalException {
        final SpecificationLoader loader = event.getLoader();

        try {
            this.store(loader);
            this.archive(loader);
            this.remove(loader);
        } catch (final Exception ex) {
            loader.updateStepStatus(loader.lastStepDone().getPosition(), StepStatusEnum.TO_DO, loader.description().getAuthor());
            loader.flush();
            LOGGER.debug("Update last step done to status 'todo'");
            throw new TechnicalException(ex);
        }
    }

    private void store(final SpecificationLoader loader) {
        if (StringUtils.isBlank(this.storageRecordRoot)) {
            return;
        }

        CoreUtil.time("Store record", () -> {
            final String code = loader.description().getRecordUid();
            LOGGER.info("Try to store record into filesystem with code {}", code);

            final Response response = this.recordService.download(code);
            if (response != null && Status.OK.getStatusCode() == response.getStatus()) {
                try {
                    this.recordStorage.storeRecord(code, (InputStream) response.getEntity(), loader.description().getReference().getCode());
                } catch (final TemporaryException ex) {
                    LOGGER.error("An error occured when trying to store record", ex);
                    throw new TechnicalException(ex);
                }
            }
        });
    }

    private void archive(final SpecificationLoader loader) {
        if (!this.recordStorageArchivePartners) {
            return;
        }

        final String code = loader.description().getRecordUid();

        LOGGER.info("Try to store record with code {} to the storage queue '{}'", code, StorageTrayEnum.ARCHIVED_PARTNERS.getName());

        final Response download = this.recordService.download(code);
        if (download != null && Status.OK.getStatusCode() == download.getStatus()) {
            try {
                final InputStream resourceAsStream = (InputStream) download.getEntity();

                final List<SearchQueryFilter> metas = new ArrayList<>();
                metas.addAll( //
                        Arrays.asList(//
                                new SearchQueryFilter("author", loader.description().getAuthor()), //
                                new SearchQueryFilter("description", loader.description().getDescription()), //
                                new SearchQueryFilter("title", loader.description().getTitle()) //
                        ) //
                );

                Optional.of(loader) //
                        .filter(l -> null != l) //
                        .map(SpecificationLoader::meta) //
                        .filter(m -> null != m) //
                        .map(FormSpecificationMeta::getMetas) //
                        .filter(l -> null != l) //
                        .orElse(new ArrayList<MetaElement>()) //
                        .forEach(m -> metas.add(new SearchQueryFilter(StringUtils.join(m.getName(), ":", m.getValue()))));

                final Response response = this.storageRestService.store( //
                        StorageTrayEnum.ARCHIVED_PARTNERS.getName(), //
                        new Attachment("storageUploadFile", MediaType.APPLICATION_OCTET_STREAM, resourceAsStream), //
                        String.format("%s.zip", code), //
                        code, //
                        metas //
                );
                if (Status.OK.getStatusCode() == response.getStatus()) {
                    LOGGER.info("Archive record {} to the storage queue {} with success", code, StorageTrayEnum.ARCHIVED_PARTNERS.getName());
                } else {
                    throw new TechnicalException(String.format("Unable to archive record %s : storage returned %s", code, response.getStatus()));
                }
            } catch (final ProcessingException ex) {
                throw new TechnicalException(String.format("Unable to archive record %s : storage returned %s", code), ex);
            }
        }
    }

    private void remove(final SpecificationLoader loader) {
        if (this.recordMemoryKeep) {
            return;
        }

        final String code = loader.description().getRecordUid();

        LOGGER.info("Try to remove record with code {}", code);
        final Response response = this.recordService.remove(code);
        if (response != null && Status.OK.getStatusCode() == response.getStatus()) {
            LOGGER.info("Remove record for code {} with success", code);
        } else {
            LOGGER.warn("Unable to remove record for code {}", code);
            throw new TechnicalException(String.format("Unable to remove record for code %s", code));
        }
    }

}
