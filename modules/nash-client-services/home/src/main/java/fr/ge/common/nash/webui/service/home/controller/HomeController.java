/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.home.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;

/**
 * The Class HomeController.
 *
 * @author Christian Cougourdan
 */
@Controller
public class HomeController {

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService service;

    /** Habilitation service. **/
    @Autowired
    private HabilitationService habilitationService;

    /** Authentification configuration. **/
    @Autowired
    private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

    /**
     * Home.
     *
     * @param model
     *            the model
     * @return the string
     */
    @RequestMapping("/")
    public String home(final Model model) {
        return "home/main";
    }

    @RequestMapping("/model/search/enhanced/data")
    @ResponseBody
    public ResponseEntity<Object> search(final SearchQuery query) {
        Map<String, List<String>> userRoles = this.habilitationService.getRoles(Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null));

        // -->Adding values to filter 'groupe' column
        final Set<String> groups = new HashSet<>(userRoles.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
        query.addFilter(new SearchQueryFilter("groupe", ":", groups));

        final SearchResult<SpecificationInfoBean> searchResult = this.service.fullTextSearch( //
                query.getStartIndex(), //
                query.getMaxResults(), //
                query.getFilters(), //
                query.getOrders(), //
                query.getSearchTerms() //
        );

        searchResult.getContent().stream().forEach(bean -> {
            bean.setAction(bean.getTitle());
            ObjectMapper mapper = new ObjectMapper();
            if (bean.getDetails().hasNonNull("description")) {
                Map<String, String> result = mapper.convertValue(bean.getDetails(), Map.class);
                if (result.get("description") != null) {
                    bean.setDescription(result.get("description"));
                } else {
                    bean.setDescription("");
                }
            }
        });

        return ResponseEntity.ok(searchResult);
    }

}
