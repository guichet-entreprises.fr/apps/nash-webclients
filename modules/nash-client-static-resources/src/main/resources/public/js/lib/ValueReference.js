/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ '_fm_data' ], function(data) {

    var keyPattern = /([^\[]+)(?:\[([^\]]*)\])?/ ;
    var _page = data._page;

    function buildKey(key) {
        var keySplitted = keyPattern.exec(key);
        return {
            name: keySplitted[1],
            position: '' === keySplitted[2] ? 0 : undefined === keySplitted[2] ? undefined : parseInt(keySplitted[2])
        };
    }

    var ValueReference = function (name) {
        var path = name.split('.');
        var k, o = _page;

        this.key = buildKey(path.pop());

        while (k = path.shift()) {
            var keyParts = buildKey(k);
            if (undefined === keyParts.position) {
                /*
                 * Simple data/group element
                 */
                o = o[k]
            } else {
                /*
                 * Multi data/group element
                 */
                o = o[keyParts.name][keyParts.position];
            }
        }

        this.parent = o;
    };

    ValueReference.create = function (name) {
        return new ValueReference(name);
    };

    ValueReference.prototype.set = function (value) {
        if (undefined === this.key.position) {
            this.parent[this.key.name] = value;
        } else {
            var lst = this.parent[this.key.name]
            if (undefined === lst) {
                lst = new Array(this.key.position + 1);
                lst.fill(null, 0, this.key.position);
            }
            lst[this.key.position] = value;
            this.parent[this.key.name] = lst;
        }
        return this;
    };

    ValueReference.prototype.get = function () {
        if (undefined === this.key.position) {
            return this.parent[this.key.name];
        } else {
            return this.parent[this.key.name][this.key.position];
        }
    };

    return ValueReference;

});
