/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.ValueAdapterInfo;
import fr.ge.common.nash.engine.adapter.ValueAdapterInfo.ValueAdapterOptionInfo;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.webui.service.devtools.bean.OptionDescriptionBean;
import fr.ge.common.nash.webui.service.devtools.engine.SpecificationLoaderMock;
import fr.ge.common.utils.web.search.bean.DatatableSearchQuery;
import fr.ge.common.utils.web.search.bean.DatatableSearchResult;

/**
 * The Class EngineController.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping(value = "/engine/types")
public class TypesEngineController {

    /** Colon. */
    private static final char COLON = ':';

    /** Comma. */
    private static final char COMMA = ',';

    /** Left parenthesis. */
    private static final char LPAR = '(';

    /** Quote. */
    private static final char QUOTE = '\'';

    /** Right parenthesis. */
    private static final char RPAR = ')';

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Constructor.
     */
    public TypesEngineController() {
        // Nothing to do.
    }

    /**
     * Inits the types.
     *
     * @param model
     *            the model
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init(final Model model) {
        final List<String> versions = ValueAdapterFactory.getValueAdapterVersions().entrySet().stream().map(version -> version.getKey()).collect(Collectors.toList());
        versions.add(ValueAdapterFactory.getLatestValueAdapterVersion());
        Collections.reverse(versions);
        model.addAttribute("versions", versions);
        return "engine/types/main";
    }

    /**
     * Gets the datatable types by version.
     *
     * @param criteria
     *            the search criteria
     * @param version
     *            the version
     * @param name
     *            the name
     * @return the types
     */
    @RequestMapping(value = "/byversion/datatable", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public DatatableSearchResult<String> requestDatatable(final DatatableSearchQuery criteria, @RequestParam(value = "version", required = false) final String version,
            @RequestParam(value = "name", required = false) final String name) {
        int draw = 0;
        int start = 0;
        int length = 0;
        if (criteria != null) {
            if (criteria.getDraw() != null) {
                draw = criteria.getDraw();
            }
            if (criteria.getStart() != null && criteria.getLength() != null) {
                start = criteria.getStart();
                length = criteria.getLength();
            }
        }
        final List<String> filteredTypesByVersion = this.getTypesByVersion(version, name);
        final List<String> filteredTypesByVersionPage = new ArrayList<>();
        final int resultStart = start;
        final int resultEnd = Math.min(start + length, filteredTypesByVersion.size());
        for (int i = resultStart; i < resultEnd; ++i) {
            final String typeKey = filteredTypesByVersion.get(i);
            filteredTypesByVersionPage.add(ValueAdapterFactory.type(version, typeKey).name());
        }
        final DatatableSearchResult<String> datatableSearchResult = new DatatableSearchResult<>(draw);
        datatableSearchResult.setData(filteredTypesByVersionPage);
        datatableSearchResult.setRecordsFiltered(filteredTypesByVersion.size());
        datatableSearchResult.setRecordsTotal(filteredTypesByVersion.size());
        return datatableSearchResult;
    }

    /**
     * Gets the gallery types by version.
     *
     * @param model
     *            the model
     * @param version
     *            the version
     * @return the types
     */
    @RequestMapping(value = "/byversion/gallery", method = RequestMethod.GET)
    public String requestGallery(final Model model, @RequestParam(value = "version", required = false) final String version) {
        final List<String> typesByVersion = this.getTypesByVersion(version, null);
        final List<String> typesByVersionDisplay = new ArrayList<>();
        for (int i = 0; i < typesByVersion.size(); ++i) {
            final String typeKey = typesByVersion.get(i);
            typesByVersionDisplay.add(ValueAdapterFactory.type(version, typeKey).name());
        }
        model.addAttribute("version", version);
        model.addAttribute("types", typesByVersionDisplay);
        return "engine/types/gallery";
    }

    /**
     * Gets the types by version.
     *
     * @param version
     *            the version
     * @param name
     *            the name
     * @return the types
     */
    private List<String> getTypesByVersion(final String version, final String name) {
        List<String> allTypes = null;
        List<String> filteredTypes = null;
        if (StringUtils.isNotEmpty(version)) {
            final Map<String, IValueAdapter<?>> versionMap = ValueAdapterFactory.getVersionedTypes().get(version);
            allTypes = new ArrayList<>(versionMap.keySet());
            filteredTypes = new ArrayList<>();
            if (StringUtils.isNotEmpty(name)) {
                final String lowerName = name.toLowerCase(Locale.getDefault());
                for (final String filteredType : allTypes) {
                    if (filteredType.contains(lowerName)) {
                        filteredTypes.add(filteredType);
                    }
                }
            } else {
                filteredTypes = allTypes;
            }
            Collections.sort(filteredTypes);
        }

        if (filteredTypes == null) {
            return Collections.<String>emptyList();
        } else {
            return filteredTypes;
        }
    }

    /**
     * Gets a type description.
     *
     * @param model
     *            the model
     * @param version
     *            the version
     * @param type
     *            the type
     * @param dataElement
     *            the data element
     * @param allRequestParams
     *            all request params
     * @return the type description
     */
    @RequestMapping(value = "/description", method = RequestMethod.GET)
    public String requestDescription(final Model model, @RequestParam(value = "version") final String version, @RequestParam(value = "type") final String type,
            @ModelAttribute(value = "dataElement") final DataElement dataElement, @RequestParam final Map<String, String> allRequestParams) {

        this.updateTypeInfoCommon(model, version, type, dataElement, allRequestParams);
        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(version, type);

        model.addAttribute("msgTypeDescription", NashMessageReader.getReader(valueAdapter).getFormatter().format(valueAdapter.description()));

        model.addAttribute("typeTranslations", Collections.emptyMap());
        return "engine/types/description";
    }

    /**
     * Gets a type XML.
     *
     * @param model
     *            the model
     * @param version
     *            the version
     * @param type
     *            the type
     * @param dataElement
     *            the data element
     * @param allRequestParams
     *            all request params
     * @return the type description
     */
    @RequestMapping(value = "/xml", method = RequestMethod.GET)
    public String requestXml(final Model model, @RequestParam(value = "version") final String version, @RequestParam(value = "type") final String type,
            @ModelAttribute(value = "dataElement") final DataElement dataElement, @RequestParam final Map<String, String> allRequestParams) {

        this.updateTypeInfoCommon(model, version, type, dataElement, allRequestParams);
        final String xml = JaxbFactoryImpl.instance().fragmentAsString(dataElement).replaceAll("\r", "");
        model.addAttribute("xml", xml);
        return "engine/types/xml";
    }

    /**
     * Gets a type display.
     *
     * @param model
     *            the model
     * @param version
     *            the version
     * @param type
     *            the type
     * @param dataElement
     *            the data element
     * @param allRequestParams
     *            all request params
     * @return the type display
     */
    @RequestMapping(value = "/display", method = RequestMethod.GET)
    public String requestDisplay(final Model model, @RequestParam(value = "version") final String version, @RequestParam(value = "type") final String type,
            @ModelAttribute(value = "dataElement") final DataElement dataElement, @RequestParam final Map<String, String> allRequestParams) {
        this.updateTypeInfoCommon(model, version, type, dataElement, allRequestParams);

        final GroupElement groupElement = new GroupElement();
        groupElement.setId("testGroup");
        groupElement.setData(Arrays.asList(dataElement));
        final FormSpecificationData spec = new FormSpecificationData();
        spec.setId("typeDisplay");
        spec.setGroups(Arrays.asList(groupElement));

        // prepare i18n
        final String specLang = "en";
        final String userLang = LocaleContextHolder.getLocale().getLanguage();
        final boolean i18nAvailable = !specLang.equals(userLang);

        model.addAttribute("spec", spec);
        model.addAttribute("specLoader", SpecificationLoaderMock.create(this.applicationContext));
        model.addAttribute("specLang", specLang);
        model.addAttribute("i18nLang", userLang);
        model.addAttribute("i18nAvailable", i18nAvailable);
        model.addAttribute("singleTranslation", false);
        model.addAttribute("grp", groupElement);
        model.addAttribute("context", Collections.emptyMap());
        model.addAttribute("version", version);

        return "engine/types/display";
    }

    /**
     * Gets a type detail.
     *
     * @param model
     *            the model
     * @param version
     *            the version
     * @param type
     *            the type
     * @param dataElement
     *            the data element
     * @param allRequestParams
     *            all request params
     */
    private void updateTypeInfoCommon(final Model model, final String version, final String type, final DataElement dataElement, final Map<String, String> allRequestParams) {

        final Map<String, String> optionValuesByNames = new HashMap<>();
        if (null != allRequestParams) {
            final Pattern pattern = Pattern.compile("options\\.(.+)");
            for (final Map.Entry<String, String> entry : allRequestParams.entrySet()) {
                final Matcher m = pattern.matcher(entry.getKey());
                if (m.matches()) {
                    optionValuesByNames.put(m.group(1), entry.getValue());
                }
            }
        }

        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(version, type);

        // set default data element attributes values
        DataElement updatedDataElement = dataElement;
        if (updatedDataElement == null || updatedDataElement.getId() == null) {
            updatedDataElement = new DataElement();
            updatedDataElement.setId("id");
            updatedDataElement.setLabel("label");
            updatedDataElement.setType(type);
            updatedDataElement.setMandatory(Boolean.FALSE);
        } else {
            updatedDataElement.setMandatory(BooleanUtils.isTrue(updatedDataElement.getMandatory()));
        }

        model.addAttribute("name", valueAdapter.name());
        model.addAttribute("version", version);
        model.addAttribute("dataElement", updatedDataElement);

        model.addAttribute("optionsBase", this.getBaseProperties(updatedDataElement));
        model.addAttribute("optionsNotice", this.getNoticesProperties(updatedDataElement));
        model.addAttribute("optionsSpecial", this.getSpecialProperties(updatedDataElement, valueAdapter, optionValuesByNames, type));
    }

    /**
     * Getter on attribute {@link #special properties}.
     *
     * @param dataElement
     *            data element
     * @param valueAdapter
     *            value adapter
     * @param optionValuesByName
     *            option values by name
     * @param type
     *            type
     * @return special properties
     */
    private List<OptionDescriptionBean> getSpecialProperties(final DataElement dataElement, final IValueAdapter<?> valueAdapter, final Map<String, String> optionValuesByName, final String type) {

        final List<OptionDescriptionBean> descriptions = new ArrayList<>();

        final List<String> typeWithOptions = new ArrayList<>();
        for (final ValueAdapterOptionInfo vaoi : ValueAdapterInfo.fromObject(valueAdapter).getOptions()) {
            // prepare type name with options
            final String optionValue = null == optionValuesByName ? null : optionValuesByName.get(vaoi.getName());
            if (StringUtils.isNotEmpty(optionValue)) {
                final StringBuilder typeWithOption = new StringBuilder().append(vaoi.getName()).append(COLON);
                if (ClassUtils.isAssignable(vaoi.getType(), Number.class)) {
                    typeWithOption.append(optionValue);
                } else if (ClassUtils.isAssignable(vaoi.getType(), Boolean.class)) {
                    typeWithOption.append(BooleanUtils.toBoolean(optionValue));
                } else {
                    typeWithOption.append(QUOTE).append(optionValue).append(QUOTE);
                }
                typeWithOptions.add(typeWithOption.toString());
            }
            // prepare list of option fields to display
            descriptions.add( //
                    new OptionDescriptionBean( //
                            vaoi.getName(), //
                            "options." + vaoi.getName(), //
                            NashMessageReader.getReader(valueAdapter).getFormatter().format(vaoi.getDescription()), //
                            false, //
                            vaoi.getType().getSimpleName(), //
                            optionValue //
                    ) //
            );
        }

        if (!typeWithOptions.isEmpty()) {
            dataElement.setType(new StringBuilder().append(type).append(LPAR).append(StringUtils.join(typeWithOptions, COMMA)).append(RPAR).toString());
        }

        return descriptions;
    }

    /**
     * Getter on attribute {@link #base properties}.
     *
     * @param dataElement
     *            data element
     * @return base properties
     */
    private List<OptionDescriptionBean> getBaseProperties(final DataElement dataElement) {
        final List<OptionDescriptionBean> descriptions = new ArrayList<>();

        descriptions
                .add(new OptionDescriptionBean("id", null, NashMessageReader.getReader().getFormatter().format("Identifier of the field"), true, String.class.getSimpleName(), dataElement.getId()));
        descriptions.add(new OptionDescriptionBean("label", null, NashMessageReader.getReader().getFormatter().format("Label associated to the field"), false, String.class.getSimpleName(),
                dataElement.getLabel()));
        descriptions.add(new OptionDescriptionBean("mandatory", null, NashMessageReader.getReader().getFormatter().format("Is this field mandatory ?"), false, Boolean.class.getSimpleName(),
                BooleanUtils.toStringTrueFalse(dataElement.getMandatory())));

        // display condition option
        if (StringUtils.isEmpty(dataElement.getDisplayCondition())) {
            dataElement.setDisplayCondition(null);
        }
        descriptions.add(new OptionDescriptionBean("if", "displayCondition", NashMessageReader.getReader().getFormatter().format("Display condition of the field"), false, String.class.getSimpleName(),
                dataElement.getDisplayCondition()));

        return descriptions;
    }

    /**
     * Getter on attribute {@link #notices properties}.
     *
     * @param dataElement
     *            data element
     * @return notices properties
     */
    private List<OptionDescriptionBean> getNoticesProperties(final DataElement dataElement) {
        final List<OptionDescriptionBean> descriptions = new ArrayList<>();

        // description option
        if (StringUtils.isEmpty(dataElement.getDescription())) {
            dataElement.setDescription(null);
        }
        descriptions.add(new OptionDescriptionBean("description", null, NashMessageReader.getReader().getFormatter().format("Description of the field"), false, "Text", dataElement.getDescription()));

        // help option
        if (StringUtils.isEmpty(dataElement.getHelp())) {
            dataElement.setHelp(null);
        }
        descriptions.add(new OptionDescriptionBean("help", null, NashMessageReader.getReader().getFormatter().format("Help of the field"), false, "Text", dataElement.getHelp()));

        // warning option
        if (dataElement.getWarnings() != null && (dataElement.getWarnings().size() == 0 || StringUtils.isEmpty(dataElement.getWarnings().get(0).getDescription()))) {
            dataElement.setWarnings(null);
        }
        descriptions.add(new OptionDescriptionBean("warning", "warnings[0].description", NashMessageReader.getReader().getFormatter().format("Warning of the field"), false, "Text",
                CollectionUtils.isNotEmpty(dataElement.getWarnings()) ? dataElement.getWarnings().get(0).getDescription() : null));

        return descriptions;
    }

}
