# Changelog
Ce fichier contient les modifications techniques du module.


## [2.15.2.1] - 2020-08-05

### Ajout

- Mise en place de la fonctionnalité "yield"

###  Ajout

  __Properties__ :

Concerne uniquement les applications UI :

| fichier                |      nom      |  description | valeur |
|------------------------|:-------------:|-------------:|-------:|
| application.properties | max.upload.file.size | Taille maximum des fichiers pouvant etre emis (en octet). Veillez à la cohérence de la valeur avec le paramétrage du serveur applicatif et/ou frontal. | 50000000 |


Concerne uniquement les applications WS :

| fichier                |      nom      |  description | valeur |
|------------------------|:-------------:|-------------:|-------:|
| application.properties | ws.storage.private.url | URL vers l'API privee **Storage** | https://{VH_STORAGE_WS_SERVER}/api |


## [2.14.3.1] - 2020-04-17

###  Ajout

- Handlebars dans les formalités autonomes
- Exécuter les transitions en cascade depuis nash-web
- Désactivation du canal dans les transitions
- API de chargement d'un dossier depuis une URL externe
- API d'upload d'un dossier depuis une URL externe

###  Ajout

  __Properties__ :

Concerne uniquement les applications : nashbo-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | application.public.url | URL publique de l'application de type nash-ui | Laissez à vide |
| engine.properties | storage.gp.url.ws.private | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| engine.properties | providers.alias.storage | Provider pour utiliser l'API Storage | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| engine.properties | providers.alias.markov | Provider pour utiliser l'API Markov | http://markov-ws.${env}.guichet-partenaires.loc:12030/api |
| engine.properties | providers.alias.nashBo | Provider pour utiliser l'API Nash pour les dossiers créées dans le Backoffice | http://backoffice-forms-ws.${env}.guichet-partenaires.loc:12030/api |

## [2.13.4.3] - 2020-02-05

- Lien TRACKER entre le fichier envoyé à EDDIE et le dossier (transition "sendEddie")
- Correction des noms de fichiers dans le ZIP DMTDU (transition "prepareZip")


## [2.13.2.0] - 2019-11-19

Création d'une V2 pour la transition SendToAuthority et findFuncIdReceiver


## [2.12.5.1] - 2019-11-19

Ajout des métadonnées lors d'un dépôt dans STORAGE

###  Ajout

  __Properties__ :

Concerne uniquement les applications : nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | storage.gp.url.ui.public | URL publique de STORAGE | https://storage.${env}.guichet-partenaires.fr |
| engine.properties | storage.gp.url.ws.private | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |

Concerne toutes les bulles : FORGE, NASH WEB, NASH-SUPPORT, NASH-BACKOFFICE et NASH-DIRECTORY

| Module           | fichier                |        nom        |        description |        valeur |
|------------------|:------------------------|:-----------------:|-------------------:|--------------:|
| module-ui        | application.properties | path.externalLibs | Chemin d'accés au répertoire contenant les librairies javascript externalisées | (à définir par l'intégration) |
| module-ws        | application.properties | path.externalLibs | Chemin d'accés au répertoire contenant les librairies javascript externalisées | (à définir par l'intégration) |

Remarque: chaque bulle doit avoir un répertoire dédié à elle. Le répertoire de la même bulle est partagé entre le module WS et le module UI 



## [2.12.5.0] - 2019-11-12

Accéder à la première étape d'un dossier dans le Backoffice


## [2.12.4.3] - 2019-11-07

Accéder aux dossiers dans NASH-SUPPORT à partir des rôles dans DIRECTORY

###  Ajout

  __Properties__ :

Concerne uniquement les applications : nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | right.record.edition.check.enabled | Utilisation des rôles de DIRECTORY | true |


## [2.12.4.1] - 2019-11-04

###  Ajout

  __Properties__ :

Concerne uniquement les applications : nashweb-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | payment.proxy.service.create.cart | URL privée de création de panier pour le Proxy Payment | https://payment.{env}.guichet-entreprises.loc/private/api/v1/cart/{token}/create |

## [2.11.10.3] - 2019-09-20

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | payment.proxy.url.public | URL publique d'accès au Proxy Payment | https://payment.{pft}.guichet-entreprises.fr/public |
| engine.properties | payment.proxy.create.session.url | URL de création de session pour le proxy Payment | http://payment.{pft}.guichet-entreprises.loc/private/api/v1/proxy/create |
| engine.properties | payment.proxy.validate.session.url | URL de validation de session pour le proxy Payment | http://payment.{pft}.guichet-entreprises.loc/private/api/v1/proxy/verify/{token} |
| engine.properties | payment.proxy.ui.callback.url | URL Calback du proxy Payment de redirection vers FORMS2 | https://forms-directory.{pft}.guichet-partenaires.fr/record/{code}/{step}/process/{processPhase}/{processId} |
| engine.properties | payment.proxy.service.callback.url | URL du Proxy Payment de génération du fichier résultant du PROXY dans FORMS 2 | http://forms-directory-ws.{pft}.guichet-partenaires.loc/api/v1/Record/code/{code}/step/{step}/phase/{processPhase}/process/{processId}/{resultFile} |
| engine.properties | forms.baseUrl	 | URL de base API module Forms	 | http://{VH_WS_FORMS}/api|
| engine.properties | nash.ws.private.url | URL privée de destination de création de dossiers NASH | http://nashweb-ws.{pft}.guichet-qualifications.loc/api |
| engine.properties | forms.profiler.private.url | URL privée de destination de création de dossiers Profiler | http://profiler-ws.{pft}.guichet-partenaires.loc:12030/api |
| engine.properties | forms.private.url | URL privée de destination de création de dossiers nashweb | http://nashweb-ws.{pft}.guichet-qualifications.loc/api |
| engine.properties | forms.directory.private.url | URL privée de destination de création de dossiers directory | http://forms-directory-ws.{pft}.guichet-partenaires.loc:12030/api |
| engine.properties | forms.backoffice.private.url | URL privée de destination de création de dossiers backoffice | http://backoffice-forms-ws.{pft}.guichet-partenaires.loc:12030/api |
| engine.properties | forms.support.private.url |  URL privée de destination de création de dossiers Support | http://nashsupport-ws.{pft}.guichet-qualifications.loc/api |
| application.properties | ws.clicker.url | URL Clicker | http://clicker-ws.{pft}.guichet-partenaires.loc/api |


## [2.11.10.2] - 2019-09-06

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

  __Properties__ :

Concerne uniquement les applications : nashprofiler-ui et nashweb-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | welcome.public.url | URL publique de Welcome Guichet-Entreprises | https://welcome.${env}.guichet-entreprises.fr |

Concerne uniquement les applications : nashbackoffice-ui, nashdirectory-ui et nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | welcome.public.url | URL publique de Welcome Guichet-Partenaires | https://welcome.${env}.guichet-partenaires.fr |
| engine.properties | welcome.gp.public.url | URL publique de Welcome Guichet-Partenaires | https://welcome.${env}.guichet-partenaires.fr |

Concerne uniquement les applications : nashforge-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | false |

Concerne uniquement les applications : nashsupport-ws

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | engine.user.type | Type d'utilisateur pour accéder aux étapes d'un dossier | support |
| application.properties | nash.support.default.role | Rôle par défaut lors de l'import d'un dossier dans Nash-support | support |
| application.properties | nash.record.additional.actions | Liste des identifiants des actions additionnelles sur un dossier | export,delete |


Concerne uniquement les applications : nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | nashBo.baseUrl | URL d'accès à Nash BackOffice | http://backoffice-forms-ws.{env}.guichet-partenaires.loc:12030/api|
| engine.properties | tracker.baseUrl | URL d'accès à Tracker ws | http://tracker.{env}.guichet-entreprises.loc/api |
| engine.properties | payment.proxy.api.private.baseUrl | URL privée d'accès à payment | http://payment.{env}.guichet-entreprises.loc/private |
| engine.properties | ws.markov.url | URL d'accès à Markov | http://markov-ws.{env}.guichet-partenaires.loc:12030/api |


###  Modification

Concerne uniquement l'application : nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | ui.dashboard.url | URL publique du Dashboard Partenaires | https://backoffice-dashboard.${env}.guichet-partenaires.fr |
| application.properties | ui.dashboard.check.url | URL publique de la page de status du Dashboard Partenaires | https://backoffice-dashboard.${env}.guichet-partenaires.fr/status |


###  Suppression

## [2.11.9.0] - 2019-07-24

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

 - Cycle de fin de vie des dossiers
 - Génération de la page des PJ à fournir après modification
 
  __Properties__ :

Concerne uniquement les applications : nashprofiler-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | (laissez à vide) |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | false |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | false |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


Concerne uniquement les applications : nashweb-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | /var/data/ge/gent/nashstorage |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | true |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | false |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


Concerne uniquement les applications : nashbackoffice-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | (laissez à vide) |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | false |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | true |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


Concerne uniquement les applications : nashdirectory-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | /var/data/ge/gent/nashstorage |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | false |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | false |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


Concerne uniquement les applications : nashsupport-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | /var/data/ge/gent/nashstorage |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | false |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | false |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


Concerne uniquement les applications : nashforge-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Répertoire de dépôt des dossiers pour Markov | (laissez à vide) |
| application.properties | record.memory.keep | Sauvegarde des dossiers finalisés en BDD | true |
| application.properties | record.archive.partners | Archivage des dossiers dans la pile 'partners' de STORAGE | false |
| application.properties | ws.storage.private.url | URL privée de STORAGE | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | cache.resources.static | Temps de chargement des ressources statiques (en secondes) | 1800 |


## [2.11.6.0] - 2019-07-64

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

 - Suppression de dossier depuis la forge
 - Ne plus afficher le fil d'Ariane si une formalité contient une seule et unique étape
 
  __Properties__ :

Concerne uniquement les applications : nashforge-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | nash.allow.remove.record | Autorise les utilisateurs à supprimer les dossiers | true |

## [2.11.5.0] - 2019-06-21

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

 - API de duplication d'une étape
 - API de chargement d'un dossier depuis une URL externe
 - API d'upload d'un dossier depuis une URL externe
 - "Enregistrer et quitter" devient "Quitter" lorsqu'un dossier bascule en lecture seule
 
  __Properties__ :

Concerne uniquement les applications : nashdirectory-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | providers.alias.storage | Provider pour utiliser l'API Storage | http://storage-ws.${env}.guichet-partenaires.loc:12030/api |
| engine.properties | providers.alias.markov | Provider pour utiliser l'API Markov | http://markov-ws.${env}.guichet-partenaires.loc:12030/api |
| engine.properties | providers.alias.nashBo | Provider pour utiliser l'API Nash pour les dossiers créées dans le Backoffice | http://backoffice-forms-ws.${env}.guichet-partenaires.loc:12030/api |

## [2.11.4.0] - 2019-06-07

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

 - Envoyer un courriel au support lorsqu'un paiement est en échec
 
  __Properties__ :

Concerne uniquement les applications : nashweb-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | contact.support.email | Courriel du support Guichet-Partenaires | contact@guichet-partenaires.fr |

## [2.11.3.0] - 2019-05-23

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

 - Suppression des références ID fantômes de la forge
 - Mise en place d'un WS pour débloquer un dossier au statut erreur (erreur métier)
 
  __Properties__ :

Concerne uniquement les applications : nashws-web

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | storage.record.root | Chemin absolu du répertoire de stockage des dossiers validés | /var/data/ge/gent/nashstorage |

Concerne uniquement les applications : nashdirectory-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | nashBo.baseUrl | URL de Nash Backoffice (consultation de dossiers) | ${VH_backoffice-forms-ws}/api |

## [2.11.2.1] - 2019-05-06

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout
- __Properties__ :

Concerne uniquement les applications : nashprofiler-ws, nashws-web, nashws-forge, nashws-backoffice, nashws-support, nashws-directory

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | mas.tracker.service.url | URL privée de WS TRACKER | http://\{VH_TRACKERWS_SERVER}/api |
| application.properties | mas.tracker.service.author | Auteur utilsé pour poster les messages TRACKER | nash |

Concerne uniquement l'application nash-web

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | payment.proxy.service.lock.record | URL privée pour bloquer un dossier | http://\{VH_NASHWEB_SERVER}/api/v1/Record/code/{code}/lock |


## [2.11.1.4] - 2019-04-26

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout
- Transition confirmPostMail-1.0

### Modification
- Transition sendToAuthority-1.0
- Transition findFuncIdReceiver-1.1
- Transition findEmailAddress-1.0
- Transition prepareEmail-1.0
- Transition findPostMailAddress-1.0
- Transition preparePostMail-1.0
- Transition mergePdf-1.0
- Transition sendEmail-1.1

### Suppression

---
---

## [2.11.1.3] - 2019-04-17

- Projet : [Nash](https://tools.projet-ge.fr/gitlab/minecraft/nash)

###  Ajout

- Nouveau fichier _Changelog.md_
- __Transition__ findNotificationList-1.0
- __Transition__ sendNotification-1.0
- __Properties__ :

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties |  ws.clicker.url | url web-services Clicker | http://\{VH_CLICKER_SERVER}/api |
| application.properties |  ws.clicker.url | url web-services Clicker | http://\{VH_CLICKER_SERVER}/api |


### Modification

### Suppression
- Properties :


| fichier   |      nom      |  description |
|-----------|:-------------:|-------------:|
| engine.properties | account.ge.baseUrl.read | base URL de account |

---

## Liens

[2.11.1.3](https://tools.projet-ge.fr/gitlab/minecraft/nash-webclients/tags/nash-webclients-2.11.1.3)

