/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/template', 'lib/loader', 'lib/error', 'lib/triggers', 'lib/Model', 'lib/parsley', 'lib/repeatables' ], function($, Template, Loader, error, triggers, Model) {

    $(function() {
        var $form = $('form[name="record"]');

        Model.page.initialize();

        var readOnly = $form.data("readonly") || false;
        if (readOnly) {
            $('input, select, textarea', $form).attr('disabled', true);
        }

        $('.list-steps [data-toggle="tooltip"]').tooltip();

        /*
         * Form initialization
         */
        $form.on('click', '.submit-buttons button', function(evt) {
            var firstFieldInError = $('.type-errors-list').closest('[data-field]').not('.hide').first();
            if (firstFieldInError.length > 0) {
                // prevent submitting the form if type specific errors exist
                var firstInputInError = firstFieldInError.filter('input, select, textarea').filter(':visible').first();
                if (firstInputInError.length > 0) {
                    firstInputInError[0].focus();
                }
                evt.preventDefault();
            } else {
                var btn = $(this), frm = $(this.form), url = btn.data('url');

                //-->Cannot submit the form when there is required data in fold groups
                var submitForm = true;
                //-->MINE-397 : URLBase64 widget sends a Base64 encoded value to HTTP Request before submitting the form
                frm.find("[data-type='URLBase64']").filter(':visible').each(function() {
                    var urlField = $(this);
                    urlField.find('input[type=text]').attr('disabled', 'disabled');
                });
                //<--
                frm.find('.panel-primary').filter("[data-fold='yes']").filter(':visible').each(function() {
                    var panelPrimary = $(this);
                    panelPrimary.find('[data-parsley-required="true"]').closest('[data-field]').not('.hide').each(function() {
                        var dataField = $(this);
                        if (dataField.closest('.panel-primary').find('.panel-body-container:first.collapse').not('.collapse.in').length > 0) {
                            dataField.closest('.panel-primary').find('.panel-body-container:first.collapse').not('.collapse.in').collapse('toggle');
                            submitForm = false;
                        }
                    });
                });
                if (!submitForm) {
                	return false;
                }
                //<--
            
                if (Loader) {
                    Loader.show();
                }

                if (url) {
                    frm.attr('action', url);
                }

                if ('draft' == btn.attr('role')) {
                    frm.parsley().destroy();
                }

                // button of type 'submit' : automatic submit
                // button of type 'button' : manual submit
                if (btn.attr('type') == 'button') {
                    frm.submit();
                }
            }
        });

        $form.on('click', '.panel-collapse-action', function() {
            var icon = $(this);
            icon.closest('.panel-primary').find('.panel-body-container:first').collapse('toggle');
            icon.find('i').toggleClass('rotated');
        });

        $form.parsley({
            errorsContainer : function(parsleyField) {
                var fld = parsleyField.$element.closest('.form-group'), container = $('.parsley-container', fld);

                if (!container || 0 == container.length) {
                    container = $('<div class="parsley-container col-md-12"></div>').appendTo(fld);
                }

                return container;
            }
        }).on('form:error', function(evt) {
            Loader && Loader.hide();
        }).on('field:error', function(fieldInstance) {
            if (fieldInstance.$element.is(":hidden")) {
                // hide the message wrapper
                fieldInstance._ui.$errorsWrapper.css('display', 'none');
                // set validation result to true
                fieldInstance.validationResult = true;
                return true;
            }
        });
        $form.attr("onsubmit", null).prop("onsubmit", null);

        /*
         * Popover buttons
         */
        $('[data-toggle="popover"]', $form).popover();

        $form.on('change', 'input, select, textarea', function(evt) {
            var fld = $(this);
            var block = fld.closest('[data-field][data-type]');
            var name = block.attr('data-field');

            Model.model.set(block);

            triggers.cascade(name);
        });
    });

});
