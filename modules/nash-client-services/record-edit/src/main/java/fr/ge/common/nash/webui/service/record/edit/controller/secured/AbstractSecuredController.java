/**
 *
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;

/**
 * Base secured controller, providing usefull methods and information about
 * user.
 *
 * @author Ademola Olubi
 */
@Component
public class AbstractSecuredController {

    @Value("${engine.user.type:user}")
    private String engineUserCode;

    /**
     * Checks if it is last user step.
     *
     * @param loader
     *            record loader
     * @param step
     *            step to check
     * @return true, if is last user step
     */
    protected boolean isLastUserStep(final SpecificationLoader loader, final StepElement step) {
        final StepElement nextStep = loader.stepsMgr().find(step.getPosition() + 1);
        return (StepStatusEnum.DONE.getStatus().equals(step.getStatus()) || StepStatusEnum.SEALED.getStatus().equals(step.getStatus())) //
                && (null == nextStep || !this.engineUserCode.equals(nextStep.getUser()));
    }

    /**
     * Redirect user to the validation if all steps are done.
     * 
     * @param loader
     *            record loader
     * @return
     */
    protected boolean allStepsDone(final SpecificationLoader loader) {
        final StepElement currentStep = loader.stepsMgr().current();
        final int currentPosition = currentStep.getPosition();
        final List<StepElement> steps = loader.stepsMgr().findAll();
        final int maxStepIndex = steps.size() - 1;
        final String validatedStep = currentStep.getStatus();
        return currentPosition == maxStepIndex && StepStatusEnum.DONE.getStatus().equals(validatedStep) || !this.engineUserCode.equals(currentStep.getUser());

    }

}
