/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery' ], function($) {

    var IDENTIFIED_PATH = /^(?:_(record|step|page|group)\.)?(.*)$/ ;

    /**
     * @param src path to translate
     * @return path as rule key
     */
    function buildRuleKey(src) {
        return src //
            .replace(/^_(record|step|page|group)\./, '') // remove scope prefix
            .replace(/\[[^\]]*\]/g, '-0-') // normalize repeatable elements
            .replace(/\./g, '-') // convert dot to caret
            .replace(/-{2,}/g, '-') // reduce consecutive caret
            .replace(/-$/, ''); // remove trailing caret
    }

    /**
     * Create a wrapper representing a specify target.
     * 
     * @param obj jQuery objet or relative path
     * @return normalize widget target
     */
    function normalize(obj) {
        var fragment, absolutePath, relativePath, type, scope;

        function fromPath(src) {
            var m = IDENTIFIED_PATH.exec(src);
            scope = m[1] ? m[1] : 'page';
            relativePath = m[2];
            absolutePath = '_' + scope + '.' + m[2];
        }

        if (typeof obj === 'string') {
            fromPath(obj);
        } else {
            fragment = obj;
            var name = fragment.attr('data-group');
            if (undefined === name) {
                type = 'field';
                name = fragment.attr('data-field');
            } else {
                type = 'group';
            }
            fromPath(name);
        }


        function retrieveFragmentAndType() {
            if (this.path.relative.indexOf('[*]') > 0) {
                /*
                 * Path relative to a collection
                 */
                var prefix = this.path.relative.replace(/[.]?([a-z0-9_]+)(\[\*\]).*/i, '');
                this._fragment = $('div[data-rule="' + this.key() + '"]');
                if (this._fragment.attr('data-field')) {
                    this._type = 'field';
                    this._fragment = this._fragment.filter('[data-field^="' + prefix + '"]');
                } else {
                    this._type = 'group';
                    this._fragment = this._fragment.filter('[data-group^="' + prefix + '"]');
                }
            } else {
                /*
                 * Path relative to one element
                 */
                this._fragment = $('div[data-field="' + this.path.relative + '"]');
                if (this._fragment.length > 0) {
                    this._type = 'field';
                } else {
                    this._type = 'group';
                    this._fragment = $('div[data-group="' + this.path.relative + '"]');
                }
            }
        }

        return {
            '_fragment': fragment,
            '_type': type,
            'scope': scope,
            'path': {
                'absolute': absolutePath,
                'relative': relativePath
            },
            'key': function () {
                if (!this._key) {
                    this._key = buildRuleKey(this.path.relative);
                }
                return this._key;
            },
            'type': function () {
                if (!this._type) {
                    retrieveFragmentAndType.call(this);
                }
                return this._type;
            },
            'fragment': function () {
                if (!this._fragment) {
                    retrieveFragmentAndType.call(this);
                }
                return this._fragment;
            }
        };
    }

    return {
        buildRuleKey: buildRuleKey,
        normalize: normalize
    };

});
