/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.webui.core.controller.AbstractRecordController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordReadOnlySecuredController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordRoutingSecuredController;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordSecuredController;
import fr.ge.common.utils.CoreUtil;

/**
 * Record root controller.
 *
 * @author Christian Cougourdan
 */
@Controller("baseRecordController")
@RequestMapping("/record/{code:[0-9]{4}-[0-9]{2}-[A-Z]{3}-[A-Z]{3}-[0-9]{2}}")
public class RecordController extends AbstractRecordController {

    @Autowired
    private RecordSecuredController secured;

    @Autowired
    private RecordRoutingSecuredController stepSecured;

    @Value("${nash.readOnly:false}")
    private boolean readOnly;

    @Autowired
    private RecordReadOnlySecuredController securedReadOnly;

    /**
     * Bootstrap for record display, redirects to active record step.
     *
     * @param model
     *            context model
     * @param code
     *            record identifier
     * @return view redirection
     */
    @RequestMapping(method = RequestMethod.GET)
    public String run(final Model model, @PathVariable("code") final String code, final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
        final SpecificationLoader loader = this.loader(code);
        return (this.readOnly || loader.hasErrorState()) ? this.securedReadOnly.redirectToFirstStep(model, loader, request, response, locale) : //
                this.stepSecured.redirectToCurrentStep(model, loader, request, response, locale);
    }

    /**
     * Download a record resource.
     *
     * @param code
     *            record identifier
     * @param request
     *            HTTP request used to retrieved resource path
     * @return expected resource
     */
    @RequestMapping(path = "view/**", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> view(@PathVariable("code") final String code, final HttpServletRequest request) {
        final String requestPath = CoreUtil.coalesce(request.getPathInfo(), request.getServletPath());
        final String resourcePath = requestPath.replaceFirst("/record/[^/]+/view/", "");

        return this.secured.downloadRecordElement(this.loader(code), resourcePath);
    }

    /**
     * Download a record resource.
     *
     * @param code
     *            record identifier
     * @param request
     *            HTTP request used to retrieved resource path
     * @return expected resource
     */
    @RequestMapping(path = "download/**", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(@PathVariable("code") final String code, final HttpServletRequest request) {
        final String requestPath = CoreUtil.coalesce(request.getPathInfo(), request.getServletPath());
        final String resourcePath = requestPath.replaceFirst("/record/[^/]+/download/", "");

        return this.secured.downloadRecordElement(this.loader(code), resourcePath);
    }

    /**
     * Retrieve record resources list as JSON.
     *
     * @param code
     *            record identifier
     * @param filter
     *            resource filter
     * @return resources list
     */
    @RequestMapping(path = "/files", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> files(@PathVariable("code") final String code, @RequestParam(value = "filter", required = false) final String filter) {
        return this.secured.displayRecordElementsList(this.loader(code), filter);
    }

    /**
     * Download record as ZIP resource file.
     *
     * @param code
     *            record identifier
     * @return record binary ZIP.
     */
    @RequestMapping(path = "/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable("code") final String code) {
        return this.secured.downloadRecord(this.loader(code));
    }

    /**
     * Download indexed record documents, ie resources registered as "document" in
     * meta data of specified record. If no resource are indexed, check each steps
     * from last one and use first found documents.
     *
     * @param code
     *            record identifier
     * @return record indexed documents
     */
    @RequestMapping(path = "/documents", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadDocuments(@PathVariable("code") final String code) {
        return this.secured.downloadRecordDocuments(this.loader(code));
    }

    /**
     * Display a record step page using the step identifier.
     *
     * @param model
     *            context model
     * @param code
     *            record identifier
     * @param stepId
     *            the step identifier
     * @return the string
     */
    @RequestMapping(value = "/step/id/{stepId}", method = RequestMethod.GET)
    public String displayPageWithStepId(final Model model, @PathVariable("code") final String code, @PathVariable("stepId") final String stepId) {
        return this.stepSecured.displayStepByCode(model, this.loader(code), stepId);
    }

    /**
     * Remove a record.
     *
     * @param code
     *            code identifier
     * @return empty response
     */
    @RequestMapping(path = "/remove", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> remove(@PathVariable("code") final String code) {

        return this.secured.removeRecord(code);

    }

}
