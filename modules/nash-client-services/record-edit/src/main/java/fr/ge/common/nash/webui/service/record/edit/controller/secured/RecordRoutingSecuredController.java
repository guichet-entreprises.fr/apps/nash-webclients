/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.RecordStepNotFoundException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.StorageEvent;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessBean;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.locale.CookieThenAcceptHeaderLocaleResolver;
import fr.ge.common.nash.webui.service.record.edit.WebUiConstants;

/**
 * Record controller managing display, save and validate. Allow to check for
 * external user permissions.
 *
 * @author Christian Cougourdan
 */
@Component
public class RecordRoutingSecuredController extends AbstractSecuredController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordRoutingSecuredController.class);

    /** The engine user type. */
    @Value("${engine.user.type}")
    private String engineUserType;

    /** The custom local resolver. **/
    @Autowired
    private CookieThenAcceptHeaderLocaleResolver localeResolver;

    /**
     * Redirect to current step.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param request
     *            the request
     * @param response
     *            the response
     * @param locale
     *            the locale
     * @return the string
     */
    public String redirectToCurrentStep(final Model model, final SpecificationLoader loader, final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
        final FormSpecificationDescription description = loader.description();

        if (null == description) {
            throw new RecordNotFoundException();
        } else {
            if (this.allStepsDone(loader)) {
                return "record/validation/main";
            }
            this.localeResolver.resolve(loader, request, response, locale);
            model.addAttribute("code", description.getRecordUid());
            model.addAttribute("step", loader.currentStepPosition());
            return "redirect:/record/{code}/{step}/page/0";
        }
    }

    /**
     * Display page from index.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @param page
     *            the page
     * @return the string
     */
    public String displayPageFromIndex(final Model model, final SpecificationLoader loader, final int stepIndex, final int page) {
        if (this.allStepsDone(loader)) {
            return "record/validation/main";
        }

        final StepElement currentStep = loader.stepsMgr().current();
        final int currentStepPosition = currentStep.getPosition();
        final List<StepElement> steps = loader.stepsMgr().findAll();
        final int maxStepIndex = steps.size() - 1;
        model.addAttribute(WebUiConstants.ATTR_PAGE, page);

        /*
         *
         * If specified step index is greater than current one, redirect to it
         *
         */
        if (stepIndex > currentStepPosition) {
            model.addAttribute(WebUiConstants.ATTR_STEP, currentStepPosition);
            model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
            return "redirect:/record/{code}/{step}/page/{page}";
        }

        /*
         *
         * If specified step index is the last one and its status is "done",
         *
         * display validation page
         *
         */
        final StepElement stepElement = loader.stepsMgr().find(stepIndex);

        final String validatedStep = stepElement.getStatus();
        if (stepIndex == maxStepIndex && StepStatusEnum.DONE.getStatus().equals(validatedStep) || !this.engineUserType.equals(currentStep.getUser())) {
            return "record/validation/main";
        }

        final int realPage = loader.group(stepIndex, page);
        if (realPage != page) {
            model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);
            model.addAttribute(WebUiConstants.ATTR_PAGE, realPage);
            return "redirect:/record/{code}/{step}/page/{page}";
        }

        if (StepStatusEnum.DONE.getStatus().equals(validatedStep)) {
            model.addAttribute(StepStatusEnum.DONE.getStatus(), true);
        } else {
            model.addAttribute(StepStatusEnum.DONE.getStatus(), false);
        }

        model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
        model.addAttribute(WebUiConstants.ATTR_STEP_INDEX, stepIndex);

        if (0 == page) {
            final IProcessResult<?> result = loader.processes().preExecute(stepElement);
            return this.manageResult(result, () -> {
                model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
                model.addAttribute(WebUiConstants.ATTR_STEP_INDEX, stepIndex);
                model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
                return "record/edit/main";
            });

        } else {
            return "record/edit/main";
        }
    }

    /**
     * Save.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @param page
     *            the page
     * @param success
     *            the success
     * @param request
     *            the request
     * @return the string
     * @throws TemporaryException
     *             the temporary exception
     */
    public String save(final Model model, final SpecificationLoader loader, final int stepIndex, final Integer page, final String success, final HttpServletRequest request) throws TemporaryException {
        final String userId = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);

        loader.updateStepStatus(stepIndex, StepStatusEnum.IN_PROGRESS, userId);
        final FormSpecificationData spec = loader.updateFromRequest(stepIndex, request);

        String view;
        if (spec.getGroups() != null && page + 1 < spec.getGroups().size() && "next".equals(success)) {
            /*
             * Move to next page
             */
            model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
            model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);
            model.addAttribute(WebUiConstants.ATTR_PAGE, page + 1);
            loader.updateStepStatus(stepIndex, StepStatusEnum.IN_PROGRESS, userId).flush();
            view = "redirect:/record/{code}/{step}/page/{page}";
        } else if ("validate".equals(success)) {
            /*
             * Move to next step
             */
            view = this.validate(model, loader, stepIndex);
        } else {
            /*
             * Save and return to dashboard
             */
            loader.updateStepStatus(stepIndex, StepStatusEnum.IN_PROGRESS, userId).flush();
            view = "forward:/dashboard";
        }

        return view;
    }

    /**
     * Validate.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @return the string
     * @throws TemporaryException
     *             the temporary exception
     */
    public String validate(final Model model, final SpecificationLoader loader, final int stepIndex) throws TemporaryException {
        final StepElement step = loader.stepsMgr().find(stepIndex);
        final IProcessResult<?> result = loader.processes().postExecute(step);

        String view = this.manageResult(result);
        if (null == view) {
            view = this.afterPostExecute(model, loader, step);
        }

        return view;
    }

    /**
     * Display step by index.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @return the string
     */
    public String displayStepByIndex(final Model model, final SpecificationLoader loader, final int stepIndex) {
        if (this.allStepsDone(loader)) {
            return "record/validation/main";
        }

        final String code = loader.description().getRecordUid();

        model.addAttribute(WebUiConstants.ATTR_CODE, code);
        model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);

        StepElement step = null;
        try {
            step = loader.stepsMgr().find(stepIndex);
            if (null != step) {
                return "redirect:/record/{code}/{step}/page/0";
            }
        } catch (final TechnicalException ex) {
            LOGGER.warn("Unable to load step '{}' for record '{}'", stepIndex, code, ex);
        }

        throw new RecordStepNotFoundException(stepIndex);
    }

    /**
     * Display step by code.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepId
     *            the step id
     * @return the string
     */
    public String displayStepByCode(final Model model, final SpecificationLoader loader, final String stepId) {
        if (this.allStepsDone(loader)) {
            return "record/validation/main";
        }
        final String code = loader.description().getRecordUid();

        model.addAttribute(WebUiConstants.ATTR_CODE, code);
        try {
            final StepElement step = loader.stepsMgr().find(stepId);
            if (null != step) {
                model.addAttribute(WebUiConstants.ATTR_STEP, step.getPosition());
                return "redirect:/record/{code}/{step}/page/0";
            }
        } catch (final TechnicalException ex) {
            LOGGER.warn("Unable to load step '{}' for record '{}'", stepId, code, ex);
        }

        throw new RecordStepNotFoundException();
    }

    /**
     * Display page from process id.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepId
     *            the step id
     * @param processPhase
     *            the process phase
     * @param processId
     *            the process id
     * @return the string
     * @throws TemporaryException
     *             the temporary exception
     */
    public String displayPageFromProcessId(final Model model, final SpecificationLoader loader, final String stepId, final String processPhase, final String processId) throws TemporaryException {
        final StepElement stepElement = loader.stepsMgr().find(stepId);

        /*
         * Step not found, redirect to current one
         */
        if (null == stepElement) {
            final int currentStepIndex = Optional.ofNullable(loader.stepsMgr().current()).map(StepElement::getPosition).orElse(-1);
            if (currentStepIndex < 0) {
                return "redirect:/record";
            } else {
                model.addAttribute(WebUiConstants.ATTR_STEP, currentStepIndex);
                model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
                return "redirect:/record/{code}/{step}/page/{page}";
            }
        }

        final int stepIndex = stepElement.getPosition();

        if ("pre".equals(processPhase)) {
            final IProcessResult<?> result = loader.processes().preExecuteAfter(stepElement, processId);
            return this.manageResult(result, () -> {
                model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
                model.addAttribute(WebUiConstants.ATTR_STEP_INDEX, stepIndex);
                model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
                return "record/edit/main";
            });
        } else {
            final IProcessResult<?> result = loader.processes().postExecuteAfter(stepElement, processId);

            String view = this.manageResult(result);
            if (null == view) {
                view = this.afterPostExecute(model, loader, stepElement);
                loader.flush();
            }

            return view;
        }
    }

    /**
     * After post execute.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param step
     *            the step
     * @return the string
     * @throws TemporaryException
     *             the temporary exception
     */
    private String afterPostExecute(final Model model, final SpecificationLoader loader, final StepElement step) throws TemporaryException, TechnicalException {
        final String userId = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);
        if (!StepStatusEnum.SEALED.getStatus().equals(step.getStatus())) {
            loader.updateStatus(step, StepStatusEnum.DONE, userId);
        }
        loader.flush();

        final StepElement activeStep = loader.stepsMgr().getActiveStep(this.engineUserType);
        if (null == activeStep) {
            loader.getEventBus().post(new StorageEvent(loader));
            return "forward:/dashboard";
        } else {
            model.addAttribute("code", loader.description().getRecordUid());
            model.addAttribute(WebUiConstants.ATTR_STEP, activeStep.getPosition());
            return "redirect:/record/{code}/{step}";
        }
    }

    /**
     * Manage result.
     *
     * @param result
     *            the result
     * @return the string
     */
    private String manageResult(final IProcessResult<?> result) {
        return this.manageResult(result, () -> null);
    }

    /**
     * Manage result.
     *
     * @param result
     *            the result
     * @param other
     *            the other
     * @return the string
     */
    private String manageResult(final IProcessResult<?> result, final Supplier<String> other) {
        if (null != result && result.isInterrupting()) {
            if (result.getContent() instanceof RedirectProcessBean) {
                final RedirectProcessBean bean = (RedirectProcessBean) result.getContent();
                return "redirect:" + bean.getUrl();
            } else {
                return "forward:/dashboard";
            }
        } else {
            return other.get();
        }
    }

    /**
     * Sets the engine user type.
     *
     * @param engineUserType
     *            the engineUserType to set
     */
    public void setEngineUserType(final String engineUserType) {
        this.engineUserType = engineUserType;
    }

}