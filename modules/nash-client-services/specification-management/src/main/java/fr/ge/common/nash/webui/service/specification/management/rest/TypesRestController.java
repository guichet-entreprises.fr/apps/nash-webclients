/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.management.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.util.ValueAdapterResourceTypeEnum;

/**
 * The Class EngineController.
 *
 * @author Christian Cougourdan
 */
@Controller("specificationManagementTypesRestController")
public class TypesRestController {

    /**
     * Gets a type preview.
     *
     * @param response
     *            the response
     * @param version
     *            the version
     * @param type
     *            the type
     * @throws IOException
     *             an IO exception
     */
    @RequestMapping(value = "/rest/type/{version}/{type}/preview", method = RequestMethod.GET)
    public void preview(final HttpServletResponse response, @PathVariable(value = "version") final String version, @PathVariable(value = "type") final String type) throws IOException {
        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(version, type);

        if (null == valueAdapter) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.getWriter().print(String.format("Type '%s' not found in version '%s'", type, version));
            return;
        }

        response.setContentType(MediaType.IMAGE_PNG_VALUE);

        final byte[] asBytes = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.PREVIEW, valueAdapter.getClass());
        if (null != asBytes) {
            response.getOutputStream().write(asBytes);
        }
    }

}
