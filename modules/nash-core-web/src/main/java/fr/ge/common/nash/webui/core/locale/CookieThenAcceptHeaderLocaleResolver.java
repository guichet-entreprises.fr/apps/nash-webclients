/**
 * 
 */
package fr.ge.common.nash.webui.core.locale;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;

/**
 * Custom locale resolver managing single or multiple translation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class CookieThenAcceptHeaderLocaleResolver implements LocaleResolver {

    /** Enable single translation. **/
    @Value("${single.translation:false}")
    private boolean singleTranslation;

    /** Locale resolver using browser accept-header. **/
    private AcceptHeaderLocaleResolver acceptHeaderLocaleResolver;

    /** Cookie locale resolver. **/
    private CookieLocaleResolver cookieLocaleResolver;

    /**
     * {@inheritDoc}
     */
    @Override
    public Locale resolveLocale(final HttpServletRequest request) {
        if (this.singleTranslation) {
            return this.cookieLocaleResolver.resolveLocale(request);
        }
        return this.acceptHeaderLocaleResolver.resolveLocale(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLocale(final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
        if (this.singleTranslation) {
            this.cookieLocaleResolver.setLocale(request, response, locale);
        } else {
            this.acceptHeaderLocaleResolver.setLocale(request, response, locale);
        }
    }

    /**
     * Setting locale according to specification loader.
     * 
     * @param loader
     *            The specification loader
     * @param request
     *            The HTTP request
     * @param response
     *            The HTTP response
     * @param locale
     *            The default locale
     */
    public void resolve(final SpecificationLoader loader, final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
        if (this.singleTranslation) {
            final TranslationsElement translationElement = Optional.ofNullable(loader.description()).map(FormSpecificationDescription::getTranslations).orElse(null);
            if (null == translationElement) {
                return;
            }
            final FormSpecificationDescription description = loader.description();
            final Locale localeSpec = Optional.ofNullable(description) //
                    .map(FormSpecificationDescription::getLang) //
                    .map(lng -> {
                        return Locale.forLanguageTag(lng);
                    }) //
                    .orElse(locale);
            this.cookieLocaleResolver.setLocale(request, response, localeSpec);
        }
    }

    /**
     * Mutateur sur l'attribut {@link #acceptHeaderLocaleResolver}.
     *
     * @param acceptHeaderLocaleResolver
     *            la nouvelle valeur de l'attribut acceptHeaderLocaleResolver
     */
    public void setAcceptHeaderLocaleResolver(final AcceptHeaderLocaleResolver acceptHeaderLocaleResolver) {
        this.acceptHeaderLocaleResolver = acceptHeaderLocaleResolver;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieLocaleResolver}.
     *
     * @param cookieLocaleResolver
     *            la nouvelle valeur de l'attribut cookieLocaleResolver
     */
    public void setCookieLocaleResolver(final CookieLocaleResolver cookieLocaleResolver) {
        this.cookieLocaleResolver = cookieLocaleResolver;
    }
}
