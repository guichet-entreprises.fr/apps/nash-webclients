/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.devtools.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import javax.servlet.Filter;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class AbstractRecordControllerTest.
 *
 * @author Christian Cougourdan
 */
public class AbstractRecordControllerTest extends AbstractTest {

    /** The Constant RECORD_UID. */
    protected static final String RECORD_UID = "2016-03-AAA-AAA-27";

    /** The mvc. */
    protected MockMvc mvc;

    /** The record service. */
    @Autowired
    protected IRecordService recordService;

    /** The web app context. */
    @Autowired
    protected WebApplicationContext webAppContext;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.recordService);

        this.mockRecordService();
    }

    /**
     * Mock record service.
     *
     * @throws Exception
     *             exception
     */
    protected void mockRecordService() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description.xml")).build());
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data.xml")).build());
        when(this.recordService.upload(eq(RECORD_UID), any(String.class), any(byte[].class))).thenReturn(Response.ok().lastModified(Calendar.getInstance().getTime()).build());
    }

}
