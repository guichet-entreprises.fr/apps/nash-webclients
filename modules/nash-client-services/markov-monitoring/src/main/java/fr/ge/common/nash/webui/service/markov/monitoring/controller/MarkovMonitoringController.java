/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.markov.monitoring.controller;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.markov.ws.v1.service.IQueueMessageRestService;

/**
 * Markov monitoring controller.
 *
 * @author jpauchet
 */
@Controller
public class MarkovMonitoringController {

    @Autowired
    @Qualifier("queueMessageRestService")
    private IQueueMessageRestService service;

    /**
     * Inits.
     * 
     * @return a template name
     */
    @RequestMapping(value = "/markov/monitoring", method = RequestMethod.GET)
    public String init() {
        return "markov/monitoring/main";
    }

    @RequestMapping(value = "/markov/monitoring/message/{id}/content", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable("id") final Long id) {
        final Response response = this.service.download(id);

        if (Status.NO_CONTENT.getStatusCode() == response.getStatus()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION));
        return new ResponseEntity<>(response.readEntity(byte[].class), headers, HttpStatus.OK);

    }
}
