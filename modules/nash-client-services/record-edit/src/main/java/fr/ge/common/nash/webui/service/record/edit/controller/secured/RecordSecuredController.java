/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.ws.v1.bean.EntryInfoBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.ZipUtil;

/**
 * Main record secured controller, managing records display, download and
 * remove. Allow to check for external user permissions.
 *
 * @author Christian Cougourdan
 */
@Component
public class RecordSecuredController extends AbstractSecuredController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordSecuredController.class);

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /**
     * Download record as binary ZIP resource.
     *
     * @param loader
     *            record loader
     * @return record ZIP resource as HTTP response
     */
    public ResponseEntity<byte[]> downloadRecord(final SpecificationLoader loader) {
        final byte[] resource = loader.getProvider().asBytes();

        if (null == resource || resource.length == 0) {
            LOGGER.warn("Unexpected record service NULL response");
            return ResponseEntity.noContent().build();
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s.zip", loader.description().getRecordUid()));
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    /**
     * Download record documents indexed by meta data.
     *
     * @param loader
     *            record loader
     * @return resource as HTTP response
     */
    public ResponseEntity<byte[]> downloadRecordDocuments(final SpecificationLoader loader) {
        final Collection<String> documentFullPathNames = this.getDocumentMeta(loader);
        if (CollectionUtils.isEmpty(documentFullPathNames)) {
            return ResponseEntity.noContent().build();
        }

        final Collection<String> documentBaseNames = new HashSet<>( //
                documentFullPathNames.stream() //
                        .map(Paths::get) //
                        .map(Path::getFileName) //
                        .map(Path::toString) //
                        .collect(Collectors.toSet()) //
        );

        final Map<String, String> documentPaths = new LinkedHashMap<>();
        for (final String documentPath : documentFullPathNames) {
            final String basename = Paths.get(documentPath).getFileName().toString();
            if (documentPaths.containsKey(basename)) {
                final String newBasename = this.findFirstIncrementedBasename(basename, documentBaseNames);
                if (null != newBasename) {
                    documentPaths.put(newBasename, documentPath);
                }
            } else {
                documentPaths.put(basename, documentPath);
            }
        }

        final byte[] resource = ZipUtil.create( //
                documentPaths.keySet(), //
                resourcePath -> loader.getProvider().asBytes(documentPaths.get(resourcePath)) //
        );

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s.zip", loader.description().getRecordUid()));
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    /**
     *
     * @param basename
     * @param basenames
     * @return
     */
    private String findFirstIncrementedBasename(final String basename, final Collection<String> basenames) {
        final Matcher m = Pattern.compile("^(.*)(\\.[^.]+)?$").matcher(basename);
        if (m.matches()) {
            final String mask = String.format( //
                    "%s-%%d.%s", //
                    m.group(1).replaceAll("%", "%%"), //
                    m.group(2).replaceAll("%", "%%") //
            );
            for (int idx = 1; idx <= 100; idx++) {
                final String newBasename = String.format(mask, idx);
                if (!basenames.contains(newBasename)) {
                    basenames.add(newBasename);
                    return newBasename;
                }
            }
            LOGGER.warn("Security error : too many resource with name [{}]", basename);
        } else {
            LOGGER.warn("Unable to parse resource path [{}]", basename);
        }

        return null;
    }

    public ResponseEntity<byte[]> downloadRecordElement(final SpecificationLoader loader, final String resourcePath) {
        final byte[] resource = loader.getProvider().asBytes(resourcePath);

        if (null == resource || resource.length == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=%s", resourcePath));
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        }
    }

    public List<String> displayRecordElementsList(final SpecificationLoader loader, final String filter) {
        final List<String> files = new ArrayList<>();

        final RecordInfoBean record = this.recordService.load(loader.description().getRecordUid());
        if (null != record.getEntries()) {
            Stream<String> stream = record.getEntries().stream().map(EntryInfoBean::getName);
            if (null != filter) {
                stream = stream.filter(name -> name.endsWith('.' + filter));
            }
            files.addAll(stream.collect(Collectors.toList()));
        }

        return files;
    }

    /**
     * Retrieve indexed documents from meta data. If not presents, follow each steps
     * from active or last one to find record documents to index.
     *
     * @param loader
     *            record loader
     * @return Document paths collection
     */
    private Collection<String> getDocumentMeta(final SpecificationLoader loader) {
        final Collection<String> documents = Optional.ofNullable(loader.meta()) //
                .map(FormSpecificationMeta::getMetas) //
                .map(Collection::stream) //
                .map(stream -> stream.filter(meta -> "document".equals(meta.getName())).map(MetaElement::getValue).collect(Collectors.toList())) //
                .orElseGet(Collections::emptyList);

        if (!documents.isEmpty()) {
            return documents;
        }

        final StepElement currentStep = loader.stepsMgr().current();
        if (this.isLastUserStep(loader, currentStep)) {
            /*
             * Current step is DONE or SEALED only if it is the last one
             */
            for (int stepIndex = currentStep.getPosition(); stepIndex >= 0; stepIndex -= 1) {
                if (loader.addDocumentMeta(stepIndex)) {
                    return Optional.ofNullable(loader.meta()) //
                            .map(FormSpecificationMeta::getMetas) //
                            .map(Collection::stream) //
                            .map(stream -> stream.filter(meta -> "document".equals(meta.getName())).map(MetaElement::getValue).collect(Collectors.toList())) //
                            .orElseGet(Collections::emptyList);
                }
            }
        }

        return Collections.emptyList();
    }

    /**
     * Remove record specified by its identifier.
     *
     * @param code
     *            record identifier.
     * @return empty OK response
     */
    public ResponseEntity<String> removeRecord(final String code) {
        this.recordService.remove(code);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
