/**
 *
 */
package fr.ge.common.nash.webui.service.specification.management.rest;

import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.engine.adapter.v1_2.StringValueAdapter;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class TypesRestControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class TypesRestControllerTest extends AbstractTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    /**
     * Test preview.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPreview() throws Exception {
        final String type = "String";
        final String version = "1.2";
        this.mvc.perform(get("/rest/type/{version}/{type}/preview.png", version, type)) //
                .andExpect(status().is(HttpStatus.OK.value())) //
                .andExpect(header().string("Content-Type", MediaType.IMAGE_PNG_VALUE)) //
                .andExpect(content().bytes(this.resourceAsBytes("/" + StringValueAdapter.class.getName().replace('.', '/') + "Resources/preview.png")));
    }

    /**
     * Test preview unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPreviewUnknown() throws Exception {
        final String type = "Unknown";
        final String version = "1.2";
        this.mvc.perform(get("/rest/type/{version}/{type}/preview.png", version, type)) //
                .andExpect(status().is(HttpStatus.NOT_FOUND.value())) //
                .andExpect(header().string("Content-Type", not(MediaType.IMAGE_PNG_VALUE))) //
                .andExpect(content().string(String.format("Type '%s' not found in version '%s'", type, version)));
    }

}
