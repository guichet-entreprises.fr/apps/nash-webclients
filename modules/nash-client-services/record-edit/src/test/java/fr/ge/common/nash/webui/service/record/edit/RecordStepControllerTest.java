/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.core.util.RecordStorageImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;

/**
 * Class RecordStepControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/test-storage-context.xml" })
@WebAppConfiguration
public class RecordStepControllerTest extends AbstractRecordControllerTest {

    /** La constante RECORD_UID_UNKNOWN. */
    private static final String RECORD_UID_UNKNOWN = "2016-12-REC-AAA-42";

    /** record storage mock. */
    @Autowired
    private RecordStorageImpl recordStorageMock;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.recordStorageMock);
    }

    /**
     * Test edit not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditNotFound() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1", RECORD_UID_UNKNOWN)) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/main"));
    }

    /**
     * Test edit.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEdit() throws Exception {
        final MvcResult actual = this.mvc.perform(get("/record/{code}/1", RECORD_UID)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl(String.format("/record/%s/1/page/0", RECORD_UID))) //
                .andReturn();

        actual.getResponse().getContentAsString();
    }

    /**
     * Test edit specific page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditSpecificPage() throws Exception {
        final int page = 1;

        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 1, page)) //
                .andExpect(view().name("record/edit/main")) //
                .andExpect(model().attribute("code", RECORD_UID)) //
                .andExpect(model().attribute("stepIndex", 1)) //
                .andExpect(model().attribute("page", page)); //
    }

    /**
     * Test edit specific page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditFutureStep() throws Exception {
        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 2, 1)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(view().name("redirect:/record/{code}/{step}/page/{page}")) //
                .andExpect(redirectedUrl(MessageFormat.format("/record/{0}/{1}/page/{2}", RECORD_UID, 1, 0))); //
    }

    /**
     * Test edit start.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditStart() throws Exception {

        when(this.recordService.load(RECORD_UID)).thenReturn(new RecordInfoBean().setCode(RECORD_UID));

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl(String.format("/record/%s/1/page/%d", RECORD_UID, 0)));
    }

    /**
     * Test edit too high page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditTooHighPage() throws Exception {
        final int page = 1;

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/page/{page}", RECORD_UID, page + 10)) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));
    }

    /**
     * Test save.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSave() throws Exception {
        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/updated.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "next") //
                .param("group.id", "group01") //
                .param("group01data01", "Doe") //
                .param("group01data02", "John");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/record/" + RECORD_UID + "/1/page/1"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray).replaceAll("[\r\n]+", " "), new String(captor.getValue()).replaceAll("[\r\n]+", " "));
    }

    /**
     * Test save last group.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveLastGroup() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 1) //
                .param("success", "next") //
                .param("group.id", "group_1");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(MockMvcResultMatchers.forwardedUrl("/dashboard"));
    }

    /**
     * Test save no change.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveNoChange() throws Exception {
        final byte[] expectedAsByteArray = this.resourceAsBytes("spec/not-updated.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("success", "exit") //
                .param("group.id", "group00") //
                .param("group01data01", "Doe") //
                .param("group01data02", "John");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(MockMvcResultMatchers.forwardedUrl("/dashboard"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService).upload(eq(RECORD_UID), eq("1-data/data.xml"), captor.capture());

        assertEquals(new String(expectedAsByteArray).replaceAll("[\r\n]+", " "), new String(captor.getValue()).replaceAll("[\r\n]+", " "));
    }

    /**
     * Test save no group id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveNoGroupId() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0)) //
                .andExpect(MockMvcResultMatchers.status().is5xxServerError()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/default/main")) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("uuid")) //
                .andReturn();
    }

    /**
     * Test save no success param.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveNoSuccessParam() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 0) //
                .param("group.id", "group_1");

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful()) //
                .andExpect(MockMvcResultMatchers.forwardedUrl("/dashboard"));
    }

    /**
     * Test save validate.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSaveValidate() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/1/page/{page}", RECORD_UID, 1) //
                .param("group.id", "group02") //
                .param("success", "validate");

        this.mvc.perform(request) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record/" + RECORD_UID + "/2"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.recordService, times(2)).upload(eq(RECORD_UID), eq("description.xml"), captor.capture());

        final FormSpecificationDescription actual = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationDescription.class);

        assertThat(actual.getSteps().getElements(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("status", equalTo(StepStatusEnum.DONE.getStatus())), //
                                hasProperty("status", equalTo(StepStatusEnum.DONE.getStatus())), //
                                hasProperty("status", equalTo(StepStatusEnum.TO_DO.getStatus())) //
                        ) //
                ) //
        );
    }

    /**
     * Test save validate last step.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSaveValidateLastStep() throws Exception {
        when(this.recordService.download(RECORD_UID, "2-option/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/2-option/data.xml")).build());
        when(this.recordService.download(RECORD_UID)).thenReturn(Response.ok(new ByteArrayInputStream(this.resourceAsBytes("spec.zip"))).build());

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/2/page/{page}", RECORD_UID, 0) //
                .param("group.id", "group02") //
                .param("success", "validate");

        this.mvc.perform(request) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/record/" + RECORD_UID + "/1"));

        verify(this.recordService, times(1)).upload(eq(RECORD_UID), eq("description.xml"), any());
        verify(this.recordStorageMock, never()).storeRecord(eq(RECORD_UID), any(InputStream.class), eq("specRefCode"));
    }

    /**
     * Test edit start.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditStartGenerated() throws Exception {
        final String resourcePath = "3-attachment/data-generated.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/3-attachment/data-generated-empty.xml");

        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-attachment.xml")).build());
        when(this.recordService.download(RECORD_UID, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        Arrays.asList("3-attachment/preprocess.xml", "3-attachment/types.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/{step}/page/{page}", RECORD_UID, 2, 0).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/main")) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("code", "stepIndex", "page"));
    }

    @Test

    public void testHangoutPostLastStep() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangout-last-step.xml")).build());

        Arrays.asList("6-hangout/data.xml", "6-hangout/preprocess.xml", "6-hangout/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        try (InputStream in = new ByteArrayInputStream(this.resourceAsBytes("spec.zip"))) {
            when(this.recordService.download(RECORD_UID)).thenReturn(Response.ok(in).build());

            this.mvc.perform(post("/record/{code}/{step}/page/{page}", RECORD_UID, 1, "0").param("success", "validate").param("group.id", "grp01")) //
                    .andExpect(status().is2xxSuccessful());
        }

        verify(this.recordService).upload(eq(RECORD_UID), eq("6-hangout/data.xml"), any());
        verify(this.recordService).upload(eq(RECORD_UID), eq("6-hangout/data-post01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("6-hangout/data-post02.xml"), any());
    }

    @Test
    public void testHanginPre() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangin.xml")).build());

        Arrays.asList("4-hangin/data.xml", "4-hangin/preprocess.xml", "4-hangin/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(get("/record/{code}/{step}/process/{processPhase}/{processId}", RECORD_UID, "step02", "pre", "prc02")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("record/edit/main")) //
                .andExpect(model().attributeExists("code", "stepIndex", "page"));

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-pre01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-pre02.xml"), any());
        verify(this.recordService).upload(eq(RECORD_UID), eq("4-hangin/data-pre03.xml"), any());
    }

    @Test
    public void testHanginPreRedirect() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangin-last-step.xml")).build());

        Arrays.asList("5-hangin/data.xml", "5-hangin/preprocess.xml", "5-hangin/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(get("/record/{code}/{step}/process/{processPhase}/{processId}", RECORD_UID, "step03", "pre", "prc01")) //
                .andExpect(redirectedUrl("https://www.projet-ge.fr/"));

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-pre01.xml"), any());
        verify(this.recordService).upload(eq(RECORD_UID), eq("5-hangin/data-pre02.xml"), any());

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-post01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-post02.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-post03.xml"), any());
    }

    @Test
    public void testHanginPost() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangin.xml")).build());

        Arrays.asList("4-hangin/data.xml", "4-hangin/preprocess.xml", "4-hangin/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(get("/record/{code}/{step}/process/{processPhase}/{processId}", RECORD_UID, "step02", "post", "prc02")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl(MessageFormat.format("/record/{0}/{1}", RECORD_UID, 2))); //

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-pre01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-pre02.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-pre03.xml"), any());

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-post01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("4-hangin/data-post02.xml"), any());
        verify(this.recordService).upload(eq(RECORD_UID), eq("4-hangin/data-post03.xml"), any());
    }

    @Test
    public void testHanginUnknownStepIndex() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangin.xml")).build());

        Arrays.asList("4-hangin/data.xml", "4-hangin/preprocess.xml", "4-hangin/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(get("/record/{code}/{step}/process/{processPhase}/{processId}", RECORD_UID, 42, "post", "prc02")) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl(MessageFormat.format("/record/{0}/{1}/page/{2}", RECORD_UID, 1, 0)));
    }

    @Test
    public void testHanginPostLastStep() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-hangin-last-step.xml")).build());

        Arrays.asList("5-hangin/data.xml", "5-hangin/preprocess.xml", "5-hangin/postprocess.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(get("/record/{code}/{step}/process/{processPhase}/{processId}", RECORD_UID, "step03", "post", "prc02")) //
                .andExpect(forwardedUrl("/dashboard"));

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-pre01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-pre02.xml"), any());

        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-post01.xml"), any());
        verify(this.recordService, times(0)).upload(eq(RECORD_UID), eq("5-hangin/data-post02.xml"), any());
        verify(this.recordService).upload(eq(RECORD_UID), eq("5-hangin/data-post03.xml"), any());
    }

    /**
     * Test edit in read only mode.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditReadOnlyMode() throws Exception {
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("readonly/description.xml")).build());
        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("readonly/1-data/data.xml")).build());
        when(this.recordService.download(RECORD_UID)).thenReturn(Response.ok(new ByteArrayInputStream(this.resourceAsBytes("readonly.zip"))).build());

        final int page = 1;

        this.mvc.perform(get("/record/{code}/{step}/page/{page}", RECORD_UID, 1, page)) //
                .andExpect(view().name("record/edit/main")) //
                .andExpect(model().attribute("code", RECORD_UID)) //
                .andExpect(model().attribute("stepIndex", 1)) //
                .andExpect(model().attribute("page", page)); //
    }
}
