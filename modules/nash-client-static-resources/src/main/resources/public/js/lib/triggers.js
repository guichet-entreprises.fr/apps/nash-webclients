/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/ValueReference', 'lib/ValidationError', 'lib/i18n', 'lib/func/localScopeModel', 'lib/utils', '_fm_rules', '_fm_data' ], function($, ValueReference, ValidationError, i18n, localScopeModel, Utils, _rules, _data) {

    var SCOPED_PATH = /^(?:([^:]+):)?(.*)$/ ;

    var triggersBySource = {};
    var triggersByTarget = {};


    function executeAction(target, predicate) {
        var fn = typeof predicate === 'function' ? predicate : predicate.predicate;
        var groupModel = localScopeModel(target.path.relative);
        var ctx = {
            target: target,
            model: {
                _record: _data._record,
                _step: _data._step,
                _page: _data._page,
                _group: groupModel
            }
        };
        return fn.bind(ctx)(groupModel);
    }


    var triggerActions = {
            'field:trigger' : function(target, predicate) {
                return executeAction(target, predicate);
            },
            'group:trigger' : function(target, predicate) {
                return executeAction(target, predicate);
            }
        };

    buildTriggers(_rules);

    function add(elementPath, actionName, dependencyKey, dependsOn, predicate) {
        var absoluteElementPath = '_page.' + elementPath;
        var absoluteParentElementPrefix = absoluteElementPath.replace(/\.[^.]+$/, '.');

        var ruleKey = Utils.buildRuleKey(dependencyKey.replace(/^_group./, absoluteParentElementPrefix));
        var absoluteDependencyKey = dependencyKey.replace(/^_group./, 'group:' + absoluteParentElementPrefix).replace(/\[[^\]]*\]/g, '[*]');
        var absoluteElementPattern = absoluteElementPath.replace(/\[[^\]]*\]/g, '[*]');

        var triggerOn = triggersBySource[ruleKey] || { pattern: buildPattern(absoluteDependencyKey) };
        var triggerOnDomain = triggerOn[actionName] || {};

        triggerOnDomain[absoluteElementPattern] = {
                predicate: predicate,
                dependencies: dependsOn
        };
        triggerOn[actionName] = triggerOnDomain;
        triggersBySource[ruleKey] = triggerOn;
    };


    function addByTarget(elementPath, actionName, predicate) {
        var ruleKey = Utils.buildRuleKey(elementPath);
        var predicates = triggersByTarget[ruleKey] || {};
        predicates[actionName] = predicate;
        triggersByTarget[ruleKey] = predicates;
    }


    function buildPattern(key) {
        var path = SCOPED_PATH.exec(key)[2];

        return new RegExp('^' + path.replace(/([.])/g, '\\$1').replace(/\[\*\]/g, '\\[[^\\]]*\\]'));
    }

    /**
     * Get server generate rules and arrange to an exploitable structure.
     * 
     * Input object :
     *      rules = {
     *          <target field path> : {
     *              type : <group | field>,
     *              <domain : display | trigger> : {
     *                  dependsOn : [ <scoped field path>, ... ],
     *                  predicate : function (_group) { ... }
     *              }
     *          }
     *      }
     * 
     * Output objects :
     *      triggersBySource = {
     *          <key corresponding to source @data-rule> : {
     *              pattern : <regexp pattern based on scoped field path>,
     *              <type:domain> : {
     *                  <scoped target field> : function (_group) { ... }
     *              }
     *          }
     *      }
     * 
     *      triggersByTarget = {
     *          <key corresponding to target @data-rule> : {
     *              <type:domain> : function (_group) { ... }
     *          }
     *      }
     */
    function buildTriggers(rules) {
        Object.keys(rules || {}, true).forEach(function (targetPath) {
            var ruleInfo = rules[targetPath];
            console.log('[Triggers.build] rule info (' + targetPath + ') =', ruleInfo);
            Object.keys(ruleInfo, true).forEach(function (domain) {
                if (ruleInfo[domain].dependsOn) {
                    console.log('[Triggers.build] rule (' + targetPath + ' / ' + domain + ') depends on =', ruleInfo[domain].dependsOn);
                    ruleInfo[domain].dependsOn.forEach(function(dep) {
                        add(targetPath, ruleInfo.type + ':' + domain, dep, ruleInfo[domain].dependsOn, ruleInfo[domain].predicate);
                    });

                    addByTarget(targetPath, ruleInfo.type + ':' + domain, ruleInfo[domain].predicate);
                }
                if ('trigger' == domain) {
                    var actionName = ruleInfo.type + ':' + domain;
                    var triggerOn = {};
                    var triggerOnDomain = triggerOn[actionName] || {};

                    triggerOnDomain[targetPath] = ruleInfo[domain].predicate;
                    triggerOn[actionName] = triggerOnDomain;
                    triggersBySource[targetPath] = triggerOn;
                }
            });
        });

        console.debug('[Triggers.build] triggers by sources =', triggersBySource);
        console.debug('[Triggers.build] triggers by targets =', triggersByTarget);
    }


    function cascade(src, predicate, cascading) {
        if (typeof src === 'string') {
            src = Utils.normalize(src);
        }

        var nextTrueStep = [];
        var nextFalseStep = [];

        var triggerOn = triggersBySource[src.key()];

        if (triggerOn) {

            console.debug('[Triggers.cascade] found trigger :', triggerOn);

            if (triggerOn) {
                Object.keys(triggerOn, true).forEach(function (domain) {
                    if (true === cascading && domain.endsWith(':trigger')) {
                        return;
                    }
                    var action = triggerActions[domain];
                    if (action) {
                        Object.keys(triggerOn[domain], true).forEach(function(target) {
                            var scopedTarget = Utils.normalize(target);
                            var targets = [];
                            if (scopedTarget.path.absolute.indexOf('[*]') > 0) {
                                /*
                                 * Cardinality
                                 */
                                var prefixPattern = buildPattern(scopedTarget.path.absolute.replace(/\.[^.]+$/, ''));
                                var prefix = prefixPattern.exec(src.path.absolute);
                                if (prefix) {
                                    /*
                                     * Same group
                                     */
                                    targets = [ Utils.normalize(scopedTarget.path.absolute.replace(prefixPattern, prefix[0])) ];
                                } else {
                                    /*
                                     * Other group, scan for all groups
                                     */
                                    $('[data-rule="' + scopedTarget.key() + '"]').each(function () {
                                        targets.push(Utils.normalize($(this)));
                                    });
                                }
                            } else {
                                targets = [ scopedTarget ];
                            }

                            targets.forEach(function (t) {
                                var actionResult = action(t, predicate || triggerOn[domain][target]);
                                if (scopedTarget.path.absolute !== src.path.absolute) {
                                    if (actionResult) {
                                        if (nextTrueStep.indexOf(t) < 0) {
                                            nextTrueStep.push(t);
                                        }
                                    } else if (nextFalseStep.indexOf(t) < 0) {
                                        nextFalseStep.push(t);
                                    }
                                }
                            });
                        });
                    }

                });
            }
        }

        nextTrueStep.forEach(function (elm) {
            cascade(elm, undefined, true);
        });

        nextFalseStep.forEach(function (elm) {
            cascade(elm, function () { return false; }, true);
        });
    }



    function execute(target) {
        var normalizedTarget = Utils.normalize(target);
        var triggers = triggersByTarget[normalizedTarget.key()] || {};

        Object.keys(triggers, true) //
            .forEach(function (domain) {
                var predicate = triggers[domain];
                var action = triggerActions[domain];
                if (undefined !== action) {
                    action(normalizedTarget, predicate);
                }
            });
    }



    function registerAction(name, action) {
        triggerActions[name] = action;
    }



    return {
        cascade: cascade,
        registerAction: registerAction, // deprecated
        action: {
            register: registerAction,
            execute: executeAction
        },
        add: add,
        execute: execute
    };

});