/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/ValueReference', 'lib/triggers', 'engine/Value', '_fm_referentials' ], function($, ValueReference, triggers, Value, fmReferentials) {

    triggers.registerAction('referential', function (target, predicate) {
        return target.fragment().trigger('fm.update');
    });

    function truePredicate() {
        return true;
    }

    function declareTriggers() {
        Object.keys(fmReferentials.fields, true).forEach(function (fieldName) {
            var field = fmReferentials.fields[fieldName];
            var displayConditionKeys = field.values //
                .reduce(function (previous, elm) { return previous.concat(elm.displayConditions); }, []) //
                .reduce(function (previous, elm) { return previous.indexOf(elm) < 0 ? previous.concat([ elm ]) : previous; }, []);

            var dependsOn = displayConditionKeys //
                .reduce(function (previous, elm) { return previous.concat(fmReferentials.displayConditions[elm].dependsOn); }, [])
                .reduce(function (previous, elm) { return previous.indexOf(elm) < 0 ? previous.concat([ elm ]) : previous; }, []);

            dependsOn.forEach(function (elm) {
                triggers.add(fieldName, 'referential', elm, truePredicate);
            });
        });
    }

    function checkPredicates(displayConditions) {
        var checkedPredicates = {};
        if (displayConditions) {
            for (var i = 0; i < displayConditions.length; i++) {
                var displayConditionKey = displayConditions[i];
                if (checkedPredicates[displayConditionKey] == undefined) {
                    checkedPredicates[displayConditionKey] = fmReferentials.displayConditions[displayConditionKey].predicate();
                }
                if (!checkedPredicates[displayConditionKey]) {
                    return false;
                }
            }
        }
        return true;
    }

    function findFieldReferential(refName) {
        var fieldReferential = fmReferentials.fields[refName];
        if (!fieldReferential) {
            fieldReferential = fmReferentials.fields[refName.replace(/\[[^\]]*\]/g, '[0]')];
        }
        if (!fieldReferential) {
            fieldReferential = fmReferentials.fields[refName.replace(/\[[^\]]*\]/g, '[]')];
        }
        return fieldReferential;
    }

    function update(field, opts, callback, isExternal) {
        var refName = field.data('field');
        if (!refName) {
            refName = '';
        }

        var fieldReferential = findFieldReferential(refName);

        if (!fieldReferential) {
            return;
        }

        var name = field.data('field');
        var savedFieldOption = ValueReference.create(name).get();
        if (!savedFieldOption) {
            savedFieldOption = {};
        }

        var savedFieldOptions;
        if (savedFieldOption) {
            if (savedFieldOption.options) {
                savedFieldOptions = savedFieldOption.options;
            } else if ($.isArray(savedFieldOption)) {
                savedFieldOptions = savedFieldOption;
            } else {
                savedFieldOptions = [savedFieldOption];
            }
        }

        if (!isExternal && fieldReferential.values && fieldReferential.values.length > 0) {
            var fieldOptions = [];
            $.each(fieldReferential.values, function(index, value) {
                if (!value.displayConditions || checkPredicates(value.displayConditions)) {
                    fieldOptions.push(value);
                }
            });
            var form = field.closest('form[name="record"]');
            var i18nAvailable = form.data("i18navailable");
            var i18nLang = form.data("i18nlang");
            var singleTranslation = form.data("singletranslation");
    
            $.each(fieldOptions, function(index, fieldOption) {
                var isOptionSelected = savedFieldOptions && savedFieldOptions.filter(function (obj) {
                    return fieldOption.id == obj.id;
                }).length > 0;
                if (singleTranslation && fieldOption.translation) {
                	fieldOption.label = fieldOption.translation;
                }
                callback(fieldOption, isOptionSelected, i18nAvailable, i18nLang);
            });
        } else if (isExternal && fieldReferential.external) {
            callback(savedFieldOptions, fieldReferential.external);
        }
    }

    $(function () {
        declareTriggers();
    });



    return {
        update : update
    };

});
