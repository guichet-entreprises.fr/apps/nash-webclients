package fr.ge.common.nash.webui.service.markov.record.search.controller;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/test-engine-context.xml" })
@WebAppConfiguration
public class MarkovRecordSearchControllerTest {

    /** The mvc. */
    private MockMvc mvc;

    @Autowired
    IRecordService recordService;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    @Before
    public void setUp() throws Exception {

        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.recordService);
    }

    @Test
    public void testSearch() throws Exception {
        // call
        this.mvc.perform(MockMvcRequestBuilders.get("/markov/search")) //
                .andExpect(status().isOk());
    }

    @Test
    public void testSearchData() throws Exception {

        // prepare

        SearchQueryOrder queryOrder = new SearchQueryOrder("updated", "desc");
        List<SearchQueryOrder> orders = new ArrayList<SearchQueryOrder>();
        orders.add(queryOrder);

        List<SearchQueryFilter> filters = new ArrayList<SearchQueryFilter>();

        RecordSearchResult sr = new RecordSearchResult(0, 10);

        when(this.recordService.search(0, 10, filters, orders)).thenReturn(sr);

        // call
        this.mvc.perform(MockMvcRequestBuilders.get("/markov/search/data?draw=1&start=0&length=10")) //
                .andExpect(status().isOk());
    }

}
