/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.manager.processor.bridge.document.PdfDocument;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfFieldChoice;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfFieldDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ConftypeElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ReferentialElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.webui.service.devtools.bean.PdfFieldDescriptionBean;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ResourceUtil;

/**
 * PDF engine controller.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping(value = "/engine/pdf")
public class PdfEngineController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfEngineController.class);

    /** Resource pattern. */
    private static final String RESOURCE_PATTERN = String.format("/%s/%%s", PdfEngineController.class.getPackage().getName().replace('.', '/').replaceFirst("^(.*)(/[^/]+)$", "$1"));

    /** Dev tool PDF. */
    private static final String DEV_TOOL_PDF = "devToolPdf";

    private static final Map<String, BiConsumer<PdfFieldDescription, DataElement>> FIELD_TYPES;

    static {
        final Map<String, BiConsumer<PdfFieldDescription, DataElement>> m = new HashMap<>();

        final BiConsumer<PdfFieldDescription, DataElement> combo = (nfo, fld) -> {
            if (nfo.isMulti()) {
                fld.setType("ComboBoxMultiple");
            } else {
                fld.setType("ComboBox");
            }
        };

        m.put("text", (nfo, fld) -> {
            if (nfo.isMulti()) {
                fld.setType("Text");
            } else {
                fld.setType("String");
            }
        });

        m.put("password", (nfo, fld) -> fld.setType("Password"));
        m.put("checkbox", (nfo, fld) -> {
            if (null != nfo.getChoices() && nfo.getChoices().size() == 1) {
                fld.setType("BooleanCheckBox");
                fld.setConftype(null);
            } else {
                fld.setType("CheckBoxes");
            }
        });
        m.put("radio", (nfo, fld) -> fld.setType("Radios"));
        m.put("list", combo);
        m.put("combo", combo);

        FIELD_TYPES = Collections.unmodifiableMap(m);
    }

    /**
     * Inits the PDF screen.
     *
     * @param request
     *            the request
     * @param model
     *            the model
     * @return the PDF screen
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init(final HttpServletRequest request, final Model model) {
        final byte[] fileBytes = (byte[]) request.getSession().getAttribute(DEV_TOOL_PDF);
        if (fileBytes != null) {
            try (final PDDocument doc = PDDocument.load(fileBytes);) {
                final List<PdfFieldDescriptionBean> fields = new ArrayList<>();
                final Map<String, Map<Integer, Float[]>> coordinatesRatios = new HashMap<>();
                if (doc != null) {
                    this.findFieldsAndCoordinatesRatios(doc, fields, coordinatesRatios);
                    model.addAttribute("numberOfPages", doc.getNumberOfPages());
                }
                model.addAttribute("fields", fields);
                model.addAttribute("coordinatesRatios", coordinatesRatios);
            } catch (final IOException ex) {
                throw new TechnicalException("", ex);
            }
        }
        return "engine/pdf/main";
    }

    /**
     * Uploads a PDF.
     *
     * @param request
     *            the request
     * @param file
     *            the file
     * @return the redirect string
     */
    @RequestMapping(method = RequestMethod.POST)
    public String upload(final Model model, final HttpServletRequest request, @RequestParam("file") final MultipartFile file) {
        try {
            request.getSession().setAttribute(DEV_TOOL_PDF, file.getBytes());
        } catch (final IOException ex) {
            throw new TechnicalException("", ex);
        }
        return "redirect:/engine/pdf";
    }

    /**
     * Gets the PDF display.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @param model
     *            the model
     * @param pageIndex
     *            the page index
     * @throws IOException
     *             an IO exception
     */
    @RequestMapping(value = "/display", method = RequestMethod.GET)
    public void display(final HttpServletRequest request, final HttpServletResponse response, final Model model, @RequestParam(value = "pageIndex", defaultValue = "0") final int pageIndex)
            throws IOException {
        final byte[] fileBytes = (byte[]) request.getSession().getAttribute(DEV_TOOL_PDF);
        InputStream in = null;
        try (final PDDocument doc = PDDocument.load(fileBytes);) {
            final PDFRenderer pdfRenderer = new PDFRenderer(doc);
            final BufferedImage bim = pdfRenderer.renderImageWithDPI(pageIndex, 80, ImageType.RGB);
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bim, "png", os);
            in = new ByteArrayInputStream(os.toByteArray());
        } catch (final IOException ex) {
            throw new TechnicalException("", ex);
        }
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    /**
     * Find fields and coordinates ratios.
     *
     * @param doc
     *            the document
     * @param fields
     *            the fields
     * @param coordinatesRatios
     *            the coordinates ratios
     */
    private void findFieldsAndCoordinatesRatios(final PDDocument doc, final List<PdfFieldDescriptionBean> fields, final Map<String, Map<Integer, Float[]>> coordinatesRatios) {
        final PDAcroForm form = doc.getDocumentCatalog().getAcroForm();
        if (form != null) {
            int maxFieldNameLength = 0;
            for (final PDField field : form.getFields()) {
                coordinatesRatios.put(field.getFullyQualifiedName(), new HashMap<>());
                final String fieldName = field.getFullyQualifiedName();
                for (final PDAnnotationWidget widget : field.getWidgets()) {
                    final PDPage widgetPage = widget.getPage();
                    if (null == widgetPage) {
                        LOGGER.warn("No page defined for one widget of field '{}'", fieldName);
                        continue;
                    }
                    final int widgetPageIndex = doc.getPages().indexOf(widgetPage);
                    final PDRectangle widgetRectangle = widget.getRectangle();
                    final PDRectangle pageRectangle = widget.getPage().getBBox();
                    final Float lowerLeftXRatio = widgetRectangle.getLowerLeftX() / pageRectangle.getUpperRightX();
                    final Float lowerLeftYRatio = widgetRectangle.getLowerLeftY() / pageRectangle.getUpperRightY();
                    final Float upperRightXRatio = widgetRectangle.getUpperRightX() / pageRectangle.getUpperRightX();
                    final Float upperRightYRatio = widgetRectangle.getUpperRightY() / pageRectangle.getUpperRightY();
                    coordinatesRatios.get(fieldName).put(widgetPageIndex, new Float[] { lowerLeftXRatio, lowerLeftYRatio, upperRightXRatio, upperRightYRatio });
                }
                if (fieldName.length() > maxFieldNameLength) {
                    maxFieldNameLength = fieldName.length();
                }
                fields.add(new PdfFieldDescriptionBean(fieldName, field.getClass(), field instanceof PDCheckBox ? "false" : "''"));
            }
            for (final PdfFieldDescriptionBean field : fields) {
                field.setSpaceToAlign(StringUtils.rightPad(StringUtils.EMPTY, maxFieldNameLength - field.getName().length()));
            }
        }
    }

    /**
     * Generates a form automatically.
     *
     * @param request
     *            a request
     * @param response
     *            a response
     * @return the form
     * @throws IOException
     *             an {@link IOException}
     */
    @RequestMapping(value = "/autoForm", method = RequestMethod.GET)
    public ResponseEntity<byte[]> generateAutoForm(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final byte[] pdfModelAsBytes = (byte[]) request.getSession().getAttribute(DEV_TOOL_PDF);
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s.zip", "autoForm"));
        if (pdfModelAsBytes == null) {
            return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
        }

        final StringBuilder pdfGenerateScriptBuilder = new StringBuilder("var pdfFields = {};\n");
        final List<GroupElement> pages = new ArrayList<>();

        final Map<String, IElement<?>> fieldsById = new LinkedHashMap<>();

        try (PdfDocument pdf = new PdfDocument(pdfModelAsBytes)) {
            final Map<String, PdfFieldDescription> pdfFields = pdf.getFields();
            final int maxFieldNameLength = this.findMaxFieldNameLength(pdfFields.values());
            String previousPage = null;

            for (final Entry<String, PdfFieldDescription> entry : pdfFields.entrySet()) {
                final PdfFieldDescription nfo = entry.getValue();
                final GroupElement page = this.findPage(nfo.getPage(), pages);
                final IElement<?> fld = this.buildDataField(nfo);

                if (fieldsById.containsKey(fld.getId())) {
                    final IElement<?> prev = fieldsById.get(fld.getId());
                    if (null == fld.getMaxOccurs() || null == prev.getMaxOccurs()) {
                        LOGGER.info("Duplicate field '{}'", nfo.getName());
                    } else if (Integer.valueOf(prev.getMaxOccurs()) < Integer.valueOf(fld.getMaxOccurs())) {
                        ((DataElement) prev).setMaxOccurs(fld.getMaxOccurs());
                    }
                } else {
                    if (null == previousPage || !previousPage.equals(page.getId())) {
                        pdfGenerateScriptBuilder.append(String.format("\nvar %s = $step1.%s;\n", page.getId(), page.getId()));
                        previousPage = page.getId();
                    }

                    page.getData().add(fld);
                    pdfGenerateScriptBuilder.append( //
                            String.format( //
                                    "pdfFields['%s'] %s= %s.%s;\n", //
                                    nfo.getName(), //
                                    StringUtils.repeat(' ', maxFieldNameLength - nfo.getName().length()), //
                                    page.getId(), //
                                    fld.getId() //
                            ) //
                    );

                    fieldsById.put(fld.getId(), fld);
                }
            }
        }

        pdfGenerateScriptBuilder.append(ResourceUtil.resourceAsString("../preprocess.js", this.getClass()));

        final DataElement defaultDataElement = new DataElement();
        defaultDataElement.setMandatory(false);
        defaultDataElement.setType("String");

        final DefaultElement defaultElement = new DefaultElement();
        defaultElement.setData(defaultDataElement);

        final FormSpecificationData spec = new FormSpecificationData("data.xml");
        spec.setId("step1");
        spec.setLabel("Step #1");
        spec.setDefault(defaultElement);
        spec.setGroups(pages.stream().filter(elm -> null != elm && CollectionUtils.isNotEmpty(elm.getData())).collect(Collectors.toList()));

        final IProvider autoFormZipProvider = new ZipProvider(ResourceUtil.resourceAsBytes("../autoForm.zip", this.getClass()));
        autoFormZipProvider.save("1-data/data.xml", spec);
        autoFormZipProvider.save("3-review/preprocess.js", pdfGenerateScriptBuilder.toString().getBytes(StandardCharsets.UTF_8));
        autoFormZipProvider.save("3-review/models/model.pdf", pdfModelAsBytes);

        return new ResponseEntity<>(autoFormZipProvider.asBytes(), headers, HttpStatus.OK);
    }

    private int findMaxFieldNameLength(final Collection<PdfFieldDescription> values) {
        return values.stream().mapToInt(nfo -> nfo.getName().length()).max().orElse(0);
    }

    /**
     * Build form data element from PDF field description.
     *
     * @param nfo
     *            PDF field description
     * @return data element
     */
    private IElement<?> buildDataField(final PdfFieldDescription nfo) {
        final Pattern REPEATABLE_POSITION = Pattern.compile("^(.*)\\[([0-9]+)\\]$");

        final DataElement fld = new DataElement();
        if (StringUtils.isNotEmpty(nfo.getDescription()) && !nfo.getName().equals(nfo.getDescription())) {
            fld.setDescription(nfo.getDescription());
        }

        final Matcher m = REPEATABLE_POSITION.matcher(nfo.getName());
        if (m.matches()) {
            fld.setId(this.toCode(m.group(1)));
            fld.setLabel(this.toLabel(m.group(1)));
            fld.setMinOccurs("0");
            fld.setMaxOccurs(m.group(2));
        } else {
            fld.setId(this.toCode(nfo.getName()));
            fld.setLabel(this.toLabel(nfo.getName()));
        }

        if (nfo.isRequired()) {
            fld.setMandatory(true);
        }

        if (CollectionUtils.isNotEmpty(nfo.getChoices())) {
            final ReferentialElement ref = new ReferentialElement();
            ref.setReferentialTextElements( //
                    nfo.getChoices().stream() //
                            .map(choice -> new ReferentialTextElement(choice.getCode(), this.getChoiceLabel(choice))) //
                            .collect(Collectors.toList()) //
            );

            final ConftypeElement confType = new ConftypeElement();
            confType.setReferentialElements(Arrays.asList(ref));

            fld.setConftype(confType);
        }

        Optional.ofNullable(FIELD_TYPES.get(nfo.getType())).orElse(FIELD_TYPES.get("text")).accept(nfo, fld);

        return fld;
    }

    /**
     *
     * @param choice
     * @return
     */
    private String getChoiceLabel(final PdfFieldChoice choice) {
        if (StringUtils.isNotEmpty(choice.getValue())) {
            return this.toLabel(choice.getValue());
        } else {
            return this.toLabel(choice.getCode());
        }
    }

    /**
     * Find a page in specified list, create it if necessary.
     *
     * @param idx
     *            position of requested page
     * @param pages
     *            list of all pages
     * @return corresponding page
     */
    private GroupElement findPage(final int idx, final List<GroupElement> pages) {
        final int offset = idx + 1;
        for (int i = pages.size(); i <= offset; i++) {
            final GroupElement page = new GroupElement();
            page.setId("page" + i);
            page.setLabel("Page #" + i);
            page.setData(new ArrayList<>());
            pages.add(page);
        }

        return pages.get(offset);
    }

    /**
     * Translate input string as a label, ie all characters other than alphanumeric
     * are replaced by spaces, upper case uses to delimitate words.
     *
     * @param value
     *            source string
     * @return corresponding label
     */
    private String toLabel(final String value) {
        final String str = CoreUtil.searchAndReplace( //
                value.replaceAll("[^a-zA-Z0-9]+", " ") //
                        .replaceAll("(?<=[a-z0-9])([A-Z])", " $1"), //
                "([A-Z])(?=[a-z])", m -> ' ' + m.group(1).toLowerCase() //
        ).replaceAll("[ ]{2,}", " ").trim();

        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * Translate input string as a camelcase code.
     *
     * @param value
     *            source string
     * @return corresponding label
     */
    private String toCode(final String value) {
        if (null == value) {
            return null;
        }

        String valueToTranslate = value;
        final char c = value.charAt(0);
        if (c >= '0' && c <= '9') {
            valueToTranslate = "fld" + value;
        } else if (value.length() < 2) {
            return value;
        }

        return CoreUtil.searchAndReplace( //
                valueToTranslate.substring(0, 1).toLowerCase() + valueToTranslate.substring(1), //
                Pattern.compile("[^a-z0-9\\[\\]]+([a-z0-9])", Pattern.CASE_INSENSITIVE), m -> m.group(1).toUpperCase() //
        );
    }

    /**
     * Error handler.
     *
     * @param response
     *            response
     * @param ex
     *            ex
     * @return string
     */
    @ExceptionHandler
    public String errorHandler(final HttpServletResponse response, final Exception ex) {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return "commons/error/default/main";
    }

}
