/**
 * 
 */
package fr.ge.common.nash.exception.expert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import fr.ge.common.nash.core.exception.ExpressionException;

/**
 * @author bsadil
 *
 */

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionHandlerController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    private static final String INNER_SUFFIX = "/inner";

    /**
     * Handles expressions.
     *
     * @param response
     *            the response
     * @param exception
     *            an expression exception
     * @return a {@link ModelAndView}
     */
    @ResponseBody
    @ExceptionHandler(ExpressionException.class)
    public ModelAndView expressionHandler(HttpServletRequest request, final HttpServletResponse response, final ExpressionException exception) {
        LOGGER.error("Error on evaluating expression : {}\n", exception.getMessage(), exception.getExpression(), exception);

        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        final ModelAndView modelAndView = new ModelAndView("record/error/build" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }
}
