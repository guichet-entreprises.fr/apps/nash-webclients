package fr.ge.common.nash.webui.service.record.edit.rest.v2.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.clicker.ws.v1.bean.ResponseReferenceBean;
import fr.ge.common.clicker.ws.v1.service.IReferenceRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Passthrough service with Clicker bubble.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ClickerPassthroughRestServiceImpl implements IReferenceRestService {

    @Autowired
    private IReferenceRestService referenceRestService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<ResponseReferenceBean> search(long startIndex, long maxResults, List<SearchQueryFilter> filters, List<SearchQueryOrder> orders) {
        return this.referenceRestService.search(startIndex, maxResults, filters, orders);
    }

}
