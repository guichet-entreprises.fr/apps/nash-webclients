/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.controller;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.ws.provider.RecordServiceProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;

/**
 * Class AbstractRecordController.
 *
 * @author Christian Cougourdan
 */
@RequestMapping("/record/{code:[0-9]{4}-[0-9]{2}-[A-Z]{3}-[A-Z]{3}-[0-9]{2}}/{step}")
public abstract class AbstractRecordController {

    /** La constante ATTR_PAGE. */
    protected static final String ATTR_PAGE = "page";

    /** La constante ATTR_STEP_INDEX. */
    protected static final String ATTR_STEP_INDEX = "stepIndex";

    /** La constante ATTR_CODE. */
    protected static final String ATTR_CODE = "code";

    /** La constante ATTR_STEP. */
    protected static final String ATTR_STEP = "step";

    /** La constante ATTR_STEP. */
    protected static final String VALID_RECORD = "done";

    /** The record service. */
    @Autowired
    protected IRecordService recordService;

    /** The engine. */
    @Autowired
    private Engine engine;

    /**
     * Constructor.
     */
    protected AbstractRecordController() {
        // Nothing to do.
    }

    /**
     * Loader.
     *
     * @param code
     *            the code
     * @return the specification loader
     */
    protected SpecificationLoader loader(final String code) {
        MDC.put("recordId", code);
        return this.engine.loader(RecordServiceProvider.create(this.recordService, code));
    }

}
