
var pdfModel = nash.doc.load('models/model.pdf').apply(pdfFields);

var data = [ spec.createData({
    id : 'record',
    label : 'Record',
    description : 'Record generated from the typed data',
    type : 'FileReadOnly',
    value : [ pdfModel.save('record.pdf') ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Generated records',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Review',
    groups : groups
});
