/**
 *
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.utils.test.AbstractTest;

/**
 * @author bsadil
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class TransitionEngineControllerTest extends AbstractTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** Engine controller. */
    @Autowired
    private TransitionEngineController transitionEngineController;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
    }

    @Test
    public void testInitTransition() {
        final ExtendedModelMap model = new ExtendedModelMap();
        assertEquals(this.transitionEngineController.init(model), "engine/transitions/main");
    }

    /**
     * Tests
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRequestDatatable() throws Exception {
        this.mvc.perform(get("/engine/transitions/name/datatable?draw=1&start=0&length=10&name=send")) //
                .andExpect(status().isOk());
    }

    @Test
    public void testRequestDatatableWithEmptyName() throws Exception {
        this.mvc.perform(get("/engine/transitions/name/datatable?draw=1&start=0&length=10")) //
                .andExpect(status().isOk());
    }

    @Test
    public void testhelp() throws Exception {
        this.mvc.perform(get("/engine/transitions/name?type=sendEmail-1.0")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
    }

    @Test
    public void testhelpWithEmptyType() throws Exception {
        this.mvc.perform(get("/engine/transitions/name?type=")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string(""));
    }
}
