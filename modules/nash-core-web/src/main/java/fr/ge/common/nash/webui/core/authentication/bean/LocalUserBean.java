/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.core.authentication.bean;

import org.apache.commons.lang3.StringUtils;

import fr.ge.core.bean.UserBean;

/**
 * The Class LocalUserBean.
 *
 * @author Christian Cougourdan
 */
public class LocalUserBean extends UserBean {

    /**
     * User country : <br>
     * Example : France.
     */
    private String country;

    /**
     * User prefered language : <br>
     * Example : fr.
     */
    private String language;

    /**
     * Tag an user as anonymous.
     */
    private boolean anonymous;

    /** User first name. */
    private String firstname;

    /** User last name. */
    private String lastname;

    /** Roles as string. **/
    private String roles;

    /**
     * Instantiates a new local user bean.
     *
     * @param id
     *            the id
     */
    public LocalUserBean(final String id) {
        super(id);
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * Sets the country.
     *
     * @param country
     *            the country
     * @return the local user bean
     */
    public LocalUserBean setCountry(final String country) {
        this.country = country;
        return this;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Sets the language.
     *
     * @param language
     *            the language
     * @return the local user bean
     */
    public LocalUserBean setLanguage(final String language) {
        this.language = language;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #anonymous}.
     *
     * @return boolean anonymous
     */
    public boolean isAnonymous() {
        return this.anonymous;
    }

    /**
     * Mutateur sur l'attribut {@link #anonymous}.
     *
     * @param anonymous
     *            la nouvelle valeur de l'attribut anonymous
     */
    public LocalUserBean setAnonymous(final boolean anonymous) {
        this.anonymous = anonymous;
        return this;
    }

    /**
     * Gets the firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return this.firstname;
    }

    /**
     * Sets the firstname.
     *
     * @param firstname
     *            the firstname to set
     */
    public LocalUserBean setFirstname(final String firstname) {
        this.firstname = firstname;
        return this;
    }

    /**
     * Gets the lastname.
     *
     * @return the lastname
     */
    public String getLastname() {
        return this.lastname;
    }

    /**
     * Sets the lastname.
     *
     * @param lastname
     *            the lastname to set
     */
    public LocalUserBean setLastname(final String lastname) {
        this.lastname = lastname;
        return this;
    }

    @Override
    public String toString() {
        final String displayName = StringUtils.isNotEmpty(this.lastname) ? this.lastname : StringUtils.EMPTY;
        final String displayFirstName = StringUtils.isNotEmpty(this.firstname) ? this.firstname : StringUtils.EMPTY;
        return String.format("%s %s", displayName, displayFirstName);
    }

    /**
     * Accesseur sur l'attribut {@link #roles}.
     *
     * @return String roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Mutateur sur l'attribut {@link #roles}.
     *
     * @param roles
     *            la nouvelle valeur de l'attribut roles
     */
    public LocalUserBean setRoles(final String roles) {
        this.roles = roles;
        return this;
    }

}
