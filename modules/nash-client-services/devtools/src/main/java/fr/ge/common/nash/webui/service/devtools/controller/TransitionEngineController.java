/**
 *
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.pegdown.PegDownProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.nash.engine.provider.TransitionFactory;
import fr.ge.common.nash.engine.provider.TransitionProvider;
import fr.ge.common.utils.web.search.bean.DatatableSearchQuery;
import fr.ge.common.utils.web.search.bean.DatatableSearchQueryOrder;
import fr.ge.common.utils.web.search.bean.DatatableSearchResult;

/**
 * Transition engine controller.
 *
 * @author bsadil
 */
@Controller
@RequestMapping(value = "/engine/transitions")
public class TransitionEngineController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransitionEngineController.class);

    /** The Constant DESC. */
    public static final String DESC = "desc";

    /** The peg down processor. */
    @Autowired
    private PegDownProcessor pegDownProcessor;

    /**
     * Constructor.
     */
    public TransitionEngineController() {
        // Nothing to do.
    }

    /**
     * Inits the types.
     *
     * @param model
     *            the model
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init(final Model model) {
        return "engine/transitions/main";
    }

    /**
     * Gets the datatable types by version.
     *
     * @param criteria
     *            the search criteria
     * @param version
     *            the version
     * @param name
     *            the name
     * @return the types
     */
    @RequestMapping(value = "/name/datatable", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public DatatableSearchResult<String> requestDatatable(final DatatableSearchQuery criteria, @RequestParam(value = "name", required = false) final String name) {
        int draw = 0;
        int start = 0;
        int length = 0;
        if (criteria != null) {
            if (criteria.getDraw() != null) {
                draw = criteria.getDraw();
            }
            if (criteria.getStart() != null && criteria.getLength() != null) {
                start = criteria.getStart();
                length = criteria.getLength();
            }
        }
        final Map<String, TransitionProvider> transitions = TransitionFactory.loadAllTransitions();

        final List<String> filtredTransition = new ArrayList<>();
        List<String> transitionsName = null;
        if (StringUtils.isNotEmpty(name)) {
            transitionsName = transitions.keySet().stream().filter(t -> t.startsWith(name)).collect(Collectors.toList());
        } else {
            transitionsName = transitions.keySet().stream().collect(Collectors.toList());
        }
        final int resultStart = start;
        final int resultEnd = Math.min(start + length, transitionsName.size());
        for (int i = resultStart; i < resultEnd; ++i) {
            final String tarnsitionName = transitionsName.get(i);
            filtredTransition.add(tarnsitionName);
        }

        if (this.isReverseOrder(criteria)) {
            Collections.reverse(filtredTransition);
        } else {
            Collections.sort(filtredTransition);
        }

        final DatatableSearchResult<String> datatableSearchResult = new DatatableSearchResult<>(draw);
        datatableSearchResult.setData(filtredTransition);
        datatableSearchResult.setRecordsFiltered(filtredTransition.size());
        datatableSearchResult.setRecordsTotal(transitions.size());
        return datatableSearchResult;
    }

    /**
     * Return true if first order parameter is reverse.
     *
     * @param query
     * @return
     */
    private boolean isReverseOrder(final DatatableSearchQuery query) {
        return Optional.ofNullable(query) //
                .map(DatatableSearchQuery::getOrder) //
                .filter(lst -> !lst.isEmpty()) //
                .map(lst -> lst.get(0)) //
                .map(DatatableSearchQueryOrder::getDir) //
                .filter(dir -> DESC.equals(dir)) //
                .isPresent();
    }

    /**
     * Help.
     *
     * @param name
     *            the name
     * @return the string
     */
    @RequestMapping(value = "/name", method = RequestMethod.GET)
    @ResponseBody
    public String help(@RequestParam(value = "type", required = false) final String name) {
        String helpAsHtml = "Not found";

        final Map<String, TransitionProvider> transitions = TransitionFactory.loadAllTransitions();

        if (transitions.containsKey(name)) {
            final TransitionProvider provider = transitions.get(name);

            final InputStream helpInputStream = new ByteArrayInputStream(provider.asBytes("description.md"));
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(helpInputStream, StandardCharsets.UTF_8))) {
                final String helpAsMarkdown = reader.lines().collect(Collectors.joining("\n"));
                helpAsHtml = this.pegDownProcessor.markdownToHtml(helpAsMarkdown);
            } catch (final IOException ex) {
                LOGGER.error("Rendering engine conditions test page : error while loading help content", ex);
            }

            return helpAsHtml;
        }

        return null;
    }

}
