/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/clipboard', 'prismjs', 'lib/datatables', 'parsley' ], function($, Clipboard, Prism) {

    window.ajaxTypeSearch = function(data, callback, settings) {
        var version = $("#versionsList").val();

        data.version = version;
        data.name = $("#search-name").val();

        $.ajax(baseUrl + 'engine/types/byversion/datatable', {
            method : "GET",
            data : data,
            cache : false
        }).done(function(res) {
            var objectVersions = [];
            $.each(res.data, function(index, value) {
                objectVersions.push({
                    version : version,
                    name : value
                });
            });
            res.data = objectVersions;
            callback && callback(res);
        });
    };

    function getAjaxOptionsData(version, type) {
        var $form = $('form[name="typeOptionsForm"]');
        var params;

        if ($form.length > 0) {
            params = $form.serializeArray();
        } else {
            params = [ {
                name : 'version',
                value : null
            }, {
                name : 'type',
                value : null
            } ];
        }

        if (version && type) {
            params = params.map(function(elm) {
                if ('version' == elm.name) {
                    elm.value = version;
                } else if ('type' == elm.name) {
                    elm.value = type;
                }
                return elm;
            });
        }

        return params;
    }

    function validateOptionsData() {
        var validates = false;
        var typeOptionsForm = $("form[name=typeOptionsForm]");
        if (typeOptionsForm.length == 0) {
            validates = true;
        } else {
            var typeOptionsFormParsley = typeOptionsForm.parsley();
            typeOptionsFormParsley.validate();
            validates = typeOptionsFormParsley.isValid();
        }
        return validates;
    }

    function refreshTypeDescription(version, type) {
        var typeOptionsForm = $('form[name="typeOptionsForm"]');
        if (typeOptionsForm.length > 0) {
            typeOptionsForm.parsley().destroy();
        }

        var data = getAjaxOptionsData(version, type);

        $.ajax(baseUrl + 'engine/types/description', {
            method : "GET",
            data : data,
            cache : false
        }).done(function(res) {
            $("#type-description-wrapper").html(res);
            refreshTypeXml();
            refreshTypeDisplay();
            $("#btn-types-copy-clipboard").click(function() {
                Clipboard.copy("type-xml");
            });
        });
    }

    function refreshTypeXml() {
        if (validateOptionsData()) {
            $.ajax(baseUrl + 'engine/types/xml', {
                method : "GET",
                data : getAjaxOptionsData(),
                cache : false
            }).done(function(res) {
                Prism.highlightElement($("#type-xml > code").html(res)[0]);
//                $('pre code').each(function(i, block) {
//                    hljs.highlightBlock(block);
//                });
            });
        }
    }

    function refreshTypeDisplay() {
        if (validateOptionsData()) {
            $.ajax(baseUrl + 'engine/types/display', {
                data : getAjaxOptionsData(),
                cache : false,
                dataType : 'html'
            }).done(function(html) {
                $('#type-display-content').html(html);
                $("#type-display-wrapper").removeClass("not-displayed");
            });
        }
    }

    function refreshTypesGallery() {
        $.ajax(baseUrl + 'engine/types/byversion/gallery', {
            method : "GET",
            data : {
                version : $('#versionsGallery').val()
            },
            cache : false
        }).done(function(res) {
            $("#type-gallery-wrapper").html(res);
        });
    }

    function isListMode() {
        return $("#btn-types-list-mode-button").not(":visible").length > 0;
    }

    function switchToListMode() {
        if (!isListMode()) {
            $("#versionsList").val($("#versionsGallery").val());
            $("form[name=typesListForm]").submit();
            $("#btn-types-list-mode-button").hide();
            $("#types-gallery-mode").hide();
            $("#types-list-mode").show();
            $("#btn-types-gallery-mode-button").show();
        }
    }

    function isGalleryMode() {
        return $("#btn-types-gallery-mode-button").not(":visible").length > 0;
    }

    function switchToGalleryMode() {
        if (!isGalleryMode()) {
            $("#versionsGallery").val($("#versionsList").val());
            refreshTypesGallery();
            $("#btn-types-gallery-mode-button").hide();
            $("#types-list-mode").hide();
            $("#types-gallery-mode").show();
            $("#btn-types-list-mode-button").show();
        }
    }

    var routes = {};
    function route(path, fn) {
        var re = '^#' + path + '$';
        routes[re] = {
            re : new RegExp(re),
            fn : fn
        };
    }

    route('list', function(m) {
        switchToListMode();
    });

    route('gallery', function(m) {
        switchToGalleryMode();
    });

    route('show\/([^\/]+)\/(.*)', function(m) {
        switchToListMode();
        refreshTypeDescription(m[1], m[2]);
    });

    $(function() {

        [ 'prism', 'prism-line-highlight', 'prism-line-numbers' ].forEach(function (css) {
            $('head').append('<link rel="stylesheet" href="' + baseUrl + '/css/libs/prismjs/' + css + '.css" />');
        });

        // switching modes
        $("#btn-types-list-mode-button").click(function() {
            window.location.hash = 'list';
        });
        $("#btn-types-gallery-mode-button").click(function() {
            window.location.hash = 'gallery';
        });

        $(window).on("hashchange", function(evt) {
            var hash = window.location.hash;
            Object.keys(routes).forEach(function (re) {
                var route = routes[re];
                var m = route.re.exec(hash);
                if (m) {
                    route.fn(m);
                    return false;
                }
            });
        });

        // list mode
//        hljs.initHighlightingOnLoad();
        var typesDatatable = $("#types").DataTable({
            dom : "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12'p>>"
        });
        $("form[name=typesListForm]").submit(function(event) {
            typesDatatable.draw();
            event.preventDefault();
        });
        $("#versionsList").change(function() {
            typesDatatable.draw();
        });
        $("#search-name").change(function() {
            typesDatatable.draw();
        });
        $("#type-description-wrapper").on("change", 'form[name="typeOptionsForm"] input, form[name="typeOptionsForm"] textarea', function() {
            refreshTypeXml();
            refreshTypeDisplay();
        });

        $(window).trigger('hashchange');

        // gallery mode
        $("#versionsGallery").change(refreshTypesGallery);

    });

});
