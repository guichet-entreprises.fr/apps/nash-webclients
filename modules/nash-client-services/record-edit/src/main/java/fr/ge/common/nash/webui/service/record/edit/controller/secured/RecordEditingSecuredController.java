/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import static fr.ge.common.utils.CoreUtil.time;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.context.ThreadContext;
import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.RecordStepDataNotFoundException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.DefaultAttributeManager;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.parser.TreeParserWithContext;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.engine.validation.Error;
import fr.ge.common.nash.engine.validation.data.NullTypeFunctionalValidator;
import fr.ge.common.nash.webui.service.record.edit.WebUiConstants;
import fr.ge.common.support.i18n.MessageFormatter;
import fr.ge.common.utils.CoreUtil;

/**
 * Secured record controller managing inner display. Allow to check for external
 * user permissions.
 *
 * @author Christian Cougourdan
 */
@Component
public class RecordEditingSecuredController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordEditingSecuredController.class);

    @Value("${engine.user.type}")
    private String engineUserType;

    @Value("${single.translation:false}")
    private boolean singleTranslation;

    @Value("${nash.readOnly:false}")
    private boolean readOnly;

    /**
     * Prepare template model to display specified record step and page. It returns
     * only HTML inner page, without HTML container.
     *
     * @param request
     *            HTTP request
     * @param response
     *            HTTP response
     * @param servletContext
     *            HTTP servlet context
     * @param model
     *            context model
     * @param loader
     *            record loader
     * @param stepIndex
     *            step index to display
     * @param page
     *            page index to display
     * @return template path
     */
    public String displayInnerPageFromIndex(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext, final Model model,
            final SpecificationLoader loader, final int stepIndex, final int page) {

        // --> get number of step to to customize the display (inner page)
        model.addAttribute("numberOfSteps", loader.stepsMgr().findAll().size());

        final FormSpecificationDescription description = loader.description();
        final String code = description.getRecordUid();
        final String version = description.getVersion();
        model.addAttribute("specDescription", description);

        final EngineContext<FormSpecificationData> engineContext = loader.buildEngineContext(stepIndex);
        if (null == engineContext) {
            LOGGER.info("Loading record step #{} page #{} : step not found", stepIndex, page);
            throw new RecordStepDataNotFoundException(stepIndex);
        }

        final StepElement step = engineContext.getParent(StepElement.class).getElement();

        final FormSpecificationData spec = engineContext.getElement();
        final Set<String> fieldsWithoutType = this.getFieldsWithoutTypeMessages(spec);

        final int idx = Math.min(null == spec.getGroups() ? 0 : spec.getGroups().size() - 1, page);

        final Map<String, Object> context = loader.extractUntil(step);
        context.putAll(RecursiveDataModelExtractor.create(engineContext.getProvider()).extract(spec));

        final GroupElement currentGrp;
        try {
            currentGrp = this.extractWorkingGroup(engineContext, context, idx);
        } catch (final ExpressionException ex) {
            throw new ExpressionException(step.getData(), ex);
        }

        // prepare i18n
        final String specLang = description.getLang();
        final String i18nLang = this.singleTranslation ? null : loader.i18n().getAlternateLanguage(LocaleContextHolder.getLocale().getLanguage());
        final boolean i18nAvailable = this.singleTranslation ? false : StringUtils.isNotEmpty(i18nLang);

        // final List<StepElement> steps =
        // loader.stepsMgr().findAll(engineUserType);
        final List<StepElement> steps = loader.stepsMgr().findAll();
        final int maxStepIndex = steps.size() - 1;

        model.addAttribute(WebUiConstants.ATTR_STEPS_SIZE, maxStepIndex);
        if (this.readOnly || loader.hasErrorState()) {
            model.addAttribute(WebUiConstants.ATTR_CURRENT_STEP, stepIndex);
        } else {
            model.addAttribute(WebUiConstants.ATTR_CURRENT_STEP, loader.stepsMgr().current().getPosition());
        }

        final StepElement stepElement = loader.stepsMgr().current();
        if (stepElement != null) {
            model.addAttribute("pathData", "/" + StringUtils.substringBefore(stepElement.getData(), "/"));
        }
        model.addAttribute(WebUiConstants.ATTR_VERSION, version);

        model.addAttribute(WebUiConstants.ATTR_CODE, code);
        model.addAttribute("user", this.engineUserType);

        model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);
        model.addAttribute(WebUiConstants.ATTR_STEP_INDEX, stepIndex);

        model.addAttribute(WebUiConstants.ATTR_PAGE, idx);

        model.addAttribute("stepElement", step);
        model.addAttribute("steps", steps);
        model.addAttribute("spec", spec);
        model.addAttribute("specLoader", loader);
        model.addAttribute("errorState", loader.hasErrorState());
        model.addAttribute("grp", currentGrp);
        model.addAttribute("context", context);
        model.addAttribute("previousPage", loader.previousGroup(stepIndex, idx));

        final Map<String, Object> additionalAttributes = new HashMap<>();
        additionalAttributes.put("specLang", specLang);
        additionalAttributes.put("i18nLang", i18nLang);
        additionalAttributes.put("i18nAvailable", i18nAvailable);
        additionalAttributes.put("singleTranslation", this.singleTranslation);
        additionalAttributes.put("readOnly", this.readOnly || loader.hasErrorState());

        model.addAllAttributes(additionalAttributes);

        time("Generate UI", () -> {
            model.addAttribute("pageContent", loader.pages().build(request, response, servletContext, engineContext, idx, additionalAttributes));
            return null;
        });

        Set<String> displayableExceptions = ThreadContext.get().extractDisplayableExceptions();
        if (displayableExceptions == null) {
            displayableExceptions = fieldsWithoutType;
        } else {
            displayableExceptions.addAll(fieldsWithoutType);
        }
        model.addAttribute("displayableExceptions", displayableExceptions);

        return "record/edit/inner";
    }

    /**
     * Build HTML fragment for record data element.
     *
     * @param model
     *            context model
     * @param loader
     *            record loader
     * @param fragment
     *            element path in record step (including page)
     * @param stepIndex
     *            step index
     * @param occurrenceIndex
     *            repeating element index
     * @param occurrencesTotal
     *            repeating element total
     * @param request
     *            HTTP request
     * @param response
     *            HTTP response
     * @param servletContext
     *            HTTP servlet context
     * @return base container template view name
     */
    public String displayFragment(final Model model, final SpecificationLoader loader, final String fragment, final int stepIndex, final Integer occurrenceIndex, final Integer occurrencesTotal,
            final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext) {

        final EngineContext<FormSpecificationData> engineContext = loader.buildEngineContext(stepIndex);

        final IElement<?> sourceElement = this.findRepeatableElement(engineContext, fragment);
        final IElement<?> element = new DefaultAttributeManager(engineContext.getElement()).register(loader.elements().clear(engineContext, sourceElement, null));

        if (null != occurrenceIndex) {
            element.setId(ElementPath.create(element.getId()).withIndex(occurrenceIndex).getFullPath());
        }

        element.setParentPath(ElementPath.create(fragment).getParentPath());

        if (StringUtils.isEmpty(element.getParentPath())) {
            element.setPath(element.getId());
        } else {
            element.setPath(element.getParentPath() + '.' + element.getId());
        }

        final String i18nLang = this.singleTranslation ? null : loader.i18n().getAlternateLanguage(LocaleContextHolder.getLocale().getLanguage());
        final boolean i18nAvailable = this.singleTranslation ? false : StringUtils.isNotEmpty(i18nLang);

        final Map<String, Object> additionalModelAttributes = new HashMap<>();
        additionalModelAttributes.put("i18nLang", i18nLang);
        additionalModelAttributes.put("i18nAvailable", i18nAvailable);
        additionalModelAttributes.put("singleTranslation", this.singleTranslation);
        additionalModelAttributes.put("readOnly", this.readOnly || loader.hasErrorState());

        model.addAttribute("fragmentContent",
                loader.pages().buildFragment(request, response, servletContext, engineContext, this.findPage(engineContext, fragment), element, additionalModelAttributes));

        return "record/fragment/main";
    }

    /**
     * Search for element in specified record step by its absolute path.
     *
     * @param engineContext
     *            record step engine context
     * @param elementPath
     *            element path to search
     * @return page position
     */
    private int findPage(final EngineContext<FormSpecificationData> engineContext, final String elementPath) {
        final String pageId = elementPath.replaceFirst("^([^.\\[\\]]+).*$", "$1");
        final List<GroupElement> pages = engineContext.getElement().getGroups();

        for (int idx = 0; idx < pages.size(); idx++) {
            final GroupElement grp = pages.get(idx);
            if (pageId.equals(ElementPath.create(grp.getId()).getId())) {
                return idx;
            }
        }

        return -1;
    }

    /**
     * Register default properties on group and data element and evaluate display
     * condition.
     *
     * @param recordModel
     *            the context
     * @param spec
     *            spec
     * @param pageIndex
     *            idx
     * @return prepared group element corresponding to idx
     */
    private GroupElement extractWorkingGroup(final EngineContext<FormSpecificationData> engineContext, final Map<String, Object> recordModel, final int pageIndex) {
        final GroupElement currentGrp = new DefaultAttributeManager(engineContext.getElement()).register(pageIndex);

        final Map<String, Object> stepModel = CoreUtil.cast(recordModel.get(engineContext.getElement().getId()));
        final Map<String, Object> pageModel = CoreUtil.cast(stepModel.get(currentGrp.getId()));

        /*
         * Rollback a previous bug used in formalities. Use "_step" instead of
         * "$<root xml ID>".
         */
        pageModel.put(engineContext.getElement().getId(), stepModel);

        final Map<String, Object> model = new HashMap<>();
        model.put(Scopes.RECORD.toContextKey(), recordModel);
        model.put(Scopes.STEP.toContextKey(), stepModel);
        model.put(Scopes.PAGE.toContextKey(), pageModel);

        engineContext.getRecord().getScriptEngine().eval("1", new ScriptExecutionContext().addAll(model), Object.class);
        final BiConsumer<IElement<?>, ScriptExecutionContext> action = this.createDisplayConditionAction(engineContext);

        TreeParserWithContext.create(action, action).parse(currentGrp, model);

        return currentGrp;
    }

    /**
     * Create display condition action for data extractor.
     *
     * @param engineContext
     *            record step engine context
     * @return extractor action
     */
    private BiConsumer<IElement<?>, ScriptExecutionContext> createDisplayConditionAction(final EngineContext<FormSpecificationData> engineContext) {
        final Map<String, Boolean> displayConditions = new HashMap<>();
        final ScriptExecutionContext scriptExecutionContext = new ScriptExecutionContext();

        return (elm, ctx) -> {
            final String exprAsString = elm.getDisplayCondition();
            if (!StringUtils.isEmpty(exprAsString)) {
                String cacheKey = exprAsString;
                Boolean displayed = displayConditions.get(cacheKey);
                if (null == displayed) {
                    final ScriptExpression expr = engineContext.getRecord().getScriptEngine().parse(exprAsString, true);
                    if (expr.getDependencies().stream().anyMatch(dep -> dep.startsWith(Scopes.GROUP.toContextKey() + '.'))) {
                        cacheKey = elm.getParentPath() + ':' + exprAsString;
                        displayed = displayConditions.get(cacheKey);
                    }

                    if (null == displayed) {
                        scriptExecutionContext.add(Scopes.GROUP.toContextKey(), ctx.getModel().get(Scopes.GROUP.toContextKey()));
                        displayed = engineContext.getRecord().getScriptEngine().eval(expr, scriptExecutionContext, Boolean.class);
                        displayConditions.put(cacheKey, displayed);
                    }
                }
                elm.setDisplay(displayed);
            }
        };
    }

    /**
     * Getter on attribute {@link #fields without type messages}.
     *
     * @param spec
     *            spec
     * @return fields without type messages
     */
    private Set<String> getFieldsWithoutTypeMessages(final FormSpecificationData spec) {
        Set<String> fieldsWithoutType = new HashSet<>();

        final MessageFormatter messageFormatter = NashMessageReader.getReader("po/fr.ge.common.nash.engine.validation.ValidationMessages").getFormatter();
        final Collection<Error> errors = new NullTypeFunctionalValidator().validate(messageFormatter, spec, null).getGlobalErrors();
        if (CollectionUtils.isNotEmpty(errors)) {
            fieldsWithoutType = new HashSet<>();
            for (final Error error : errors) {
                fieldsWithoutType.add(error.getMessage());
            }
        }

        return fieldsWithoutType;
    }

    /**
     * Search for repeatable element corresponding to specified element absolute
     * path.
     *
     * @param engineContext
     *            record step engine context
     * @param path
     *            repeatable element to find
     * @return found element
     */
    private IElement<?> findRepeatableElement(final EngineContext<FormSpecificationData> engineContext, final String path) {
        final String fragmentZeroRepeatables = path.replaceAll("\\[[^\\]]+\\]", "[0]");
        IElement<?> sourceElement = SpecificationLoader.find(engineContext.getElement(), fragmentZeroRepeatables);
        if (null == sourceElement) {
            final String fragmentEmptyRepeatables = path.replaceAll("\\[[^\\]]+\\]", "[]");
            sourceElement = SpecificationLoader.find(engineContext.getElement(), fragmentEmptyRepeatables);
        }
        return sourceElement;
    }

}
