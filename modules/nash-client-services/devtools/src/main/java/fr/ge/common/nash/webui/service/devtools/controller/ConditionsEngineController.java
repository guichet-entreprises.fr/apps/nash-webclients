/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.nash.core.context.ThreadContext;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.nash.ws.provider.RecordServiceProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class ConditionsEngineController.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping(value = "/engine/conditions")
public class ConditionsEngineController {

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    @Autowired
    private Engine engine;

    /**
     * Inits the conditions.
     *
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "engine/conditions/main";
    }

    /**
     * Execute.
     *
     * @param recordCode
     *            the record code
     * @param exprAsString
     *            the expr as string
     * @param groupId
     *            the group id
     * @param specId
     *            the spec id
     * @return the object
     */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object execute(@RequestParam("record") final String recordCode, @RequestParam("expr") final String exprAsString, @RequestParam(value = "grp", required = false) final String groupId,
            @RequestParam(value = "spc", required = false) final String specId) {

        final SpecificationLoader loader = this.loader(recordCode);

        final Map<String, Object> model = loader.buildEngineContext().buildModel();
        final ScriptExecutionContext ctx = new ScriptExecutionContext().addAll(model);
        if (null != specId) {
            final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
            final Map<String, Object> stepModel = CoreUtil.cast(recordModel.get(specId));
            ctx.add(Scopes.STEP.toContextKey(), stepModel);
            if (null != groupId) {
                ctx.add(Scopes.PAGE.toContextKey(), stepModel.get(groupId));
            }
        }

        final Map<String, Object> map = new HashMap<>();
        map.put("object", loader.getScriptEngine().eval(exprAsString, true, ctx));
        map.put("boolean", loader.getScriptEngine().eval(exprAsString, true, ctx, Boolean.class));
        map.put("displayableExceptions", ThreadContext.get().extractDisplayableExceptions());

        return map;
    }

    /**
     * Loader.
     *
     * @param code
     *            the code
     * @return the specification loader
     */
    protected SpecificationLoader loader(final String code) {
        return this.engine.loader(RecordServiceProvider.create(this.recordService, code));
    }

}
