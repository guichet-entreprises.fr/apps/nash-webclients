/**
 *
 */
package fr.ge.common.nash.webui.service.record.edit.filter;

import java.util.Base64;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;
import fr.ge.ct.authentification.utils.CookieUtils;

/**
 * The interceptor to assign a record to current user from cookie. The current
 * user will be the owner for the form requested.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordAssignInterceptor extends HandlerInterceptorAdapter {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordAssignInterceptor.class);

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** Anonymous profiler cookie name. **/
    @Autowired
    private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!this.authentificationConfigurationWebBean.isCookieAnonymousAllow()) {
            if (!(handler instanceof ResourceHttpRequestHandler)) {
                final Object variables = request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
                if (variables instanceof Map) {
                    Map<String, String> mapVariables = (Map<String, String>) variables;
                    if (null != mapVariables.get("code")) {
                        final String code = mapVariables.get("code");
                        final String userId = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);

                        final Cookie cookieAnonymous = CookieUtils.getCookie(this.authentificationConfigurationWebBean.getCookieAnonymousName(), request);
                        LOGGER.info("Find anonymous cookie {}", (null != cookieAnonymous));
                        final String cookieAnonymousValue = Optional.ofNullable(cookieAnonymous) //
                                .map(Cookie::getValue) //
                                .filter(value -> StringUtils.isNotEmpty(value) && StringUtils.isNotEmpty(userId)) //
                                .map(value -> new String(Base64.getDecoder().decode(value))) //
                                .orElse(null);

                        boolean isOwner = Optional.ofNullable(cookieAnonymousValue) //
                                .map(value -> this.recordService.own(code, value)) //
                                .orElse(false);

                        LOGGER.info("Trying to assign all records from anonymous user {} to the current user {}", cookieAnonymousValue, userId);
                        Optional.ofNullable(cookieAnonymousValue) //
                                .filter(value -> isOwner && !value.equals(userId)) //
                                .map(value -> this.recordService.assign(value, userId)) //
                                .orElse(null);
                    }
                }
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}
