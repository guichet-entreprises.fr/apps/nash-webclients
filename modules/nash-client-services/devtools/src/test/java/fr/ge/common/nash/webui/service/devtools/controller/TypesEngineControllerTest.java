/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.devtools.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.Filter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.AbstractReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.WarningElement;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class EngineControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class TypesEngineControllerTest extends AbstractTest {

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** Engine controller. */
    @Autowired
    private TypesEngineController engineController;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.recordService);
    }

    /**
     * Tests {@link TypesEngineController#init(org.springframework.ui.Model)}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInitTypes() throws Exception {
        final ExtendedModelMap model = new ExtendedModelMap();
        this.engineController.init(model);
        final List<String> versions = CoreUtil.cast(model.get("versions"));
        assertThat(versions, hasItems("latest", "1.0"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDatatable(fr.ge.common.nash.webui.forge.bean.datatable.DatatableSearchQuery, String, String)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetDatatableTypesByVersion() throws Exception {
        this.mvc.perform(get("/engine/types/byversion/datatable?draw=1&start=0&length=10&version=1.0")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string("{\"draw\":1,\"recordsTotal\":6,\"recordsFiltered\":6,\"data\":" //
                        + "[\"Boolean\",\"Date\",\"File\",\"FileReadOnly\",\"Integer\",\"String\"]}"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDatatable(fr.ge.common.nash.webui.forge.bean.datatable.DatatableSearchQuery, String, String)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetDatatableTypesByVersionLimitLength() throws Exception {
        this.mvc.perform(get("/engine/types/byversion/datatable?draw=1&start=0&length=3&version=1.0")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string("{\"draw\":1,\"recordsTotal\":6,\"recordsFiltered\":6,\"data\":" //
                        + "[\"Boolean\",\"Date\",\"File\"]}"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDatatable(fr.ge.common.nash.webui.forge.bean.datatable.DatatableSearchQuery, String, String)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetDatatableTypesByVersionNoLength() throws Exception {
        this.mvc.perform(get("/engine/types/byversion/datatable?version=1.0")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string("{\"draw\":0,\"recordsTotal\":6,\"recordsFiltered\":6,\"data\":[]}"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDatatable(fr.ge.common.nash.webui.forge.bean.datatable.DatatableSearchQuery, String, String)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetDatatableTypesByVersionWithName() throws Exception {
        this.mvc.perform(get("/engine/types/byversion/datatable?draw=1&start=0&length=10&version=1.0&name=te")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string("{\"draw\":1,\"recordsTotal\":2,\"recordsFiltered\":2,\"data\":" //
                        + "[\"Date\",\"Integer\"]}"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDatatable(fr.ge.common.nash.webui.forge.bean.datatable.DatatableSearchQuery, String, String)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetDatatableTypesByVersionWithNameUppercase() throws Exception {
        this.mvc.perform(get("/engine/types/byversion/datatable?draw=1&start=0&length=10&version=1.0&name=String")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andExpect(content().string("{\"draw\":1,\"recordsTotal\":1,\"recordsFiltered\":1,\"data\":" //
                        + "[\"String\"]}"));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestDescription(org.springframework.ui.Model, String, String, fr.ge.common.nash.webui.engine.mapping.form.v1_2.data.DataElement, java.util.Map)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetTypeDescription() throws Exception {
        final String expected = this.resourceAsString("description.html");
        final byte[] actual = this.mvc.perform(get("/engine/types/description?version=1.2&type=String")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andReturn().getResponse().getContentAsByteArray();

        final Pattern pattern = Pattern.compile("^[ ]*[\n\r]+|\r", Pattern.MULTILINE);
        Assert.assertEquals(pattern.matcher(expected).replaceAll(""), pattern.matcher(new String(actual, StandardCharsets.UTF_8)).replaceAll(""));
    }

    /**
     * Tests
     * {@link TypesEngineController#requestXml(org.springframework.ui.Model, String, String, fr.ge.common.nash.webui.engine.mapping.form.v1_2.data.DataElement, java.util.Map)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetTypeXml() throws Exception {
        final String expected = this.resourceAsString("xml.html");
        final byte[] actual = this.mvc.perform(get("/engine/types/xml?version=1.2&type=String&id=id&label=label&mandatory=true&description=The field description&options.min=4&options.max=15")) //
                .andExpect(status().isOk()) //
                .andDo(print()) //
                .andReturn().getResponse().getContentAsByteArray();

        final Pattern pattern = Pattern.compile("^[ ]*[\n\r]+|\r", Pattern.MULTILINE);
        Assert.assertEquals(pattern.matcher(expected).replaceAll(""), pattern.matcher(new String(actual, StandardCharsets.UTF_8)).replaceAll(""));
    }

    /**
     * Test display.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplay() throws Exception {
        final MockHttpServletRequestBuilder request = get("/engine/types/display") //
                .param("version", "1.2") //
                .param("type", "String") //
                .param("id", "id") //
                .param("label", "label") //
                .param("mandatory", "true") //
                .param("description", "The field description") //
                .param("options.min", "4") //
                .param("options.max", "15");

        this.mvc.perform(request) //
                .andExpect(status().isOk());
    }

    /**
     * Test marshalling.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMarshalling() throws Exception {
        final DataElement dataElement = new DataElement();
        dataElement.setId("fld01");
        dataElement.setType("String");
        dataElement.setLabel("Field 01 label");
        dataElement.setDescription("Field 01 description");

        // final byte[] asBytes =
        // JaxbFactoryImpl.instance().asByteArray(dataElement);
        // final String asString = new String(asBytes,
        // StandardCharsets.UTF_8);
        final String asString = JaxbFactoryImpl.instance().fragmentAsString(dataElement);

        assertEquals("<data id=\"fld01\" label=\"Field 01 label\" type=\"String\">\n    <description>Field 01 description</description>\n</data>", asString);
    }

    @Test
    @Ignore
    public void testGenerateAllFeaturesSpecification() throws Exception {
        final Map<String, IValueAdapter<?>> types = Optional.ofNullable(ValueAdapterFactory.getVersionedTypes()) //
                .map(m -> m.get("latest")) //
                .orElseGet(Collections::emptyMap);

        final List<String> typeNames = types.values().stream().map(IValueAdapter::name).sorted().collect(Collectors.toList());

        final WarningElement warningElement = new WarningElement();
        warningElement.setLabel("Warning");
        warningElement.setDescription("No warning, everything is OK !");

        final List<DataElement> dataElements = new ArrayList<>();
        int idx = 1;
        for (final String typeName : typeNames) {
            final IValueAdapter<?> type = types.get(typeName.toLowerCase());
            final DataElement dataElement = new DataElement();
            dataElement.setId(String.format("fld%02d", idx));
            dataElement.setLabel(String.format("Field %02d : %s", idx, type.name()));
            dataElement.setHelp("No help needed !");
            dataElement.setDescription(type.description());
            dataElement.setWarnings(Arrays.asList(warningElement));
            if (type instanceof AbstractReferentialValueAdapter<?>) {
                dataElement.setType(String.format("%s(ref: 'fruits')", type.name()));
            } else {
                dataElement.setType(type.name());
            }
            dataElements.add(dataElement);
            idx += 1;
        }

        final GroupElement groupElement = new GroupElement();
        groupElement.setId("grp01");
        groupElement.setLabel("Group 01");
        groupElement.setData(dataElements);

        final FormSpecificationData spec = new FormSpecificationData();
        spec.setId("step01");
        spec.setLabel("Step 01");
        spec.setGroups(Arrays.asList(groupElement));

        System.out.println(JaxbFactoryImpl.instance().asString(spec));
    }

}
