/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.webui.core.controller.AbstractRecordController;
import fr.ge.common.nash.webui.core.util.ControllerUtil;
import fr.ge.common.nash.webui.service.record.edit.controller.secured.RecordEditingSecuredController;

/**
 * Record fragment controller.
 *
 * @author jpauchet
 */
@Controller
public class RecordFragmentController extends AbstractRecordController {

    @Autowired
    private RecordEditingSecuredController secured;

    @Autowired
    private ServletContext servletContext;

    /**
     * Gets the HTML of a "group" or "data" in order to copy it.
     *
     * @param model
     *            the model
     * @param code
     *            the code of the specification which contains a field to copy
     * @param stepIndex
     *            the step index within the specification
     * @param path
     *            the path of the element to duplicate, within the step
     * @return the HTML
     */
    @RequestMapping(path = "/fragment/**", method = RequestMethod.GET)
    public String fragment(final Model model, @PathVariable("code") final String code, @PathVariable("step") final int stepIndex, //
            @QueryParam("occurrenceIndex") final Integer occurrenceIndex, @QueryParam("occurrencesTotal") final Integer occurrencesTotal, final HttpServletRequest request,
            final HttpServletResponse response) {

        final String fragment = ControllerUtil.translateIdFromHtml(ControllerUtil.extractPathSuffix(request, "/fragment/")).replaceAll("\\[\\]", "[0]");

        return this.secured.displayFragment(model, this.loader(code), fragment, stepIndex, occurrenceIndex, occurrencesTotal, request, response, this.servletContext);
    }

}
