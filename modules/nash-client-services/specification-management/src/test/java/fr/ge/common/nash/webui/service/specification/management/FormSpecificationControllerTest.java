/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.management;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.IOException;
import java.net.URI;
import java.util.Calendar;
import java.util.List;

import javax.servlet.Filter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import fr.ge.common.nash.engine.validation.Error;
import fr.ge.common.nash.engine.validation.Error.Level;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class FormSpecificationControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class FormSpecificationControllerTest extends AbstractTest {

    /** La constante SPEC_UID. */
    private static final String SPEC_UID = "2016-03-ABC-DEF-27";

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService formSpecService;

    @Autowired
    @Qualifier("specificationServiceClientWeb")
    private ISpecificationService formWebSpecService;

    /** controller. */
    @Autowired
    private FormSpecificationController controller;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.formSpecService, this.formWebSpecService);
    }

    @Test
    public void testSearchWithBaseFilters() throws Exception {
        when(this.formSpecService.search(any(long.class), any(long.class), any(), any())).thenReturn(new SpecificationSearchResult(0, 10));

        this.mvc.perform( //
                get("/form/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
                        .param("filters", "author:2017-03-JON-DOE-42") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.formSpecService).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        final List<SearchQueryFilter> filters = filtersCaptor.getValue();

        assertThat(filters, hasSize(2));
        assertThat(filters.get(0), //
                allOf( //
                        hasProperty("column", equalTo("author")), //
                        hasProperty("operator", equalTo(":")), //
                        hasProperty("value", equalTo("2017-03-JON-DOE-42")) //
                ) //
        );
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRemove() throws Exception {
        this.mvc.perform(get("/form/{code}/remove", SPEC_UID)) //
                .andExpect(status().isOk()); //

        verify(this.formSpecService).remove("2016-03-ABC-DEF-27");
    }

    /**
     * Test upload.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUpload() throws Exception {
        final MockHttpServletRequestBuilder request = fileUpload("/form/upload") //
                .file(new MockMultipartFile("file", "Cerfa_XXX", "application/zip", this.resourceAsBytes("upload.zip")));

        when(this.formSpecService.upload(any(byte[].class), any(boolean.class), eq(null), eq("all"), any(boolean.class)))
                .thenReturn(Response.created(URI.create("/v1/Specification/")).entity(new SpecificationInfoBean().setReferenceId(42L)).build());

        this.mvc.perform(request) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/form?reference=42"));

        verify(this.formSpecService).upload(any(byte[].class), any(boolean.class), eq(null), eq("all"), any(boolean.class));
    }

    /**
     * Test upload invalid.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadInvalid() throws Exception {
        final MockHttpServletRequestBuilder request = fileUpload("/form/upload") //
                .file(new MockMultipartFile("file", "Cerfa_XXX", "application/zip", "zip form content".getBytes()));

        final Level errorLevel = Error.Level.FATAL;
        final String errorMessage = "[38,33] La référence à l'entité \"token\" doit se terminer par le délimiteur ';'.";
        final Error error = new Error(errorLevel, errorMessage);
        final Errors errors = new Errors();
        errors.add(error);
        when(this.formSpecService.upload(any(byte[].class), any(boolean.class), eq(null), eq("all"), any(boolean.class))) //
                .thenReturn(Response.status(Status.BAD_REQUEST).entity(errors).build());

        this.mvc.perform(request) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("form/search/main")) //
                .andExpect(model().attributeExists("errors"));
    }

    /**
     * Test upload io exception.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadIOException() throws Exception {
        final String errorMessage = "Oups, an IO exception";

        final MultipartFile file = mock(MultipartFile.class);
        when(file.isEmpty()).thenReturn(Boolean.FALSE);
        when(file.getBytes()).thenThrow(new IOException(errorMessage));

        final Model model = new ExtendedModelMap();
        final String viewName = this.controller.upload(file, model);

        assertThat(viewName, equalTo("form/search/main"));

        final Errors errors = (Errors) model.asMap().get("errors");
        assertThat(errors.getGlobalErrors(), //
                contains( //
                        allOf( //
                                hasProperty("level", equalTo(Error.Level.FATAL)), //
                                hasProperty("message", equalTo(errorMessage)) //
                        ) //
                ) //
        );
    }

    /**
     * Test upload entity error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadEntityError() throws Exception {
        final MockHttpServletRequestBuilder request = fileUpload("/form/upload") //
                .file(new MockMultipartFile("file", "Cerfa_XXX", "application/zip", this.resourceAsBytes("entity.zip")));

        final Level errorLevel = Error.Level.FATAL;
        final String errorMessage = "[38,33] La référence à l'entité \"token\" doit se terminer par le délimiteur ';'.";
        final Error error = new Error(errorLevel, errorMessage);
        final Errors errors = new Errors();
        errors.add(error);
        when(this.formSpecService.upload(any(byte[].class), any(boolean.class), eq(null), eq("all"), any(boolean.class))) //
                .thenReturn(Response.status(Status.BAD_REQUEST).entity(errors).build());

        final MvcResult actual = this.mvc.perform(request) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("form/search/main")) //
                .andExpect(model().attributeExists("errors")) //
                .andReturn();

        final Errors actualErrors = (Errors) actual.getModelAndView().getModel().get("errors");
        assertThat(actualErrors.getGlobalErrors(), //
                contains( //
                        allOf( //
                                hasProperty("level", equalTo(errorLevel)), //
                                hasProperty("message", equalTo(errorMessage)) //
                        ) //
                ) //
        );

    }

    /**
     * Test download unknown from id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknown() throws Exception {
        final MockHttpServletRequestBuilder request = get("/form/{code}/download.zip", SPEC_UID);

        when(this.formSpecService.download(SPEC_UID)).thenReturn(Response.noContent().build());

        this.mvc.perform(request) //
                .andExpect(status().isNoContent());
    }

    /**
     * Test download from id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownload() throws Exception {
        final byte[] asByteArray = "zip form content".getBytes();
        final MockHttpServletRequestBuilder request = get("/form/{code}/download.zip", SPEC_UID);

        when(this.formSpecService.download(SPEC_UID)).thenReturn(Response.ok(asByteArray).header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + SPEC_UID + ".zip").build());

        this.mvc.perform(request) //
                .andExpect(status().isOk()) //
                .andExpect(content().bytes(asByteArray));
    }

    /**
     * Test publish from uid.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPublishFromUid() throws Exception {
        final SpecificationInfoBean bean = buildSpecificationInfoBean();
        bean.setTagged(false);

        when(this.formSpecService.load(SPEC_UID)).thenReturn(bean);

        final byte[] asByteArray = "zip form content".getBytes();
        when(this.formSpecService.download(SPEC_UID)).thenReturn(Response.ok(asByteArray).header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + SPEC_UID + ".zip").build());

        when(this.formWebSpecService.upload(any(String.class), any(byte[].class), any(boolean.class), eq(null), eq("all"), any(boolean.class)))
                .thenReturn(Response.created(URI.create("/v1/Specification/")).entity(new SpecificationInfoBean().setReferenceId(42L)).build());
        this.mvc.perform(get("/form/{code}/publish/", SPEC_UID)) //
                .andExpect(status().isOk());

        bean.setTagged(true);

        verify(this.formSpecService).persist(SPEC_UID, bean);
        // verify(this.formWebSpecService).upload(any(String.class),
        // any(byte[].class),
        // any(boolean.class), eq(null));
    }

    /**
     * Test publish from uid.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUnpublishFromUid() throws Exception {
        final SpecificationInfoBean bean = buildSpecificationInfoBean();
        bean.setTagged(true);

        when(this.formSpecService.load(SPEC_UID)).thenReturn(bean);

        this.mvc.perform(get("/form/{code}/unpublish", SPEC_UID)) //
                .andExpect(status().isOk());

        bean.setTagged(false);

        verify(this.formSpecService).persist(SPEC_UID, bean);
        // verify(this.formWebSpecService).remove(SPEC_UID);
    }

    /**
     * Test view.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testView() throws Exception {
        final byte[] expectedResourceAsBytes = this.resourceAsBytes("upload/description.xml");
        when(this.formSpecService.download(SPEC_UID, "description.xml")).thenReturn( //
                Response.ok(expectedResourceAsBytes) //
                        .header(HttpHeaders.CONTENT_TYPE, "text/xml") //
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=description.xml") //
                        .build() //
        );

        this.mvc.perform(get("/form/{code}/content/description.xml", SPEC_UID)) //
                .andExpect(status().isOk()) //
                .andExpect(content().bytes(expectedResourceAsBytes));
    }

    /**
     * Build specification info bean.
     *
     * @return specification info bean
     */
    private static SpecificationInfoBean buildSpecificationInfoBean() {
        final Calendar now = Calendar.getInstance();

        return new SpecificationInfoBean().setCode(SPEC_UID) //
                .setTitle("Cerfa 14004*02") //
                .setCreated(now) //
                .setUpdated(now) //
                .setTagged(false) //
                .setReference("cerfa14004*02") //
                .setRevision(1L);
    }

    /**
     * Test download template from id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadTemplate() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("template.json");
        final MockHttpServletRequestBuilder request = get("/form/{code}/template", SPEC_UID);

        when(this.formSpecService.templateByCode(SPEC_UID)).thenReturn(Response.ok(resourceAsBytes).header(HttpHeaders.CONTENT_DISPOSITION, "inline; attachment=" + SPEC_UID + ".json").build());

        this.mvc.perform(request) //
                .andExpect(status().isOk()) //
                .andExpect(content().bytes(resourceAsBytes));
    }

    /**
     * Test download unknown template from id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknownTemplate() throws Exception {
        final MockHttpServletRequestBuilder request = get("/form/{code}/template", SPEC_UID);

        when(this.formSpecService.templateByCode(SPEC_UID)).thenReturn(Response.noContent().build());

        this.mvc.perform(request) //
                .andExpect(status().isNoContent());
    }
}
