/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.authentication.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;
import fr.ge.directory.ws.v1.service.IAuthorityService;

/**
 * Get user habilitations from Directory.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class HabilitationService {

    /** Default formalities role. **/
    private static final String DEFAULT_FORMALITIES_ROLE = "all";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HabilitationService.class);

    @Autowired
    @Qualifier("defaultJsonProvider")
    private JacksonJaxbJsonProvider jsonProvider;

    @Value("${ws.directory.url}")
    private String directoryDefaultBaseUrl;

    @Value("${ws.directory.private.url}")
    private String directoryPrivateBaseUrl;

    /**
     * Get the roles for an user.
     *
     * @param userId
     *            the user identifier
     * @return the roles assigned to the user
     */
    @Cacheable(value = "roleByUserId", key = "#userId")
    public Map<String, List<String>> getRoles(final String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }
        // -->Get list of unique user roles. For example, an user could be
        // referent for multiples authorities
        Map<String, List<String>> rights = new HashMap<String, List<String>>();
        try {
            final AuthorityServicesWebService service = JAXRSClientFactory.create(this.directoryPrivateBaseUrl, AuthorityServicesWebService.class, Arrays.asList(this.jsonProvider));
            rights = Optional.ofNullable(service.findUserRights(userId)) //
                    .orElse(new HashMap<String, List<String>>());
        } catch (final Exception e) {
            LOGGER.error("An error occured when calling directory", e);
        }

        // -->Adding default access rights and access rights returning by
        // Directory above
        final Set<String> accessRights = new HashSet<>();
        accessRights.add("*");
        accessRights.add(DEFAULT_FORMALITIES_ROLE);
        if (null != rights.get(userId)) {
            accessRights.addAll(rights.get(userId));
        }
        rights.put(userId, new ArrayList<>(accessRights));

        LOGGER.debug("Rights found for current user : {}", rights);
        return rights;
    }

    @Cacheable(value = "roleForAuthority", key = "{#userId, #entityId, #roles}")
    public boolean hasRoleForAuthority(final String userId, final String authorityFuncId, final List<String> roles) throws TechnicalException {
        ResponseAuthorityRoleBean authorityBean = null;
        try {
            final IAuthorityService service = JAXRSClientFactory.create(this.directoryDefaultBaseUrl, IAuthorityService.class, Arrays.asList(this.jsonProvider));
            authorityBean = Optional.ofNullable(service.findAuthoritiesByUserId(userId)) //
                    .map(ArrayList<ResponseAuthorityRoleBean>::new) //
                    .orElse(new ArrayList<ResponseAuthorityRoleBean>()) //
                    .stream().filter(authority -> (authority.getEntityId().equals(authorityFuncId))).findFirst().orElse(null);
        } catch (final Exception e) {
            throw new TechnicalException("Cannot searching for authorites for a user identifier", e);
        }

        return Optional.ofNullable(authorityBean).map(ResponseAuthorityRoleBean::getRoles) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .anyMatch(authorityRole -> roles.contains(authorityRole));
    }

}
