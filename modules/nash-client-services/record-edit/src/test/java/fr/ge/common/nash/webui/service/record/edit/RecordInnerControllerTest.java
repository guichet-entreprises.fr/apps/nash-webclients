/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.record.edit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Class RecordInnerControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordInnerControllerTest extends AbstractRecordControllerTest {

    /** La constante RECORD_UID_UNKNOWN. */
    private static final String RECORD_UID_UNKNOWN = "2016-12-REC-AAA-42";

    /**
     * Test edit specific page.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditSpecificPageInner() throws Exception {
        final int stepIndex = 1;
        final int page = 1;

        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data.xml")).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/{step}/page/{page}/inner", RECORD_UID, stepIndex, page).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/inner")) //
                .andExpect(MockMvcResultMatchers.model().attribute("code", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.model().attribute("stepIndex", stepIndex)) //
                .andExpect(MockMvcResultMatchers.model().attribute("page", page)) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("spec", "grp"));
    }

    @Test
    public void testEditGroupConditions() throws Exception {
        final int stepIndex = 1;
        final int page = 0;

        when(this.recordService.download(RECORD_UID, "1-data/data.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/1-data/data-conditions.xml")).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/{step}/page/{page}/inner", RECORD_UID, stepIndex, page).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/inner")) //
                .andExpect(MockMvcResultMatchers.model().attribute("code", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.model().attribute("stepIndex", stepIndex)) //
                .andExpect(MockMvcResultMatchers.model().attribute("page", page)) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("spec", "grp"));

    }

    /**
     * Test edit too high page inner.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEditTooHighPageInner() throws Exception {
        final int page = 1;

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/page/{page}/inner", RECORD_UID, page + 10).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/inner")) //
                .andExpect(MockMvcResultMatchers.model().attribute("code", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.model().attribute("stepIndex", 1)) //
                .andExpect(MockMvcResultMatchers.model().attribute("page", page)) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("steps", "spec"));
    }

    /**
     * Test edit unknown record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEditUnknownRecord() throws Exception {
        final int page = 1;

        when(this.recordService.download(RECORD_UID_UNKNOWN, "description.xml")).thenReturn(Response.noContent().build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/page/{page}/inner", RECORD_UID_UNKNOWN, page).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/inner"));
    }

    /**
     * Test edit rest error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEditRestError() throws Exception {
        final int page = 1;

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/1/page/{page}/inner", RECORD_UID_UNKNOWN, page).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/inner"));
    }

    /**
     * Test edit start.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditStartGenerated() throws Exception {
        final String resourcePath = "2-attachment/data-generated.xml";
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/2-attachment/data-generated-empty.xml");

        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/description-attachment.xml")).build());
        when(this.recordService.download(RECORD_UID, resourcePath)).thenReturn(Response.ok(resourceAsBytes).build());

        Arrays.asList("2-attachment/preprocess.xml", "2-attachment/types.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/{step}/page/{page}/inner", RECORD_UID, 1, 0).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/inner")) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("code", "stepIndex", "page"));

        // final ArgumentCaptor<byte[]> captor =
        // ArgumentCaptor.forClass(byte[].class);
        // verify(this.recordService).upload(eq(RECORD_UID),
        // eq("2-attachment/data-generated.xml"), captor.capture());

        // assertEquals(new String(resourceAsBytes, StandardCharsets.UTF_8), new
        // String(captor.getValue(), StandardCharsets.UTF_8));
    }

    @Test
    public void testPreprocessError() throws Exception {
        reset(this.recordService);
        when(this.recordService.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(this.resourceAsBytes("script-error/description.xml")).build());
        when(this.recordService.download(RECORD_UID, "preprocess.xml")).thenReturn(Response.ok(this.resourceAsBytes("script-error/preprocess.xml")).build());
        when(this.recordService.upload(eq(RECORD_UID), any(String.class), any(byte[].class))).thenReturn(Response.ok().lastModified(Calendar.getInstance().getTime()).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/0/page/{page}/inner", RECORD_UID, 0).header("X-Requested-With", "XMLHttpRequest")) //
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}
