/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.core.advice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.RecordStepDataNotFoundException;
import fr.ge.common.nash.core.exception.RecordStepNotFoundException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;

/**
 * Handle every exceptions occuring in the admin web app.
 *
 * @author atuffrea
 *
 */
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class GenericErrorHandler {

    private static final String INNER_SUFFIX = "/inner";

    /** The Constant UUID_PARAMETER. */
    private static final String UUID_PARAMETER = "uuid";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericErrorHandler.class);

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    @ExceptionHandler(UnauthorizedAccessException.class)
    public ModelAndView unauthorizedAccessHandler(final HttpServletRequest request, final HttpServletResponse response, final UnauthorizedAccessException exception) {
        LOGGER.error("Unauthorized access : {}", exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/access" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    @ExceptionHandler(RecordNotFoundException.class)
    public ModelAndView notFoundHandler(final HttpServletRequest request, final RecordNotFoundException exception) {
        LOGGER.error("Record not found : {}", exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/notfound" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    @ExceptionHandler(RecordStepNotFoundException.class)
    public ModelAndView stepNotFoundHandler(final HttpServletRequest request, final RecordStepNotFoundException exception) {
        LOGGER.error("Record step #{} not found : {}", exception.getStepIndex(), exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/step-notfound" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    @ExceptionHandler(RecordStepDataNotFoundException.class)
    public ModelAndView stepDataNotFoundHandler(final HttpServletRequest request, final RecordStepDataNotFoundException exception) {
        LOGGER.error("Record step #{} data not found : {}", exception.getStepIndex(), exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/step-data-notfound" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

    /**
     * Handle every exceptions.
     *
     * @param response
     *            the http reponse to send
     * @param exception
     *            the occuring exception
     * @return ModelAndView
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ModelAndView defaultHandler(final HttpServletRequest request, final Exception exception) {
        LOGGER.error("Unexpected error : {}", exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/default" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

    /**
     * Handle every exceptions.
     *
     * @param response
     *            the http reponse to send
     * @param exception
     *            the occuring exception
     * @return ModelAndView
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(TemporaryException.class)
    public ModelAndView temporaryHandler(final HttpServletRequest request, final HttpServletResponse response, final TemporaryException exception) {
        LOGGER.error("Temporary error : {}", exception.getMessage(), exception);

        final ModelAndView modelAndView = new ModelAndView("commons/error/retry" + (request.getRequestURI().endsWith(INNER_SUFFIX) ? INNER_SUFFIX : "/main"));
        modelAndView.addObject(UUID_PARAMETER, MDC.get("correlationId"));
        modelAndView.addObject("error", exception);

        return modelAndView;
    }

}
