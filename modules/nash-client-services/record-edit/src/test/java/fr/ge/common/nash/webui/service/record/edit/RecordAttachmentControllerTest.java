/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Calendar;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class RecordAttachmentControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RecordAttachmentControllerTest extends AbstractTest {

    /** La constante DEFAULT_CHARSET. */
    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /** The Constant ATTACHMENT_FILE_CONTENT. */
    private static final byte[] ATTACHMENT_FILE_CONTENT = "xxx".getBytes(DEFAULT_CHARSET);

    /** The Constant ATTACHMENT_FILE_TYPE. */
    private static final String ATTACHMENT_FILE_TYPE = "application/pdf";

    /** The Constant ATTACHMENT_FILE_NAME. */
    private static final String ATTACHMENT_FILE_NAME = "cni user.pdf";

    /** The Constant RECORD_UID. */
    private static final String RECORD_UID = "2016-03-AAA-AAA-27";

    /** The mvc. */
    private MockMvc mvc;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        AccountContext.currentUser(null);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.recordService);
    }

    /**
     * Test edit not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditNotFound() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().isNotFound()) //
                .andExpect(MockMvcResultMatchers.view().name("commons/error/notfound/main"));
    }

    /**
     * Test init.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testInit() throws Exception {
        this.mockSimple();

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2", RECORD_UID)) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.redirectedUrl(String.format("/record/%s/2/page/%d", RECORD_UID, 0)));
    }

    /**
     * Test edit start.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditStart() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2/page/{page}", RECORD_UID, 0)) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("record/edit/main")) //
                .andExpect(MockMvcResultMatchers.model().attributeExists("code", "stepIndex", "page"));

        verify(this.recordService).download(eq(RECORD_UID), eq("description.xml"));
    }

    /**
     * Test upload.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUpload() throws Exception {
        final String resourcePath = String.format("2-attachment/uploaded/%s-%s-%s", "attachment.identityMain", 3, ATTACHMENT_FILE_NAME);
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/{step}/upload/{resource}", RECORD_UID, 2, "attachment_identityMain") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, ATTACHMENT_FILE_CONTENT));

        this.mockGenerated();

        final MvcResult actual = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andReturn();

        assertEquals(
                "{\"type\":\"SUCCESS\",\"title\":\"Attachment cni user.pdf saved\",\"details\":{\"id\":\"attachment_identityMain\",\"attachments\":[{\"id\":1,\"label\":\"file01.png\"},{\"id\":2,\"label\":\"file02.png\"},{\"id\":3,\"label\":\"cni user.pdf\"}]}}",
                actual.getResponse().getContentAsString());

        verify(this.recordService).upload(eq(RECORD_UID), eq(resourcePath), eq(ATTACHMENT_FILE_CONTENT));
    }

    /**
     * Test upload no file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadNoFile() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/{step}/upload/{resource}", RECORD_UID, 2, "file01") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, new byte[] {}));

        this.mockGenerated();

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    /**
     * Test upload empty file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadEmptyFile() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/2/upload/{path}", RECORD_UID, "file01") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, (byte[]) null));

        this.mockSimple();

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isBadRequest()) //
                .andExpect(MockMvcResultMatchers.content().string("{\"type\":\"ERROR\",\"title\":\"No attachment to save\",\"details\":null,\"args\":null}"));
    }

    /**
     * Test upload unknown.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadUnknown() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/2/upload/{path}", RECORD_UID, "file99") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, ATTACHMENT_FILE_CONTENT));

        this.mockGenerated();

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isBadRequest()) //
                .andExpect(MockMvcResultMatchers.content()
                        .string("{\"type\":\"ERROR\",\"title\":\"No corresponding attachment specification\",\"details\":{\"id\":\"file99\",\"attachments\":[]},\"args\":null}"));
    }

    /**
     * Test reorder.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testReorder() throws Exception {
        final byte[] expectedResourceAsBytes = this.resourceAsBytes("spec/2-attachment/data-reordered.xml");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/record/{code}/2/reorder/{field}", RECORD_UID, "attachment_identityMain") //
                .param("values", "2", "1");

        this.mockGenerated();

        this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.content().string(
                        "{\"type\":\"SUCCESS\",\"title\":\"Attachments reordered on [attachment_identityMain]\",\"details\":{\"id\":\"attachment_identityMain\",\"attachments\":[{\"id\":2,\"label\":\"file02.png\"},{\"id\":1,\"label\":\"file01.png\"}]}}"));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.recordService).upload(eq(RECORD_UID), eq("2-attachment/data-generated.xml"), captor.capture());

        assertEquals(new String(expectedResourceAsBytes, DEFAULT_CHARSET).replaceAll("[\r\n]+", " "), new String(captor.getValue(), DEFAULT_CHARSET).replaceAll("[\r\n]+", " "));
    }

    /**
     * Test empty.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEmpty() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.post("/record/{code}/2/empty/{field}", RECORD_UID, "attachment_identityMain")) //
                .andExpect(MockMvcResultMatchers.status().isOk()); //
        // .andExpect(MockMvcResultMatchers.content()
        // .string("{\"type\":\"SUCCESS\",\"title\":\"2 attachments removed on
        // [identityMain]\",\"details\":{\"id\":\"identityMain\",\"attachments\":[]}}"));

        verify(this.recordService, times(2)).remove(any(), any());
        verify(this.recordService).remove(eq(RECORD_UID), eq(String.format("2-attachment/uploaded/%s-%s-%s", "attachment.identityMain", 1, "file01.png")));
        verify(this.recordService).remove(eq(RECORD_UID), eq(String.format("2-attachment/uploaded/%s-%s-%s", "attachment.identityMain", 2, "file02.png")));
    }

    /**
     * Test empty unknown.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEmptyUnknown() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.post("/record/{code}/2/empty/{field}", RECORD_UID, "file99")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.content().string("{\"type\":\"ERROR\",\"title\":\"No corresponding attachment specification\",\"details\":null,\"args\":null}"));
    }

    /**
     * Test download.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownload() throws Exception {
        this.mockGenerated();

        when(this.recordService.download(RECORD_UID, String.format("2-attachment/uploaded/%s-%d-%s", "attachment.identityMain", 0, "file01.png"))) //
                .thenReturn(Response.ok(ATTACHMENT_FILE_CONTENT).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2/download/{field}/{idx}/{name}", RECORD_UID, "attachment_identityMain", 0, "file01.png")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, "inline;  filename=file01.png")) //
                .andExpect(MockMvcResultMatchers.content().bytes(ATTACHMENT_FILE_CONTENT));
    }

    /**
     * Test download unknown attachment.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknownAttachment() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2/download/{field}/{idx}/{name}", RECORD_UID, "file99", 0, ATTACHMENT_FILE_NAME)) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test download unknown file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknownId() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2/download/{field}/{idx}/{name}", RECORD_UID, "file01", 0, "unknown.pdf")) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test download unknown file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownloadUnknownFile() throws Exception {
        this.mockGenerated();

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/2/download/{field}/{idx}/{name}", RECORD_UID, "attachment_identityMain", 0, "unknown.pdf")) //
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Test download other step.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadOtherStep() throws Exception {
        this.mockReview();

        when(this.recordService.download(RECORD_UID, String.format("3-review/generated/%s-%d-%s", "generated.cerfa14004", 1, "cerfa14004-02.pdf"))) //
                .thenReturn(Response.ok(ATTACHMENT_FILE_CONTENT).build());

        this.mvc.perform(MockMvcRequestBuilders.get("/record/{code}/3/download/{field}/{idx}/{name}", RECORD_UID, "generated_cerfa14004", 1, "cerfa14004-02.pdf")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, "inline;  filename=cerfa14004-02.pdf")) //
                .andExpect(MockMvcResultMatchers.content().bytes(ATTACHMENT_FILE_CONTENT));

    }

    /**
     * Mock simple.
     */
    protected void mockSimple() {
        when(this.recordService.load(RECORD_UID)).thenReturn(new RecordInfoBean().setCode(RECORD_UID));
        when(this.recordService.upload(eq(RECORD_UID), any(String.class), any(byte[].class))).thenReturn(Response.ok().lastModified(Calendar.getInstance().getTime()).build());

        Arrays.asList("description.xml", "0-context/context.xml", "1-data/data.xml", "2-attachment/preprocess.xml", "2-attachment/types.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));
    }

    /**
     * Mock generated.
     */
    protected void mockGenerated() {
        this.mockSimple();

        Arrays.asList("2-attachment/data-generated.xml") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));
    }

    /**
     * Mock review.
     */
    protected void mockReview() {
        this.mockGenerated();

        Arrays.asList("3-review/preprocess.xml", "3-review/data-generated.xml", "3-review/preprocess.js", "3-review/models/cerfa14004-02.pdf") //
                .forEach(s -> when(this.recordService.download(RECORD_UID, s)).thenReturn(Response.ok(this.resourceAsBytes("spec/" + s)).build()));
    }

    /**
     * Test upload with split limit.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadSplitLimit() throws Exception {
        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/{step}/upload/{resource}", RECORD_UID, 2, "attachment_identityMain") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, ATTACHMENT_FILE_CONTENT));

        this.mockSimple();

        when(this.recordService.download(RECORD_UID, "2-attachment/data-generated.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/2-attachment/data-generated-split.xml")).build());

        final MvcResult actual = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is4xxClientError()) //
                .andReturn();

        assertEquals("{\"type\":\"ERROR\",\"title\":\"{0} documents allowed\",\"details\":{\"id\":\"attachment_identityMain\",\"attachments\":[]},\"args\":[2]}",
                actual.getResponse().getContentAsString());

        final String resourcePath = String.format("2-attachment/uploaded/%s-%s-%s", "attachment.identityMain", 3, ATTACHMENT_FILE_NAME);
        verify(this.recordService, never()).upload(eq(RECORD_UID), eq(resourcePath), eq(ATTACHMENT_FILE_CONTENT));
    }

    /**
     * Test upload with size limit.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUploadSizeLimit() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("spec/files/image-2Mo.pdf");

        final MockHttpServletRequestBuilder request = MockMvcRequestBuilders.fileUpload("/record/{code}/{step}/upload/{resource}", RECORD_UID, 2, "attachment_identityMain") //
                .file(new MockMultipartFile("attachment", ATTACHMENT_FILE_NAME, ATTACHMENT_FILE_TYPE, resourceAsBytes));

        this.mockSimple();

        when(this.recordService.download(RECORD_UID, "2-attachment/data-generated.xml")).thenReturn(Response.ok(this.resourceAsBytes("spec/2-attachment/data-generated-size.xml")).build());

        final MvcResult actual = this.mvc.perform(request) //
                .andExpect(MockMvcResultMatchers.status().is4xxClientError()) //
                .andReturn();

        assertEquals("{\"type\":\"ERROR\",\"title\":\"Maximum allowed size : {0} Mo\",\"details\":{\"id\":\"attachment_identityMain\",\"attachments\":[]},\"args\":[1]}",
                actual.getResponse().getContentAsString());

        final String resourcePath = String.format("2-attachment/uploaded/%s-%s-%s", "attachment.identityMain", 3, ATTACHMENT_FILE_NAME);
        verify(this.recordService, never()).upload(eq(RECORD_UID), eq(resourcePath), eq(ATTACHMENT_FILE_CONTENT));
    }
}
