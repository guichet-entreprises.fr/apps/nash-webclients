/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.filter;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.authentication.manager.AccountManager;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-rest-services-context.xml", "classpath:spring/internal-components-loading-context.xml" })
public class AclInterceptorTest extends AbstractRestTest {

    private IRestService restService;

    @Autowired
    private HabilitationService habilitationService;

    @Autowired
    private AccountManager accountManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        this.restService = mock(IRestService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.restService);
        serverFactory.setProvider(jsonProvider);

        this.server = serverFactory.create();

        this.providers = Arrays.asList(jsonProvider);
    }

    @Test
    public void testHeader() throws Exception {
        final String userId = "2018-12-JON-DOE-42";
        final String userEntity = "A/B/2018-12-ENTITY_ONE";
        final Map<String, List<String>> userRoles = Collections.singletonMap(userEntity, Arrays.asList("ROL1", "ROL2"));

        when(this.habilitationService.getRoles(userId)).thenReturn(userRoles);
        when(this.restService.ask(any())).then(invocation -> {
            final Map<String, List<String>> roles = new ObjectMapper().readValue((String) invocation.getArgument(0), new TypeReference<Map<String, List<String>>>() {
            });

            assertThat(roles.entrySet(), hasSize(2));
            assertThat(roles.get(userEntity), contains(userRoles.get(userEntity).toArray()));

            return Response.ok().build();
        });

        doNothing().when(accountManager).persistUserContext(any());
        AccountContext.currentUser(new LocalUserBean(userId).setFirstname("John").setLastname("Doe"));
        final Response response = this.client().path("/ask").get();
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return null;
    }

    public static interface IRestService {

        @Path("/ask")
        @GET
        Response ask(@HeaderParam("X-Roles") String roles);

    }

}
