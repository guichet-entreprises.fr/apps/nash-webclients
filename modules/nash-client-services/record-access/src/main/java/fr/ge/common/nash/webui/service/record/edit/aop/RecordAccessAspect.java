/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.aop;

import java.util.Arrays;
import java.util.Optional;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.authentication.user.HabilitationService;

/**
 *
 * @author Christian Cougourdan
 */
@Aspect
public class RecordAccessAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordAccessAspect.class);

    /** Habilitations service. **/
    @Autowired
    private HabilitationService habilitationService;

    /** Checking user roles from directory **/
    @Value("${right.record.edition.check.enabled:false}")
    private boolean checkUserRolesForAuthority;

    @Around("execution(public * fr.ge.common.nash.webui.service.record.edit.controller.secured..*.*(..))")
    public Object invoke(final ProceedingJoinPoint joinPoint) throws Throwable {
        final SpecificationLoader loader = Arrays.stream(joinPoint.getArgs()).filter(SpecificationLoader.class::isInstance).findFirst().map(SpecificationLoader.class::cast).orElse(null);
        if (null != loader) {
            final FormSpecificationDescription description = loader.description();
            if (null == description) {
                throw new RecordNotFoundException();
            }

            final String ownerUid = description.getAuthor();
            final String userUid = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);

            if (this.isIdentified(ownerUid)) {
                return joinPoint.proceed();
            }

            // --> Throwing a functionnal exception if the user does
            // not contain any role such as "referent" or "user" for
            // the corresponding authority
            if (this.checkUserRolesForAuthority) {
                LOGGER.debug("The requested record is assigned to {} as an authority. Now checking rights to edit that record", ownerUid);
                if (!this.habilitationService.hasRoleForAuthority(userUid, ownerUid, Arrays.asList("user", "referent"))) {
                    throw new UnauthorizedAccessException("The user identifier has not any role identified to edit the requested record.");
                }
            } else {
                throw new UnauthorizedAccessException("The user identifier is not allowed to edit the requested record.");
            }
        }

        return joinPoint.proceed();
    }

    /**
     * Check if authenticated user and record owner match.
     *
     * @param ownerId
     *            record owner
     * @return true if match
     */
    private boolean isIdentified(final String ownerId) {
        final String userUid = Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null);

        if (null == userUid) {
            LOGGER.warn("Special case, user not identified has all access");
            return true;
        } else if (null == ownerId) {
            LOGGER.info("Record has no author registered (???)");
            return false;
        } else if (ownerId.equals(userUid)) {
            LOGGER.debug("The record author matchs the current user identifier");
            return true;
        } else {
            LOGGER.info("WARN : user {} is not equals to record's owner.", userUid);
            return false;
        }
    }

}
