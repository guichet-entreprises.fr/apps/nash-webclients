/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.webui.service.markov.monitoring.rest.v1.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;

/**
 * @author Christian Cougourdan
 *
 */
public class MarkovPassthroughRestServiceImpl implements IQueueMessageRestService {

    @Autowired
    @Qualifier("queueMessageRestService")
    private IQueueMessageRestService service;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(final String uid) {
        return this.service.download(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(final long id) {
        return this.service.download(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<ResponseQueueMessageBean> search(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        return this.service.search(startIndex, maxResults, filters, orders);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response delete(final long id) {
        return this.service.delete(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response upload(final String queueCode, final boolean overwritePreviousOne, final Attachment attachment) {
        return this.service.upload(queueCode, overwritePreviousOne, attachment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response move(final long id, final String dstQueueCode) {
        return this.service.move(id, dstQueueCode);
    }
}
