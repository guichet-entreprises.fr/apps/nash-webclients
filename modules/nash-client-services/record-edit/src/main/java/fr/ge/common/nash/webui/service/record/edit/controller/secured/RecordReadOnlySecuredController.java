/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.record.edit.controller.secured;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.webui.core.locale.CookieThenAcceptHeaderLocaleResolver;
import fr.ge.common.nash.webui.service.record.edit.WebUiConstants;

/**
 * Record Read-only controller. Allow to check for external user permissions.
 *
 * @author Ademola Olubi
 */
@Component
public class RecordReadOnlySecuredController {

    /** The custom local resolver. **/
    @Autowired
    private CookieThenAcceptHeaderLocaleResolver localeResolver;

    /** The engine user type. */
    @Value("${engine.user.type}")
    private String engineUserType;

    /**
     * Redirect to first step.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param request
     *            the request
     * @param response
     *            the response
     * @param locale
     *            the locale
     * @return the string
     */
    public String redirectToFirstStep(final Model model, final SpecificationLoader loader, final HttpServletRequest request, final HttpServletResponse response, final Locale locale) {
        final FormSpecificationDescription description = loader.description();

        if (null == description) {
            throw new RecordNotFoundException();
        } else {
            this.localeResolver.resolve(loader, request, response, locale);
            model.addAttribute(WebUiConstants.ATTR_CODE, description.getRecordUid());
            model.addAttribute("step", 0);
            return "redirect:/record/{code}/{step}/page/0";
        }
    }

    /**
     * Display page from index.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @param page
     *            the page
     * @return the string
     */
    public String displayPageFromIndex(final Model model, final SpecificationLoader loader, final int stepIndex, final int page) {
        final StepElement currentStep = loader.stepsMgr().current();
        final List<StepElement> steps = loader.stepsMgr().findAll();
        final int maxStepIndex = steps.size() - 1;

        /*
         *
         * If specified step index is greater than max step index, redirect to the first
         * step
         *
         */
        if (stepIndex > maxStepIndex) {
            model.addAttribute(WebUiConstants.ATTR_STEP, 0);
            model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
            return "redirect:/record/{code}/{step}/page/{page}";
        }

        /*
         * If specified step index is greater than error step index, redirect to the
         * first step too
         */
        if (loader.hasErrorState()) {
            final int stepErrorIndex = loader.stepsMgr().findFirst(s -> s.getStatus().equals(StepStatusEnum.ERROR.getStatus())).getPosition();
            if (stepIndex > stepErrorIndex) {
                model.addAttribute(WebUiConstants.ATTR_STEP, 0);
                model.addAttribute(WebUiConstants.ATTR_PAGE, 0);
                return "redirect:/record/{code}/{step}/page/{page}";
            }
        }

        /*
         *
         * If specified step index is the last one and its status is "done",
         *
         * display validation page
         *
         */
        final StepElement stepElement = loader.stepsMgr().find(stepIndex);

        final String validatedStep = stepElement.getStatus();
        if (stepIndex == maxStepIndex && StepStatusEnum.DONE.getStatus().equals(validatedStep) || !this.engineUserType.equals(currentStep.getUser())) {
            return "record/validation/main";
        }

        final int realPage = loader.group(stepIndex, page);
        if (realPage != page) {
            model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);
            model.addAttribute(WebUiConstants.ATTR_PAGE, realPage);
            return "redirect:/record/{code}/{step}/page/{page}";
        }

        model.addAttribute(WebUiConstants.ATTR_PAGE, page);
        model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
        model.addAttribute(WebUiConstants.ATTR_STEP_INDEX, stepIndex);
        return "record/edit/main";
    }

    /**
     * Save.
     *
     * @param model
     *            the model
     * @param loader
     *            the loader
     * @param stepIndex
     *            the step index
     * @param page
     *            the page
     * @param success
     *            the success
     * @param request
     *            the request
     * @return the string
     * @throws TemporaryException
     *             the temporary exception
     */
    public String next(final Model model, final SpecificationLoader loader, final int stepIndex, final Integer page, final String success, final HttpServletRequest request) throws TemporaryException {
        final StepElement stepToUpdate = loader.stepsMgr().find(stepIndex);

        final FormSpecificationData spec = loader.data(stepToUpdate);
        if (StepStatusEnum.ERROR.getStatus().equals(stepToUpdate.getStatus())) {
            model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
            model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);

            if (spec.getGroups() != null && page + 1 < spec.getGroups().size() && "next".equals(success)) {
                model.addAttribute(WebUiConstants.ATTR_PAGE, page + 1);
                return "redirect:/record/{code}/{step}/page/{page}";
            }
            return "redirect:/record/{code}/{step}/page/0";
        }

        String view;
        if (spec.getGroups() != null && page + 1 < spec.getGroups().size() && "next".equals(success)) {
            model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
            model.addAttribute(WebUiConstants.ATTR_STEP, stepIndex);
            model.addAttribute(WebUiConstants.ATTR_PAGE, page + 1);
            view = "redirect:/record/{code}/{step}/page/{page}";
        } else if ("validate".equals(success)) {
            view = this.nextStep(model, loader, stepIndex);
        } else {
            view = "forward:/dashboard";
        }

        return view;
    }

    private String nextStep(final Model model, final SpecificationLoader loader, final int currentStepIndex) {
        final StepElement nextStep = loader.stepsMgr().find(currentStepIndex + 1);
        if (null == nextStep || !this.engineUserType.equals(nextStep.getUser())) {
            return "forward:/dashboard";
        } else {
            model.addAttribute(WebUiConstants.ATTR_CODE, loader.description().getRecordUid());
            model.addAttribute(WebUiConstants.ATTR_STEP, nextStep.getPosition());
            return "redirect:/record/{code}/{step}";
        }
    }

    /**
     * Sets the engine user type.
     *
     * @param engineUserType
     *            the engineUserType to set
     */
    public void setEngineUserType(final String engineUserType) {
        this.engineUserType = engineUserType;
    }

}
