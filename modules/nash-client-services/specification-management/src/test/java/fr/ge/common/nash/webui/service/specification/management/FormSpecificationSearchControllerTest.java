/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.management;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class FormSpecificationSearchControllerTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService formSpecService;

    /** slf4j context filter. */
    @Autowired
    protected Filter slf4jContextFilter;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .addFilter(this.slf4jContextFilter, "/*") //
                .build();
        reset(this.formSpecService);
    }

    @Test
    public void testSearchMain() throws Exception {
        this.mvc.perform(get("/form")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("form/search/main"));
    }

    @Test
    public void testSearchWithBaseFilters() throws Exception {
        when(this.formSpecService.search(any(long.class), any(long.class), any(), any())).thenReturn(new SpecificationSearchResult(0, 10));

        this.mvc.perform( //
                get("/form/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
                        .param("filters", "author:2017-03-JON-DOE-42") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.formSpecService).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        final List<SearchQueryFilter> filters = filtersCaptor.getValue();

        assertThat(filters, hasSize(2));
        assertThat(filters.get(0), //
                allOf( //
                        hasProperty("column", equalTo("author")), //
                        hasProperty("operator", equalTo(":")), //
                        hasProperty("value", equalTo("2017-03-JON-DOE-42")) //
                ) //
        );
    }

}
