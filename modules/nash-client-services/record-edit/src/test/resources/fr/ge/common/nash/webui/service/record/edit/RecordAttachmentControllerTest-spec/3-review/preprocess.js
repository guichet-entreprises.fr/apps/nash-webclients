var cerfa = pdf.create('models/cerfa14004-02.pdf', $cerfa14004.notifierIdentification);

var cerfaPdf = pdf.save('cerfa14004-02.pdf', cerfa);

return form.create({
    id : 'review',
    label : 'Review',
    groups : [ group.create({
        id : 'generated',
        label : 'Formulaires générés',
        data : [ data.create({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
