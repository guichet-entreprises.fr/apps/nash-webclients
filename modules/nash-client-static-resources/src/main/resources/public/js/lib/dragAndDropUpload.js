/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

define([ 'jquery', 'bootstrap', ], function ($, bootstrap) {

    var uploadNotificationContainer;

    function sendFileToServer(uploadUrl, formData, status){

        var extraData ={};
        var jqXHR=$.ajax({
            xhr: function() {
                var xhrobj = $.ajaxSettings.xhr();
                if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }

                        console.debug(percent, position, total, event.lengthComputable);
                        status.setProgress(percent);
                    }, false);
                }
                return xhrobj;
            },
            url: uploadUrl,
            type: "POST",
            contentType:false,
            processData: false,
            cache: false,
            data: formData,
            success: function(data){
                status.setProgress(100);
     
                $("#status1").append("File upload Done<br>");
            }
        }); 
     
        status.setAbort(jqXHR);
    }

    var ProgressBar = (function () {

        var ProgressBar = function (parent) {
            this.container = parent;
            this.root = $('<div class="statusbar"></div>');
            this.label = $('<div class="filename"></div>').appendTo(this.root);
            this.progressBar = $('<progress class="progressUploadBar" value="0" max="100"></progress>').appendTo(this.root);
            this.btnAbort = $('<i class="fa fa-times-circle" aria-hidden="true"></i>').appendTo(this.root);

            this.container.append(this.root).show();
        };

        ProgressBar.prototype.setFileNameSize = function (name, size) {
            var sizeStr = '';
            var reducedSize = size;
            if (reducedSize < 1024) {
                sizeStr = reducedSize.toFixed(0) + 'B';
            } else if ((reducedSize = reducedSize / 1024) < 1024) {
                sizeStr = reducedSize.toFixed(0) + 'KB';
            } else {
                sizeStr = (reducedSize / 1024).toFixed(0) + 'MB';
            }

            this.label.html(name + ' (' + sizeStr + ')');
        };

        ProgressBar.prototype.setProgress = function (progress) {
            this.progressBar.attr('value', progress).html(progress + '%');

            if (parseInt(progress) >= 100) {
                this.root.remove();
                if (0 == $('> .statusbar', this.container).size()) {
                    this.container.hide();
                }
            }
        };

        ProgressBar.prototype.setAbort = function (jqXHR) {
            var sb = this.root;
            this.btnAbort.on('click', function () {
                jqxhr.abort();
                sb.hide();
            });
        };

        return ProgressBar;
    })();

    function initGlobalEvents() {
        $(document).on('dragenter.dragAndDropUpload', function (evt) {
            evt.stopPropagation();
            evt.preventDefault();
        }).on('drop.dragAndDropUpload', function (evt) {
            evt.stopPropagation();
            evt.preventDefault();
        });
    }

    function handleFileUpload(files, elm) {
        if (files.length > 0 && !uploadNotificationContainer) {
            uploadNotificationContainer = $('<div id="upload-notifications"></div>').appendTo('body');
        }

        for (var i = 0; i < files.length; i++) {
            var fd = new FormData();
            fd.append('file', files[i]);

            var status = new ProgressBar(uploadNotificationContainer);
            status.setFileNameSize(files[i].name, files[i].size);
            sendFileToServer(elm.data('opts').url, fd, status);
        }
    }

    function init(opts) {
        if (!opts || !opts.url) {
            console.warn('No service url specified');
            return this;
        }

        initGlobalEvents();

        return this.each(function () {
            var self = $(this);
            var uploadArea = $('<div id="upload-area"></div>').append('Déposez les fichiers ici').hide().insertAfter(self);
            self.data('opts', opts) //
            .on('dragenter', function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
                uploadArea.show();
            });
            
            uploadArea.on('dragenter', function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
            }).on('dragover', function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
            }).on('dragleave', function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
                uploadArea.hide();
            }).on('drop', function (evt) {
                evt.preventDefault();

                var files = evt.originalEvent.dataTransfer.files;

                handleFileUpload(files, self);
                uploadArea.hide();
            });
        });
    }

    $.fn.dragAndDropUpload = init;

});
