/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.webui.service.specification.management;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXParseException;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.engine.validation.Error;
import fr.ge.common.nash.engine.validation.Error.Level;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;
import fr.ge.common.nash.webui.core.bean.ErrorMessage;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class FormSpecificationController.
 *
 * @author Christian Cougourdan
 */
@Controller
public class FormSpecificationController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormSpecificationController.class);

    /** No corresponding attachment specification. */
    private static final String ERROR_FORM_WEB_PUBLISHING = "Error when publishing form,Form-Web service is temporarily unavailable";

    /** No corresponding attachment specification. */
    private static final String ERROR_FORM_WEB_UNPUBLISHING = "Error when unpublishing form,Form-Web service is temporarily unavailable";

    /** No corresponding attachment specification. */
    private static final String ERROR_FORM_WEB_REMOVE = "Error when removing form,Form-Web service is temporarily unavailable";

    /** The form spec service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService formSpecService;

    @Value("${ws.nash.web.url:}")
    private String externalServiceUri;

    /**
     * View.
     *
     * @param code
     *            the code
     * @param request
     *            the request
     * @return the response entity
     */
    @RequestMapping(value = "/form/{code}/content/**", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> view(@PathVariable("code") final String code, final HttpServletRequest request) {
        final String requestPath = CoreUtil.coalesce(request.getPathInfo(), request.getServletPath());
        final String resourcePath = requestPath.replaceFirst("/form/[^/]+/content/", "");

        final Response response = this.formSpecService.download(code, resourcePath);
        final byte[] sourceContent = response.readEntity(byte[].class);

        return ResponseEntity.ok(sourceContent);
    }

    /**
     * Download.
     *
     * @param code
     *            the code
     * @return the response entity
     */
    @RequestMapping(value = "/form/{code}/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable("code") final String code) {
        final Response response = this.formSpecService.download(code);

        if (Status.NO_CONTENT.getStatusCode() == response.getStatus()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION));
        return new ResponseEntity<>(response.readEntity(byte[].class), headers, HttpStatus.OK);

    }

    /**
     * Removes the.
     *
     * @param code
     *            the code
     * @return the string
     */
    @RequestMapping(value = "/form/{code}/remove", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Object> remove(@PathVariable("code") final String code) {

        final SpecificationInfoBean beanForge = this.formSpecService.load(code);
        if (null != beanForge && beanForge.isTagged()) {
            try {
                this.externalService().ifPresent(service -> service.remove(code));
            } catch (NotFoundException | ProcessingException | Fault e) {
                LOGGER.error(MessageFormat.format("Error calling form Web with code \"{0}\": {1}", code, e.getMessage()));
                return new ResponseEntity<>(new ErrorMessage<>(ERROR_FORM_WEB_REMOVE, HttpStatus.SERVICE_UNAVAILABLE), HttpStatus.SERVICE_UNAVAILABLE);
            }
        }
        this.formSpecService.remove(code);

        return ResponseEntity.ok("ok");
    }

    /**
     * Upload.
     *
     * @param file
     *            the file
     * @param model
     *            the model
     * @return the string
     * @throws FunctionalException
     *             the functional exception
     */
    @RequestMapping(value = "/form/upload", method = RequestMethod.POST)
    public String upload(@RequestParam("file") final MultipartFile file, final Model model) throws FunctionalException {
        Errors errors = new Errors();
        if (!file.isEmpty()) {
            try {
                final byte[] resourceAsBytes = file.getBytes();
                final Response response = this.formSpecService.upload(resourceAsBytes, false, Optional.ofNullable(AccountContext.currentUser()).map(LocalUserBean::getId).orElse(null), "all", false);
                if (response.getStatus() == Status.CREATED.getStatusCode()) {
                    final SpecificationInfoBean bean = response.readEntity(SpecificationInfoBean.class);
                    model.addAttribute("reference", bean.getReferenceId());
                } else if (response.getStatus() == Status.BAD_REQUEST.getStatusCode()) {
                    LOGGER.warn("Specification file is invalid.");
                    errors = response.readEntity(Errors.class);
                }
            } catch (final IOException ex) {
                errors.add(new Error(Level.FATAL, ex.getMessage()));
                LOGGER.trace(StringUtils.EMPTY, ex);
            } catch (final TechnicalException ex) {
                this.handleUploadError(errors, ex);
            }
        }

        if (null != errors && errors.hasErrors()) {
            model.addAttribute("errors", errors);
            return "form/search/main";
        } else {
            return "redirect:/form";
        }
    }

    /**
     * Handle upload error.
     *
     * @param errors
     *            the errors
     * @param ex
     *            the ex
     */
    private void handleUploadError(final Errors errors, final TechnicalException ex) {
        final SAXParseException saxException = CoreUtil.findException(ex, SAXParseException.class);
        if (null == saxException) {
            errors.add(new Error(Level.FATAL, "Unexpected error"));
            LOGGER.warn("Specification file validation error : unexpected error", ex);
        } else {
            final String msg = String.format("[%d,%d] %s", saxException.getLineNumber(), saxException.getColumnNumber(), saxException.getMessage());
            errors.add(new Error(Level.FATAL, msg));
            LOGGER.warn("Specification file validation error : " + msg);
        }
    }

    /**
     * Mark a form specification as public according to its technical CODE.
     *
     * @param code
     *            the code
     * @return the response entity
     * @throws TemporaryException
     */
    @RequestMapping(value = "/form/{code}/publish", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<Object> publish(@PathVariable("code") final String code) throws TemporaryException {
        final SpecificationInfoBean bean = this.formSpecService.load(code);

        if (null != bean) {
            final Response downloaded = this.formSpecService.download(code);

            if (Status.NO_CONTENT.getStatusCode() == downloaded.getStatus()) {
                LOGGER.info("Try to instanciante unknown specification '{}'", code);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            try {
                if (StringUtils.isEmpty(this.externalServiceUri) || this.sendToExternalAppInstance(bean, downloaded.readEntity(byte[].class))) {
                    bean.setTagged(true);
                    this.formSpecService.persist(code, bean);
                    return ResponseEntity.ok("ok");
                }
            } catch (NotFoundException | ProcessingException | Fault e) {
                LOGGER.error(MessageFormat.format("Error calling form Web with code \"{0}\" and author \"{1}\" : {2}", code, bean.getAuthor(), e.getMessage()));
                return new ResponseEntity<>(new ErrorMessage<>(ERROR_FORM_WEB_PUBLISHING, HttpStatus.SERVICE_UNAVAILABLE), HttpStatus.SERVICE_UNAVAILABLE);
            }
        }

        return ResponseEntity.ok("ok");
    }

    private boolean sendToExternalAppInstance(final SpecificationInfoBean bean, final byte[] content) {
        return this.externalService() //
                .map(service -> service.upload(bean.getCode(), content, true, bean.getAuthor(), "all", false)) //
                .map(Response::getStatus) //
                .map(status -> Status.CREATED.getStatusCode() == status) //
                .orElse(false);
    }

    private Optional<ISpecificationService> externalService() {
        if (StringUtils.isEmpty(this.externalServiceUri)) {
            return Optional.empty();
        } else {
            return Optional.of(JAXRSClientFactory.create(this.externalServiceUri, ISpecificationService.class));
        }
    }

    /**
     * Mark a form specification as unavailable for end users according to its
     * CODE.
     *
     * @param code
     *            the code
     * @return the response entity
     */
    @RequestMapping(value = "/form/{code}/unpublish", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<Object> unpublish(@PathVariable("code") final String code) {
        final SpecificationInfoBean bean = this.formSpecService.load(code);

        if (null != bean) {
            try {
                this.externalService().ifPresent(service -> service.remove(code));
            } catch (NotFoundException | ProcessingException | Fault e) {
                LOGGER.error(MessageFormat.format("Error calling form Web with code \"{0}\" and author \"{1}\" : {2}", code, bean.getAuthor(), e.getMessage()));
                return new ResponseEntity<>(new ErrorMessage<>(ERROR_FORM_WEB_UNPUBLISHING, HttpStatus.SERVICE_UNAVAILABLE), HttpStatus.SERVICE_UNAVAILABLE);

            }
            bean.setTagged(false);
            this.formSpecService.persist(code, bean);
        }

        return ResponseEntity.ok("ok");
    }

    /**
     * Download the specification template as a json file.
     *
     * @param code
     *            the code
     * @return the response entity
     */
    @RequestMapping(value = "/form/{code}/template", method = RequestMethod.GET)
    public ResponseEntity<byte[]> template(@PathVariable("code") final String code) {
        final Response response = this.formSpecService.templateByCode(code);

        if (Status.NO_CONTENT.getStatusCode() == response.getStatus()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION));
        return new ResponseEntity<>(response.readEntity(byte[].class), headers, HttpStatus.OK);

    }
}
