package fr.ge.common.nash.webui.core.loader.event;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.webui.core.authentication.bean.LocalUserBean;
import fr.ge.common.nash.webui.core.authentication.context.AccountContext;

public class LocalUserContextEventTest {

    /** La constante USER_UID. */
    private static final String USER_UID = "2016-12-USR-AAA-42";

    /** The event. */
    @InjectMocks
    private LocalUserContextEvent accountContextEvent;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        AccountContext.currentUser(null);
    }

    @Test
    public void testGetCurrentUser() {
        AccountContext.currentUser(new LocalUserBean(USER_UID));

        Object user = accountContextEvent.getCurrentUser();
        assertNotNull(user);
        assertThat(((LocalUserBean) user).getId(), equalTo(USER_UID));
    }

}
